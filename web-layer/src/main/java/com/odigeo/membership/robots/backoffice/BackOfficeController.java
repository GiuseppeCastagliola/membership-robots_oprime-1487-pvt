package com.odigeo.membership.robots.backoffice;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.robots.consumer.BookingUpdatesMessageProcessor;
import com.odigeo.membership.robots.discard.MembershipDiscardService;

import java.util.List;

public class BackOfficeController implements BackOfficeService {

    @Override
    public void discardMemberships(List<Long> membershipIds) {
        ConfigurationEngine.getInstance(MembershipDiscardService.class).discardMemberships(membershipIds);
    }

    @Override
    public void processBookings(List<Long> bookingIds) {
        BookingUpdatesMessageProcessor bookingUpdatesMessageProcessor = ConfigurationEngine.getInstance(BookingUpdatesMessageProcessor.class);
        bookingIds.forEach(bookingUpdatesMessageProcessor::putBookingIdInProcessingQueue);
    }
}
