package com.odigeo.membership.robots.rest;

import com.odigeo.membership.robots.backoffice.BackOfficeController;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<>();

    public ServiceApplication() {
        singletons.add(new BackOfficeController());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}
