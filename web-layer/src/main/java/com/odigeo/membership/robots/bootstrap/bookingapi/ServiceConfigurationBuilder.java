package com.odigeo.membership.robots.bootstrap.bookingapi;


import com.odigeo.bookingapi.client.v12.configuration.BookingApiSecurityInterceptor;
import com.odigeo.bookingapi.client.v12.configuration.DateConverter;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.error.JsonIgnorePropertiesContextResolver;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public final class ServiceConfigurationBuilder {

    private static final int MAX_CONCURRENT_CONNECTIONS = 20;

    private ServiceConfigurationBuilder() {
    }

    static <T> ServiceConfiguration setupBookingService(Class<T> serviceClass, int connectionTimeout, int socketTimeout) {
        ConnectionConfiguration connectionConfiguration = getConnectionConfiguration(connectionTimeout, socketTimeout);
        InterceptorConfiguration<T> interceptorConfiguration = new InterceptorConfiguration<>();
        interceptorConfiguration.addInterceptor(new BookingApiSecurityInterceptor());
        ServiceConfiguration<T> serviceConfiguration = new ServiceConfiguration.Builder<>(serviceClass)
                .withInterceptorConfiguration(interceptorConfiguration)
                .withConnectionConfiguration(connectionConfiguration)
                .withRestErrorsHandler(new SimpleRestErrorsHandler(serviceClass))
                .build();
        serviceConfiguration.getFactory().addStringConverter(DateConverter.class);
        JsonIgnorePropertiesContextResolver.configureIntoFactory(serviceConfiguration.getFactory());
        return serviceConfiguration;
    }

    private static ConnectionConfiguration getConnectionConfiguration(int connectionTimeout, int socketTimeout) {
        return new ConnectionConfiguration.Builder()
                .socketTimeoutInMillis(socketTimeout)
                .connectionTimeoutInMillis(connectionTimeout)
                .maxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .build();
    }
}
