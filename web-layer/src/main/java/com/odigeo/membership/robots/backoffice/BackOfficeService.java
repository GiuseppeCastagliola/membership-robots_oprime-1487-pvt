package com.odigeo.membership.robots.backoffice;

import io.swagger.annotations.ApiOperation;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;

@Path("/engineering/back-office")
public interface BackOfficeService {

    @PUT
    @Path("/discard")
    @Produces({"application/json; charset=UTF-8"})
    @ApiOperation(
            value = "Discard the memberships identified by the id's and generate a new PENDING_TO_COLLECT membership for each one of them.",
            notes = "Sample request: http://serverDomain/membership-robots/engineering/discard/discard"
        )
    void discardMemberships(List<Long> membershipIds);

    @PUT
    @Path("/process-bookings")
    @Consumes({"application/json; charset=UTF-8"})
    void processBookings(List<Long> bookingIds);
}
