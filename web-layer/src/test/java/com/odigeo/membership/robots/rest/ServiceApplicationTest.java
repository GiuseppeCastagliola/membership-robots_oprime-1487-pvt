package com.odigeo.membership.robots.rest;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ServiceApplicationTest {

    private static final int SINGLETONS = 1;

    @Test
    public void testServiceApplication() {
        assertEquals(new ServiceApplication().getSingletons().size(), SINGLETONS);
    }
}
