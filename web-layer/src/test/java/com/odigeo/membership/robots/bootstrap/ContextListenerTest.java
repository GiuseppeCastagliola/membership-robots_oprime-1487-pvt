package com.odigeo.membership.robots.bootstrap;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.robots.RobotsBootstrap;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class ContextListenerTest {
    @Mock
    RobotsBootstrap bootstrap;
    @Mock
    ServletContextEvent servletContextEvent;
    @Mock
    ServletContext servletContext;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        when(servletContextEvent.getServletContext()).thenReturn(servletContext);
        ConfigurationEngine.init(binder -> binder.bind(RobotsBootstrap.class).toInstance(bootstrap));
    }

    @Test
    public void contextDestroyedTest() {
        ContextListener contextListener = new ContextListener();
        contextListener.contextDestroyed(servletContextEvent);
        verify(bootstrap).shutdownScheduler();
    }

}
