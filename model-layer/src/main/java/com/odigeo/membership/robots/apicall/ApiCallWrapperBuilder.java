package com.odigeo.membership.robots.apicall;

public class ApiCallWrapperBuilder<T, U> {
    private final ApiCall.Endpoint endpointId;
    private ApiCallWrapper.Result result;
    private Throwable throwable;
    private String message;
    private T response;
    private U request;

    public ApiCallWrapperBuilder(ApiCall.Endpoint endpoint) {
        this.endpointId = endpoint;
    }

    public ApiCallWrapperBuilder(ApiCall.Endpoint endpoint, ApiCall.Result result) {
        this.result = result;
        this.endpointId = endpoint;
    }

    public ApiCallWrapperBuilder<T, U> result(ApiCallWrapper.Result result) {
        this.result = result;
        return this;
    }

    public ApiCallWrapperBuilder<T, U> throwable(Throwable throwable) {
        this.throwable = throwable;
        return this;
    }

    public ApiCallWrapperBuilder<T, U> message(String message) {
        this.message = message;
        return this;
    }

    public ApiCallWrapperBuilder<T, U> response(T response) {
        this.response = response;
        return this;
    }

    public ApiCallWrapperBuilder<T, U> request(U request) {
        this.request = request;
        return this;
    }

    public ApiCallWrapper.Result getResult() {
        return result;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public String getMessage() {
        return message;
    }

    public T getResponse() {
        return response;
    }

    public U getRequest() {
        return request;
    }

    public ApiCall.Endpoint getEndpointId() {
        return endpointId;
    }

    public ApiCallWrapper<T, U> build() {
        return new ApiCallWrapper<>(this);
    }
}
