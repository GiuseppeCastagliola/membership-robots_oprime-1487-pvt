package com.odigeo.membership.robots.dto;

import java.math.BigDecimal;
import java.util.Objects;

public class MembershipOfferDTO {
    private final Long productId;
    private final String website;
    private final String offerId;
    private final BigDecimal price;
    private final String currencyCode;
    private final Integer duration;

    private MembershipOfferDTO(Builder builder) {
        website = builder.website;
        productId = builder.productId;
        price = builder.price;
        currencyCode = builder.currencyCode;
        offerId = builder.offerId;
        duration = builder.duration;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Long getProductId() {
        return productId;
    }

    public String getWebsite() {
        return website;
    }

    public String getOfferId() {
        return offerId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Integer getDuration() {
        return duration;
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private Long productId;
        private String website;
        private String offerId;
        private BigDecimal price;
        private String currencyCode;
        private Integer duration;

        public Builder productId(Long productId) {
            this.productId = productId;
            return this;
        }

        public Builder website(String website) {
            this.website = website;
            return this;
        }

        public Builder offerId(String offerId) {
            this.offerId = offerId;
            return this;
        }

        public Builder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public Builder duration(Integer duration) {
            this.duration = duration;
            return this;
        }

        public MembershipOfferDTO build() {
            return new MembershipOfferDTO(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembershipOfferDTO that = (MembershipOfferDTO) o;
        return Objects.equals(productId, that.productId)
                && Objects.equals(website, that.website)
                && Objects.equals(offerId, that.offerId)
                && Objects.equals(price, that.price)
                && Objects.equals(currencyCode, that.currencyCode)
                && Objects.equals(duration, that.duration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, website, offerId, price, currencyCode, duration);
    }
}
