package com.odigeo.membership.robots.consumer;

import java.util.Objects;
import java.util.StringJoiner;

public class CollectionSummaryChargebackNotifications {

    private final Long bookingId;
    private final String action;
    private final String status;
    private final Long executionTimeInMillis;
    private final Long storageTimeInMillis;

    private CollectionSummaryChargebackNotifications(Builder builder) {
        this.bookingId = builder.bookingId;
        this.action = builder.action;
        this.status = builder.status;
        this.executionTimeInMillis = builder.executionTimeInMillis;
        this.storageTimeInMillis = builder.storageTimeInMillis;
    }

    public Long getBookingId() {
        return bookingId;
    }

    public String getAction() {
        return action;
    }

    public String getStatus() {
        return status;
    }

    public Long getExecutionTimeInMillis() {
        return executionTimeInMillis;
    }

    public Long getStorageTimeInMillis() {
        return storageTimeInMillis;
    }

    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {

        private Long bookingId;
        private String action;
        private String status;
        private Long executionTimeInMillis;
        private Long storageTimeInMillis;

        public Builder bookingId(Long bookingId) {
            this.bookingId = bookingId;
            return this;
        }

        public Builder action(String action) {
            this.action = action;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder executionTimeInMillis(Long executionTimeInMillis) {
            this.executionTimeInMillis = executionTimeInMillis;
            return this;
        }

        public Builder storageTimeInMillis(Long storageTimeInMillis) {
            this.storageTimeInMillis = storageTimeInMillis;
            return this;
        }

        public CollectionSummaryChargebackNotifications build() {
            return new CollectionSummaryChargebackNotifications(this);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CollectionSummaryChargebackNotifications that = (CollectionSummaryChargebackNotifications) o;
        return Objects.equals(bookingId, that.bookingId)
                && Objects.equals(action, that.action)
                && Objects.equals(status, that.status)
                && Objects.equals(executionTimeInMillis, that.executionTimeInMillis)
                && Objects.equals(storageTimeInMillis, that.storageTimeInMillis);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingId, action, status, executionTimeInMillis, storageTimeInMillis);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CollectionSummaryChargebackNotifications.class.getSimpleName() + "[", "]")
                .add("bookingId=" + bookingId)
                .add("action='" + action + '\'')
                .add("status='" + status + '\'')
                .add("executionTimeInMillis=" + executionTimeInMillis)
                .add("storageTimeInMillis=" + storageTimeInMillis)
                .toString();
    }
}
