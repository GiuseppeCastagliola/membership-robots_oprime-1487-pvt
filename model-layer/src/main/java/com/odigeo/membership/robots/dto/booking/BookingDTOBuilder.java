package com.odigeo.membership.robots.dto.booking;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class BookingDTOBuilder {
    protected long id;
    protected long userId;
    protected Long membershipId;
    protected List<String> productTypes;
    protected List<FeeContainerDTO> feeContainers;
    protected String currencyCode;
    protected Boolean testBooking;
    protected String bookingStatus;
    protected List<ProductCategoryBookingDTO> bookingProducts;
    protected List<CollectionAttemptDTO> collectionAttempts;
    protected LocalDateTime bookingDate;
    protected BigDecimal totalAvoidFeesAmount;
    protected BigDecimal totalCostFeesAmount;
    protected BigDecimal totalPerksAmount;

    public BookingDTOBuilder id(long id) {
        this.id = id;
        return this;
    }

    public BookingDTOBuilder userId(long userId) {
        this.userId = userId;
        return this;
    }

    public BookingDTOBuilder membershipId(Long membershipId) {
        this.membershipId = membershipId;
        return this;
    }

    public BookingDTOBuilder productTypes(List<String> productTypes) {
        this.productTypes = productTypes;
        return this;
    }

    public BookingDTOBuilder feeContainers(List<FeeContainerDTO> feeContainers) {
        this.feeContainers = feeContainers;
        return this;
    }

    public BookingDTOBuilder currencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public BookingDTOBuilder testBooking(Boolean testBooking) {
        this.testBooking = testBooking;
        return this;
    }

    public BookingDTOBuilder bookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
        return this;
    }

    public BookingDTOBuilder bookingProducts(List<ProductCategoryBookingDTO> bookingProducts) {
        this.bookingProducts = bookingProducts;
        return this;
    }

    public BookingDTOBuilder collectionAttempts(List<CollectionAttemptDTO> collectionAttempts) {
        this.collectionAttempts = collectionAttempts;
        return this;
    }

    public BookingDTOBuilder bookingDate(LocalDateTime bookingDate) {
        this.bookingDate = bookingDate;
        return this;
    }

    public BookingDTOBuilder totalAvoidFeesAmount(BigDecimal totalAvoidFeesAmount) {
        this.totalAvoidFeesAmount = totalAvoidFeesAmount;
        return this;
    }

    public BookingDTOBuilder totalCostFeesAmount(BigDecimal totalCostFeesAmount) {
        this.totalCostFeesAmount = totalCostFeesAmount;
        return this;
    }

    public BookingDTOBuilder totalPerksAmount(BigDecimal totalPerksAmount) {
        this.totalPerksAmount = totalPerksAmount;
        return this;
    }

    public BookingDTO build() {
        return new BookingDTO(this);
    }

}
