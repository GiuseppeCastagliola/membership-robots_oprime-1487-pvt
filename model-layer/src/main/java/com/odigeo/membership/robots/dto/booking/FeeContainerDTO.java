package com.odigeo.membership.robots.dto.booking;

import java.util.List;
import java.util.Objects;

import static java.util.Collections.unmodifiableList;

public class FeeContainerDTO {
    private final long id;
    private final String feeContainerType;
    private final List<FeeDTO> feeDTOs;

    private FeeContainerDTO(Builder builder) {
        this.id = builder.id;
        this.feeContainerType = builder.feeContainerType;
        this.feeDTOs = builder.feeDTOs;
    }

    public long getId() {
        return id;
    }

    public String getFeeContainerType() {
        return feeContainerType;
    }

    public List<FeeDTO> getFeeDTOs() {
        return unmodifiableList(feeDTOs);
    }

    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private long id;
        private String feeContainerType;
        private List<FeeDTO> feeDTOs;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder feeContainerType(String feeContainerType) {
            this.feeContainerType = feeContainerType;
            return this;
        }

        public Builder fees(List<FeeDTO> feeDTOs) {
            this.feeDTOs = unmodifiableList(feeDTOs);
            return this;
        }

        public FeeContainerDTO build() {
            return new FeeContainerDTO(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FeeContainerDTO that = (FeeContainerDTO) o;
        return id == that.id && Objects.equals(feeContainerType, that.feeContainerType)
                && Objects.equals(feeDTOs, that.feeDTOs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, feeContainerType, feeDTOs);
    }
}

