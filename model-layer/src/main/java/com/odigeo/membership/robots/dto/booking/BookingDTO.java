package com.odigeo.membership.robots.dto.booking;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.Collections.unmodifiableList;

public class BookingDTO {
    private final long id;
    private final long userId;
    private final Long membershipId;
    private final List<String> productTypes;
    private final List<FeeContainerDTO> feeContainers;
    private final String currencyCode;
    private final Boolean testBooking;
    private final String bookingStatus;
    private final List<ProductCategoryBookingDTO> bookingProducts;
    private final List<CollectionAttemptDTO> collectionAttempts;
    private final LocalDateTime bookingDate;
    private final BigDecimal totalAvoidFeesAmount;
    private final BigDecimal totalCostFeesAmount;
    private final BigDecimal totalPerksAmount;

    protected BookingDTO(BookingDTOBuilder builder) {
        id = builder.id;
        userId = builder.userId;
        membershipId = builder.membershipId;
        productTypes = builder.productTypes;
        feeContainers = builder.feeContainers;
        currencyCode = builder.currencyCode;
        testBooking = builder.testBooking;
        bookingStatus = builder.bookingStatus;
        bookingProducts = builder.bookingProducts;
        collectionAttempts = builder.collectionAttempts;
        bookingDate = builder.bookingDate;
        totalAvoidFeesAmount = builder.totalAvoidFeesAmount;
        totalCostFeesAmount = builder.totalCostFeesAmount;
        totalPerksAmount = builder.totalPerksAmount;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public Long getMembershipId() {
        return membershipId;
    }

    public List<String> getProductTypes() {
        return unmodifiableList(productTypes);
    }

    public List<FeeContainerDTO> getFeeContainers() {
        return unmodifiableList(feeContainers);
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Boolean isTestBooking() {
        return testBooking;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public List<ProductCategoryBookingDTO> getBookingProducts() {
        return bookingProducts;
    }

    public List<CollectionAttemptDTO> getCollectionAttempts() {
        return collectionAttempts;
    }

    public LocalDateTime getBookingDate() {
        return bookingDate;
    }

    public BigDecimal getTotalAvoidFeesAmount() {
        return totalAvoidFeesAmount;
    }

    public BigDecimal getTotalCostFeesAmount() {
        return totalCostFeesAmount;
    }

    public BigDecimal getTotalPerksAmount() {
        return totalPerksAmount;
    }

    public static BookingDTOBuilder builder() {
        return new BookingDTOBuilder();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BookingDTO that = (BookingDTO) o;
        return new EqualsBuilder()
                .append(id, that.id)
                .append(userId, that.userId)
                .append(membershipId, that.membershipId)
                .append(productTypes, that.productTypes)
                .append(feeContainers, that.feeContainers)
                .append(currencyCode, that.currencyCode)
                .append(testBooking, that.testBooking)
                .append(bookingStatus, that.bookingStatus)
                .append(bookingProducts, that.bookingProducts)
                .append(collectionAttempts, that.collectionAttempts)
                .append(bookingDate, that.bookingDate)
                .append(totalAvoidFeesAmount, that.totalAvoidFeesAmount)
                .append(totalCostFeesAmount, that.totalCostFeesAmount)
                .append(totalPerksAmount, that.totalPerksAmount)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(userId)
                .append(membershipId)
                .append(productTypes)
                .append(feeContainers)
                .append(currencyCode)
                .append(testBooking)
                .append(bookingStatus)
                .append(bookingProducts)
                .append(collectionAttempts)
                .append(bookingDate)
                .append(totalAvoidFeesAmount)
                .append(totalCostFeesAmount)
                .append(totalPerksAmount)
                .toHashCode();
    }
}
