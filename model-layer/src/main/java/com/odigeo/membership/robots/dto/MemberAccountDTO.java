package com.odigeo.membership.robots.dto;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class MemberAccountDTO {
    private final long id;
    private final long userId;
    private final String name;
    private final String lastNames;
    private final List<MembershipDTO> membershipDTOS;

    private MemberAccountDTO(Builder builder) {
        lastNames = builder.lastNames;
        membershipDTOS = builder.membershipDTOS;
        name = builder.name;
        id = builder.id;
        userId = builder.userId;
    }

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public List<MembershipDTO> getMembershipDTOS() {
        return Collections.unmodifiableList(membershipDTOS);
    }

    public static Builder builder() {
        return new Builder();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class Builder {
        private long id;
        private long userId;
        private String name;
        private String lastNames;
        private List<MembershipDTO> membershipDTOS;

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder userId(long userId) {
            this.userId = userId;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder lastNames(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        public Builder memberships(List<MembershipDTO> membershipDTOS) {
            this.membershipDTOS = Collections.unmodifiableList(membershipDTOS);
            return this;
        }

        public MemberAccountDTO build() {
            return new MemberAccountDTO(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MemberAccountDTO that = (MemberAccountDTO) o;
        return id == that.id
                && userId == that.userId
                && Objects.equals(name, that.name)
                && Objects.equals(lastNames, that.lastNames)
                && Objects.equals(membershipDTOS, that.membershipDTOS);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, name, lastNames, membershipDTOS);
    }
}
