package com.odigeo.membership.robots.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class MembershipDTOBuilder {
    protected long id;
    protected String website;
    protected String status;
    protected String autoRenewal;
    protected long memberAccountId;
    protected LocalDateTime expirationDate;
    protected LocalDateTime activationDate;
    protected LocalDateTime timestamp;
    protected BigDecimal userCreditCardId;
    protected String membershipType;
    protected BigDecimal balance;
    protected Integer monthsDuration;
    protected String productStatus;
    protected BigDecimal totalPrice;
    protected String currencyCode;
    protected String sourceType;
    protected String recurringId;
    protected String name;
    protected String lastName;
    protected Long userId;

    public static MembershipDTOBuilder builderFromDto(MembershipDTO membershipDTO) {
        return new MembershipDTOBuilder()
                .id(membershipDTO.getId())
                .userCreditCardId(membershipDTO.getUserCreditCardId())
                .status(membershipDTO.getStatus())
                .totalPrice(membershipDTO.getTotalPrice())
                .productStatus(membershipDTO.getProductStatus())
                .recurringId(membershipDTO.getRecurringId())
                .activationDate(membershipDTO.getActivationDate())
                .expirationDate(membershipDTO.getExpirationDate())
                .timestamp(membershipDTO.getTimestamp())
                .monthsDuration(membershipDTO.getMonthsDuration())
                .sourceType(membershipDTO.getSourceType())
                .balance(membershipDTO.getBalance())
                .autoRenewal(membershipDTO.getAutoRenewal())
                .memberAccountId(membershipDTO.getMemberAccountId())
                .website(membershipDTO.getWebsite())
                .membershipType(membershipDTO.getMembershipType())
                .currencyCode(membershipDTO.getCurrencyCode())
                .name(membershipDTO.getName())
                .lastName(membershipDTO.getLastName())
                .userId(membershipDTO.getUserId())
                .totalPrice(membershipDTO.getTotalPrice());
    }

    public MembershipDTOBuilder id(long id) {
        this.id = id;
        return this;
    }

    public MembershipDTOBuilder website(String website) {
        this.website = website;
        return this;
    }

    public MembershipDTOBuilder status(String status) {
        this.status = status;
        return this;
    }

    public MembershipDTOBuilder autoRenewal(String autoRenewal) {
        this.autoRenewal = autoRenewal;
        return this;
    }

    public MembershipDTOBuilder memberAccountId(long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public MembershipDTOBuilder expirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    public MembershipDTOBuilder activationDate(LocalDateTime activationDate) {
        this.activationDate = activationDate;
        return this;
    }

    public MembershipDTOBuilder userCreditCardId(BigDecimal userCreditCardId) {
        this.userCreditCardId = userCreditCardId;
        return this;
    }

    public MembershipDTOBuilder membershipType(String membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public MembershipDTOBuilder balance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public MembershipDTOBuilder monthsDuration(Integer monthsDuration) {
        this.monthsDuration = monthsDuration;
        return this;
    }

    public MembershipDTOBuilder productStatus(String productStatus) {
        this.productStatus = productStatus;
        return this;
    }

    public MembershipDTOBuilder totalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public MembershipDTOBuilder currencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public MembershipDTOBuilder sourceType(String sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public MembershipDTOBuilder recurringId(String recurringId) {
        this.recurringId = recurringId;
        return this;
    }

    public MembershipDTOBuilder timestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public MembershipDTOBuilder name(String name) {
        this.name = name;
        return this;
    }

    public MembershipDTOBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public MembershipDTOBuilder userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public MembershipDTO build() {
        return new MembershipDTO(this);
    }
}
