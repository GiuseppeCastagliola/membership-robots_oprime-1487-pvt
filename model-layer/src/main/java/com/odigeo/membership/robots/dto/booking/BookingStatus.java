package com.odigeo.membership.robots.dto.booking;

public enum BookingStatus {
    CONTRACT, REQUEST, INIT
}
