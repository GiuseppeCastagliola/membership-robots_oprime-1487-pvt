package com.odigeo.membership.robots.dto;


import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class SearchMemberAccountsDTOTest extends BeanTest<SearchMemberAccountsDTO> {

    private static final String SURNAME = "Ba";
    private static final String NAME = "Abel";
    private static final Long USER_ID = 1L;

    @Override
    protected SearchMemberAccountsDTO getBean() {
        return SearchMemberAccountsDTO.builder()
                .lastName(SURNAME)
                .firstName(NAME)
                .userId(USER_ID)
                .build();
    }

    @Test
    public void searchMemberAccountsDTOTestEqualsVerifierTest() {
        EqualsVerifier.forClass(SearchMemberAccountsDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}