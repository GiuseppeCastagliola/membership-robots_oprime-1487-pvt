package com.odigeo.membership.robots.report;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class ReportTest extends BeanTest<Report> {

    @Override
    protected Report getBean() {
        return new Report();
    }

    @Test
    public void membershipOfferDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(Report.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}