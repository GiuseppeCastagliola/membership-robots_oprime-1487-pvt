package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

public class SearchMembershipsDTOTest extends BeanTest<SearchMembershipsDTO> {

    private static final LocalDate NOW = LocalDate.now();
    private static final String ENABLED = "ENABLED";
    private static final String ACTIVATED = "ACTIVATED";
    private static final String WEBSITE = "ES";

    @Override
    protected SearchMembershipsDTO getBean() {
        return SearchMembershipsDTO.builder()
                .autoRenewal(ENABLED)
                .fromExpirationDate(NOW)
                .toExpirationDate(NOW)
                .status(ACTIVATED)
                .withMemberAccount(false)
                .searchMemberAccountsDTO(SearchMemberAccountsDTO.builder().build())
                .website(WEBSITE)
                .fromActivationDate(NOW)
                .minBalance(BigDecimal.ONE)
                .build();
    }

    @Test
    public void searchMembershipsDTOEqualsVerifierTest() {
        EqualsVerifier.forClass(SearchMembershipsDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}