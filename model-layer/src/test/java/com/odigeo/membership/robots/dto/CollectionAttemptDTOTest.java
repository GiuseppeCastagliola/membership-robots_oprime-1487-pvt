package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import com.odigeo.membership.robots.dto.booking.CollectionAttemptDTO;
import com.odigeo.membership.robots.dto.booking.MovementDTO;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class CollectionAttemptDTOTest extends BeanTest<CollectionAttemptDTO> {

    private static final Integer ID = 1;
    private static final String RECURRING_ID = "abc";

    @Override
    protected CollectionAttemptDTO getBean() {
        return CollectionAttemptDTO.builder()
                .id(ID)
                .recurringId(RECURRING_ID)
                .lastMovement(MovementDTO.builder().build())
                .build();
    }

    @Test
    public void bookingDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(CollectionAttemptDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

}
