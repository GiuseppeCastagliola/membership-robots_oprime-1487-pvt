package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import com.odigeo.membership.robots.dto.booking.FeeContainerDTO;
import com.odigeo.membership.robots.dto.booking.FeeDTO;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.util.Collections;

public class FeeContainerDTOTest extends BeanTest<FeeContainerDTO> {

    private static final long FEE_CONTAINER_ID = 1L;
    private static final String FEE_CONTAINER_TYPE = "type";

    @Override
    protected FeeContainerDTO getBean() {
        return FeeContainerDTO.builder()
                .fees(Collections.singletonList(FeeDTO.builder().build()))
                .id(FEE_CONTAINER_ID)
                .feeContainerType(FEE_CONTAINER_TYPE)
                .build();
    }

    @Test
    public void feeContainerDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(FeeContainerDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}