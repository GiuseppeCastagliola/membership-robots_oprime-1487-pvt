package com.odigeo.membership.robots.mocks.bookingsearchapiservice;

import com.odigeo.bookingsearchapi.mock.v1.response.BookingSummaryBuilder;
import com.odigeo.bookingsearchapi.mock.v1.response.BuilderException;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.bookingsearchapi.v1.requests.DepartureOrArrival;
import com.odigeo.bookingsearchapi.v1.requests.GeoNodeType;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingAggregations;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.PopularRoutes;
import com.odigeo.bookingsearchapi.v1.responses.RouteBookingAggregations;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.bookingsearchapi.v1.responses.WebsiteGeoNodePriority;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.robots.mocks.MockException;
import com.odigeo.membership.robots.mocks.ServiceMock;
import com.odigeo.membership.robots.mocks.membershipsearchapi.MembershipSearchApiEndpointName;
import com.odigeo.membership.robots.utils.FunctionalUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import static com.odigeo.membership.robots.mocks.bookingsearchapiservice.BookingSearchApiServiceEndpointName.SEARCH_BOOKINGS;

public class BookingSearchApiServiceMock extends ServiceMock<BookingSearchApiServiceEndpointName> implements BookingSearchApiService {

    private static final Logger LOGGER = Logger.getLogger(BookingSearchApiServiceMock.class);
    private static final long REQUEST_TIMEOUT = 500L;

    @Override
    public BookingSummary getBookingSummary(String s, String s1, Locale locale, long l) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public SearchBookingsPageResponse<List<BookingSummary>> searchBookings(String string, String string1, Locale locale, SearchBookingsRequest searchBookingsRequest) {
        /*
         *    TODO fix error throwing/mapping/handling
         *     Currently errors are simulated by causing a timeout which triggers an UndeclaredThrowableException in the calling method
         */
        List<Object> bookingParams = Arrays.asList(string, string1, locale, searchBookingsRequest);
        saveRequest(SEARCH_BOOKINGS, bookingParams);

        if (isExceptionResponse(SEARCH_BOOKINGS)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }

        Optional<Object> mockedErrorResponse = findMockedErrorResponse(SEARCH_BOOKINGS, searchBookingsRequest.getMembershipId());
        if (mockedErrorResponse.isPresent()) {
            Object mockedResponse = mockedErrorResponse.get();
            if (mockedResponse instanceof String && "null".equals(mockedResponse)) {
                return null;
            }
            return (SearchBookingsPageResponse) mockedResponse;
        }

        List<Object> bookings = findResponseEntities(SEARCH_BOOKINGS);
        List<BookingSummary> bookingSummaries = bookings.stream()
                .map(BookingSummaryBuilder.class::cast)
                .map(this::buildBookingSummary)
                .filter(Optional::isPresent).map(Optional::get)
                .filter(bookingSummary -> requestFilter(searchBookingsRequest, bookingSummary))
                .collect(Collectors.toList());
        return new SearchBookingsPageResponse<>(bookingSummaries, bookingSummaries.size());
    }

    @Override
    public List<RouteBookingAggregations> routeByCarrier(String s, String s1, String s2, Date date, Date date1, String s3, Integer integer) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public PopularRoutes getPopularRoutes(String s, String s1, String s2, String s3, Date date, Date date1, Integer integer) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public List<BookingAggregations> bookingAggregations(String s, String s1, DepartureOrArrival departureOrArrival, int i, GeoNodeType geoNodeType, String s2, Date date, Date date1, Integer integer) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public List<WebsiteGeoNodePriority> getGeoNodePriorities(String s, String s1, Integer integer) {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    private Optional<BookingSummary> buildBookingSummary(BookingSummaryBuilder bookingSummaryBuilder) {
        try {
            return Optional.of(bookingSummaryBuilder.build(new Random()));
        } catch (BuilderException e) {
            LOGGER.error("BuilderException thrown for BookingSummaryBuilder.BookingBasicInfo.Id: " + bookingSummaryBuilder.getBookingBasicInfo().getId(), e);
        }
        return Optional.empty();
    }

    private boolean requestFilter(SearchBookingsRequest request, BookingSummary booking) {
        Long memberAccountId = getMemberAccountIdForMembershipId(request.getMembershipId(), booking);

        return (request.getIsBookingSubscriptionPrime().isEmpty()
                || request.getIsBookingSubscriptionPrime().equalsIgnoreCase(booking.getIsBookingSubscriptionPrime()))
                && (Objects.isNull(request.getMembershipId())
                || memberAccountId.equals(booking.getMemberAccountId()));
    }

    private Long getMemberAccountIdForMembershipId(Long membershipId, BookingSummary booking) {
        List<Long> memberAccountIds = findResponseEntities(MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS).stream()
                .map(MembershipResponse.class::cast)
                .filter(m -> m.getId() == membershipId)
                .map(MembershipResponse::getMemberAccountId)
                .collect(Collectors.toList());

        if (memberAccountIds.size() != 1) {
            throw new IllegalStateException(String.format("%s memberAccountIds retrieved for memberships with membershipId %s. There should %sbe one.", memberAccountIds.size(), booking.getMembershipId(), memberAccountIds.size() > 1 ? "only " : ""));
        }

        return memberAccountIds.get(0);
    }
}
