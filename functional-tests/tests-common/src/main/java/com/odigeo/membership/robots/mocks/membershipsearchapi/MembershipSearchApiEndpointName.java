package com.odigeo.membership.robots.mocks.membershipsearchapi;

import com.odigeo.membership.robots.mocks.EndpointName;

public enum MembershipSearchApiEndpointName implements EndpointName {
    SEARCH_MEMBERSHIPS,
    SEARCH_ACCOUNTS,
    GET_MEMBERSHIP
}
