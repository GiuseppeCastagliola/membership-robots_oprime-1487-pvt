package com.odigeo.membership.robots.mocks;

public class MockException extends RuntimeException {
    public static final String NO_MOCKED_METHOD = "This method hasn't been mocked, please add ServiceMock.saveRequestAndFindResponse to its body.";

    public MockException() {
    }

    public MockException(Exception e) {
        super(e);
    }
}
