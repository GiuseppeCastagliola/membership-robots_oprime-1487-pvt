package com.odigeo.membership.robots.mocks.bookingsearchapiservice;

import com.odigeo.membership.robots.server.JaxRsServiceHttpServer;

import javax.inject.Singleton;

@Singleton
public class BookingSearchApiServiceServer extends JaxRsServiceHttpServer {

    private static final String BOOKING_SEARCH_API_PATH = "/engine";
    private static final int BOOKING_SEARCH_API_PORT = 2332;

    public BookingSearchApiServiceServer() {
        super(BOOKING_SEARCH_API_PATH, BOOKING_SEARCH_API_PORT);
    }
}
