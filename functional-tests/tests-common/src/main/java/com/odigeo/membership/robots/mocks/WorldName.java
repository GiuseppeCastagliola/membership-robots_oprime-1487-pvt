package com.odigeo.membership.robots.mocks;

public enum WorldName {
    MEMBERSHIP,
    BOOKING_API,
    BOOKING_SEARCH_API,
    MEMBERSHIP_SEARCH_API,
    MEMBERSHIP_OFFER
}
