package com.odigeo.membership.robots.mocks.bookingapiservice;

import com.odigeo.membership.robots.server.JaxRsServiceHttpServer;

import javax.inject.Singleton;

@Singleton
public class BookingApiServiceServer extends JaxRsServiceHttpServer {

    private static final String BOOKING_API_PATH = "/engine";
    private static final int BOOKING_API_PORT = 2331;

    public BookingApiServiceServer() {
        super(BOOKING_API_PATH, BOOKING_API_PORT);
    }
}
