package com.odigeo.membership.robots.mocks.membershipservice;

import com.google.inject.Singleton;
import com.odigeo.membership.robots.mocks.ServiceServer;

@Singleton
public class MembershipServiceServer extends ServiceServer {

    private static final String MEMBERSHIP_SERVICE_PATH = "/membership";
    private static final int MEMBERSHIP_SERVICE_PORT = 2335;

    public MembershipServiceServer() {
        super(MEMBERSHIP_SERVICE_PATH, MEMBERSHIP_SERVICE_PORT);
    }
}
