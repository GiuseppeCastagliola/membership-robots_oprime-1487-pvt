package com.odigeo.membership.robots.steps;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.odigeo.bookingapi.mock.v12.response.AdditionalProductBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.CollectionAttemptBuilder;
import com.odigeo.bookingapi.mock.v12.response.CollectionSummaryBuilder;
import com.odigeo.bookingapi.mock.v12.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v12.response.FeeContainerBuilder;
import com.odigeo.bookingapi.mock.v12.response.MembershipPerksBuilder;
import com.odigeo.bookingapi.mock.v12.response.MembershipSubscriptionBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.MovementBuilder;
import com.odigeo.bookingapi.mock.v12.response.ProductCategoryBookingBuilder;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingBasicInfoBuilder;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingSummaryBuilder;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.membership.robots.mocks.bookingapiservice.BookingApiServiceEndpointName;
import com.odigeo.membership.robots.mocks.bookingapiservice.BookingApiServiceWorld;
import com.odigeo.membership.robots.mocks.bookingsearchapiservice.BookingSearchApiServiceEndpointName;
import com.odigeo.membership.robots.mocks.bookingsearchapiservice.BookingSearchApiServiceWorld;
import com.odigeo.membership.robots.utils.FunctionalUtils;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import static com.odigeo.bookingapi.v12.requests.ProductCategoryBookingItemType.ADDITIONAL_PRODUCT;
import static com.odigeo.bookingapi.v12.requests.ProductCategoryBookingItemType.MEMBERSHIP_RENEWAL;
import static com.odigeo.bookingapi.v12.requests.ProductCategoryBookingItemType.MEMBERSHIP_SUBSCRIPTION;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

@SuppressWarnings({"PMD.NullAssignment", "PMD.TooManyStaticImports"})
@ScenarioScoped
public class BookingsSteps {

    private static final String MEMBERSHIP_ID = "membershipId";
    private static final String MEMBER_ACCOUNT_ID = "memberAccountId";
    private static final String IS_BOOKING_SUBSCRIPTION_PRIME = "isBookingSubscriptionPrime";
    private static final String MEMBERSHIP_TYPE = "membershipType";
    private static final String SUBSCRIPTION_PRICE = "subscriptionPrice";
    private static final String SUBCODE = "subCode";
    private static final String NULL_AS_STRING = "null";
    private static final String IS_RENEWAL_BOOKING = "isRenewalBooking";
    private static final String IS_THERE_ADDITIONAL_PRODUCT = "isThereAdditionalProduct";

    private static final List<String> BOOKING_HEADERS = Arrays.asList(MEMBERSHIP_ID, MEMBER_ACCOUNT_ID,
            IS_BOOKING_SUBSCRIPTION_PRIME, MEMBERSHIP_TYPE, SUBSCRIPTION_PRICE, SUBCODE);
    private static final Map<String, String> BOOKING_DEFAULT_VALUES = ImmutableMap.of(
            IS_BOOKING_SUBSCRIPTION_PRIME, "true",
            MEMBERSHIP_TYPE, "MEMBERSHIP_SUBSCRIPTION",
            SUBSCRIPTION_PRICE, "54.99");
    private static final ImmutableMap<String, String> MEMBERSHIP_FEE_CONTAINER_TYPES = ImmutableMap.of(
            "MEMBERSHIP_RENEWAL", "AE10",
            "MEMBERSHIP_SUBSCRIPTION", "AE11");

    private final BookingSearchApiServiceWorld bookingSearchApiServiceWorld;
    private final BookingApiServiceWorld bookingApiServiceWorld;
    private final AtomicLong idGenerator = new AtomicLong(7877000L);

    @Inject
    public BookingsSteps(BookingSearchApiServiceWorld bookingSearchApiServiceWorld, BookingApiServiceWorld bookingApiServiceWorld) {
        this.bookingSearchApiServiceWorld = bookingSearchApiServiceWorld;
        this.bookingApiServiceWorld = bookingApiServiceWorld;
    }

    @Given("^the following bookings exist:")
    public void createBookings(DataTable bookingsPropertiesDataTable) {
        List<Map<String, String>> bookingsProperties = FunctionalUtils.asMapsWithNullsAndEmptyStrings(bookingsPropertiesDataTable);

        generateAndStoreBookingSummaryBuilders(bookingsProperties);
        generateAndStoreBookingDetailBuilders(bookingsProperties);
    }

    @Given("^the following booking exists:")
    public void createBooking(DataTable bookingPropertiesDataTable) {
        Map<String, String> bookingProperties = FunctionalUtils.asMapWithNullsAndEmptyStrings(bookingPropertiesDataTable);
        generateAndStoreBookingSummaryBuilders(Collections.singletonList(bookingProperties));
        generateAndStoreBookingDetailBuilders(Collections.singletonList(bookingProperties));
    }

    @Given("^(SEARCH_BOOKINGS|GET_BOOKING|UPDATE_BOOKING) endpoint of BookingsApi service returns a (null|error) response for membershipId (\\d+)")
    public void endpointOfBookingApiReturnsErrorResponse(String membershipEndpoint, String responseType, long membershipId) {
        switch (membershipEndpoint) {
        case "SEARCH_BOOKINGS":
            SearchBookingsPageResponse searchBookingsErrorResponse = new SearchBookingsPageResponse<>();
            searchBookingsErrorResponse.setErrorMessage("Mocked error response");
            bookingSearchApiServiceWorld.addMockedErrorResponse(BookingSearchApiServiceEndpointName.SEARCH_BOOKINGS, membershipId,
                    responseType.equals(NULL_AS_STRING) ? NULL_AS_STRING : searchBookingsErrorResponse);
            break;
        case "GET_BOOKING":
            BookingDetail getBookingErrorResponse = new BookingDetail();
            getBookingErrorResponse.setErrorMessage("Mocked error response");
            bookingApiServiceWorld.addMockedErrorResponse(BookingApiServiceEndpointName.GET_BOOKING, membershipId,
                    responseType.equals(NULL_AS_STRING) ? NULL_AS_STRING : getBookingErrorResponse);
            break;
        case "UPDATE_BOOKING":
            BookingDetail updateBookingErrorResponse = new BookingDetail();
            updateBookingErrorResponse.setErrorMessage("Mocked error response");
            bookingApiServiceWorld.addMockedErrorResponse(BookingApiServiceEndpointName.UPDATE_BOOKING, membershipId,
                    responseType.equals(NULL_AS_STRING) ? NULL_AS_STRING : updateBookingErrorResponse);
            break;
        default:
        }
    }

    /**
     * This step deletes the default FeeContainer created during the createBooking(s) steps
     * It is used when you want to create custom FeeContainers for a booking.
     * It should be called once, after createBooking(s), and before addFeeContainersToBooking is called.
     *
     * @param bookingId
     */
    @Given("^booking (\\d+) has custom feeContainers")
    public void bookingHasCustomFeeContainers(String bookingId) {
        List<BookingDetailBuilder> storedBookingDetailBuilders = bookingApiServiceWorld.getStoredMockResponsesForEndpoint(BookingApiServiceEndpointName.GET_BOOKING).stream()
                .map(BookingDetailBuilder.class::cast)
                .collect(toList());

        BookingDetailBuilder bookingDetailBuilder = storedBookingDetailBuilders.stream()
                .filter(builder -> builder.getBookingBasicInfoBuilder().getId() == Long.parseLong(bookingId))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No stored booking found for " + bookingId));

        bookingDetailBuilder.allFeeContainers(Collections.<FeeContainerBuilder>emptyList());
        bookingApiServiceWorld.updateMockedResponseEntities(BookingApiServiceEndpointName.GET_BOOKING, storedBookingDetailBuilders);
    }

    @Given("^booking (\\d+) has the following feeContainers:")
    public void addFeeContainersToBooking(String bookingId, DataTable feeContainerPropertiesDataTable) {
        List<Map<String, String>> feeContainerProperties = FunctionalUtils.asMapsWithNullsAndEmptyStrings(feeContainerPropertiesDataTable);

        generateAndAddFeeContainerBuildersToStoredBooking(bookingId, feeContainerProperties);
    }

    @Given("^feeContainer (\\d+) in booking (\\d+) has the following fees:")
    public void addFeesToFeeContainerInBooking(String feeContainerId, String bookingId, DataTable feePropertiesDataTable) {
        List<Map<String, String>> feeProperties = FunctionalUtils.asMapsWithNullsAndEmptyStrings(feePropertiesDataTable);

        generateAndAddFeeBuildersToFeeContainerInStoredBooking(feeContainerId, bookingId, feeProperties);
    }

    void createSubscriptionBookingForMembership(Map<String, String> membershipProperties) {
        List<List<String>> raw = membershipProperties.entrySet().stream()
                .filter(entry -> BOOKING_HEADERS.contains(entry.getKey()))
                .map(entry -> Arrays.asList(entry.getKey(), entry.getValue()))
                .collect(toList());

        createBooking(DataTable.create(raw));
    }

    private void generateAndStoreBookingSummaryBuilders(List<Map<String, String>> bookingsProperties) {
        List<BookingSummaryBuilder> bookingSummaryBuilders = bookingsProperties.stream()
                .map(this::generateBookingSummaryBuilders)
                .collect(toList());

        bookingSearchApiServiceWorld.addMockedResponseEntities(BookingSearchApiServiceEndpointName.SEARCH_BOOKINGS, bookingSummaryBuilders);
    }

    private void generateAndStoreBookingDetailBuilders(List<Map<String, String>> bookingsProperties) {
        List<BookingDetailBuilder> bookingDetailBuilders = bookingsProperties.stream()
                .map(this::generateBookingDetailBuilders)
                .collect(toList());
        bookingApiServiceWorld.addMockedResponseEntities(BookingApiServiceEndpointName.GET_BOOKING, bookingDetailBuilders);
    }

    private BookingSummaryBuilder generateBookingSummaryBuilders(Map<String, String> bookingProperties) {
        Long bookingId = Long.valueOf(bookingProperties.getOrDefault("bookingId", bookingProperties.get(MEMBERSHIP_ID)));
        BookingBasicInfoBuilder bookingBasicInfoBuilder = new BookingBasicInfoBuilder().id(bookingId);

        return new BookingSummaryBuilder()
                .bookingBasicInfo(bookingBasicInfoBuilder)
                .membershipId(Long.valueOf(bookingProperties.get(MEMBERSHIP_ID)))
                .membershipAccountId(Long.valueOf(bookingProperties.get(MEMBER_ACCOUNT_ID)))
                .setIsBookingSubscriptionPrime(bookingProperties.getOrDefault(IS_BOOKING_SUBSCRIPTION_PRIME, BOOKING_DEFAULT_VALUES.get(IS_BOOKING_SUBSCRIPTION_PRIME)));
    }

    private BookingDetailBuilder generateBookingDetailBuilders(Map<String, String> bookingProperties) {
        Long bookingId = Long.valueOf(bookingProperties.getOrDefault("bookingId", bookingProperties.get(MEMBERSHIP_ID)));
        String membershipType = bookingProperties.getOrDefault(MEMBERSHIP_TYPE, BOOKING_DEFAULT_VALUES.get(MEMBERSHIP_TYPE));


        return new BookingDetailBuilder()
                .bookingBasicInfoBuilder(new com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder().id(bookingId))
                .currencyCode(bookingProperties.getOrDefault("currencyCode", "EUR"))
                .testBooking(Boolean.getBoolean(bookingProperties.getOrDefault("test", "false")))
                .bookingStatus(bookingProperties.getOrDefault("bookingStatus", "CONTRACT"))
                .collectionSummary(getCollectionWithValidMovementBuilder(bookingProperties))
                .membershipPerks(perksIfMembershipIdIsNotNull(bookingProperties.get(MEMBERSHIP_ID)))
                .bookingProducts(buildSubscriptionOrRenewalProduct(bookingProperties))
                .allFeeContainers(Collections.singletonList(generateFeeContainerBuilder(
                        membershipType,
                        bookingProperties.getOrDefault(SUBSCRIPTION_PRICE, BOOKING_DEFAULT_VALUES.get(SUBSCRIPTION_PRICE)),
                        bookingProperties.getOrDefault("currencyCode", "EUR"),
                        bookingProperties.getOrDefault(SUBCODE, MEMBERSHIP_FEE_CONTAINER_TYPES.getOrDefault(membershipType, "ZZ00"))))
                );
    }

    private CollectionSummaryBuilder getCollectionWithValidMovementBuilder(Map<String, String> bookingProperties) {
        if (Boolean.parseBoolean(bookingProperties.getOrDefault("withRecurringAndValidCollection", "false"))) {
            return new CollectionSummaryBuilder().attempts(Collections.singletonList(new CollectionAttemptBuilder()
                    .recurringId("123")
                    .validMovements(Collections.singletonList(new MovementBuilder().action("CONFIRM").status("PAID")))));
        }
        return null;
    }

    private List<ProductCategoryBookingBuilder> buildSubscriptionOrRenewalProduct(Map<String, String> bookingProperties) {
        ArrayList<ProductCategoryBookingBuilder> productsBuilder = new ArrayList<>();
        if (nonNull(bookingProperties.get(MEMBERSHIP_ID))) {
            boolean isSubscription = Boolean.parseBoolean(bookingProperties.get(IS_BOOKING_SUBSCRIPTION_PRIME));
            boolean isRenewal = Boolean.parseBoolean(bookingProperties.get(IS_RENEWAL_BOOKING));
            boolean isThereAdditionalProduct = Boolean.parseBoolean(bookingProperties.get(IS_THERE_ADDITIONAL_PRODUCT));
            if (isSubscription) {
                productsBuilder.add(new MembershipSubscriptionBookingBuilder()
                        .memberId(Long.parseLong(bookingProperties.get(MEMBERSHIP_ID)))
                        .productType(MEMBERSHIP_SUBSCRIPTION.name())
                        .totalPrice(new BigDecimal(bookingProperties.getOrDefault(SUBSCRIPTION_PRICE, BOOKING_DEFAULT_VALUES.get(SUBSCRIPTION_PRICE)))));
            }
            if (isRenewal) {
                productsBuilder.add(new MembershipSubscriptionBookingBuilder()
                        .memberId(Long.parseLong(bookingProperties.get(MEMBERSHIP_ID)))
                        .productType(MEMBERSHIP_RENEWAL.name()));
            }
            if (isThereAdditionalProduct) {
                productsBuilder.add(new AdditionalProductBookingBuilder()
                        .productType(ADDITIONAL_PRODUCT.name()));
            }
        }
        return productsBuilder;
    }

    private MembershipPerksBuilder perksIfMembershipIdIsNotNull(String membershipId) {
        if (nonNull(membershipId)) {
            return new MembershipPerksBuilder().memberId(Long.parseLong(membershipId));
        }
        return null;
    }

    private FeeContainerBuilder generateFeeContainerBuilder(String membershipType, String subscriptionPrice, String currency, String subCode) {
        FeeBuilder membershipSubscriptionFee = new FeeBuilder().id(idGenerator.incrementAndGet())
                .subCode(subCode)
                .amount(new BigDecimal(subscriptionPrice))
                .currency(currency);

        return new FeeContainerBuilder().id(idGenerator.incrementAndGet())
                .feeContainerType(membershipType)
                .fees(Collections.singletonList(membershipSubscriptionFee))
                .totalAmount(new BigDecimal(subscriptionPrice));
    }

    private FeeContainerBuilder convertToFeeContainerBuilder(Map<String, String> feeContainerProperties) {
        return new FeeContainerBuilder()
                .id(Long.parseLong(feeContainerProperties.get("feeContainerId")))
                .feeContainerType(feeContainerProperties.get("feeContainerType"))
                .totalAmount(new BigDecimal(feeContainerProperties.get("totalAmount")));
    }

    private FeeBuilder convertToFeeBuilder(Map<String, String> feeBuilderProperties) {
        return new FeeBuilder().id(idGenerator.incrementAndGet())
                .subCode(feeBuilderProperties.get(SUBCODE))
                .feeLabel(feeBuilderProperties.getOrDefault("label", StringUtils.EMPTY))
                .amount(new BigDecimal(feeBuilderProperties.get("amount")))
                .currency(feeBuilderProperties.get("currency"));
    }

    private void generateAndAddFeeContainerBuildersToStoredBooking(String bookingId, List<Map<String, String>> feeContainerProperties) {
        List<BookingDetailBuilder> storedBookingDetailBuilders = bookingApiServiceWorld.getStoredMockResponsesForEndpoint(BookingApiServiceEndpointName.GET_BOOKING).stream()
                .map(BookingDetailBuilder.class::cast)
                .collect(toList());

        BookingDetailBuilder bookingDetailBuilder = storedBookingDetailBuilders.stream()
                .filter(builder -> builder.getBookingBasicInfoBuilder().getId() == Long.parseLong(bookingId))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No stored booking found for " + bookingId));

        List<FeeContainerBuilder> feeContainerBuilders = feeContainerProperties.stream()
                .map(this::convertToFeeContainerBuilder)
                .collect(toList());

        feeContainerBuilders.addAll(bookingDetailBuilder.getAllFeeContainers());
        bookingDetailBuilder.allFeeContainers(feeContainerBuilders);

        bookingApiServiceWorld.updateMockedResponseEntities(BookingApiServiceEndpointName.GET_BOOKING, storedBookingDetailBuilders);
    }

    private void generateAndAddFeeBuildersToFeeContainerInStoredBooking(String feeContainerId, String bookingId, List<Map<String, String>> feeProperties) {
        List<BookingDetailBuilder> storedBookingDetailBuilders = bookingApiServiceWorld.getStoredMockResponsesForEndpoint(BookingApiServiceEndpointName.GET_BOOKING).stream()
                .map(BookingDetailBuilder.class::cast)
                .collect(toList());

        BookingDetailBuilder bookingDetailBuilder = storedBookingDetailBuilders.stream()
                .filter(builder -> builder.getBookingBasicInfoBuilder().getId() == Long.parseLong(bookingId))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No stored booking found for " + bookingId));

        FeeContainerBuilder storedFeeContainerBuilder = bookingDetailBuilder.getAllFeeContainers().stream()
                .filter(feeContainer -> feeContainer.getId() == Long.parseLong(feeContainerId))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("No feeContainer with id %s found in stored booking with id %s", feeContainerId, bookingId)));

        String bookingCurrencyCode = bookingDetailBuilder.getCurrencyCode();

        List<FeeBuilder> feeBuilders = feeProperties.stream()
                .map(this::convertToFeeBuilder)
                .peek(fee -> fee.currency(bookingCurrencyCode))
                .collect(toList());

        storedFeeContainerBuilder.fees(feeBuilders);
        bookingApiServiceWorld.updateMockedResponseEntities(BookingApiServiceEndpointName.GET_BOOKING, storedBookingDetailBuilders);
    }

}
