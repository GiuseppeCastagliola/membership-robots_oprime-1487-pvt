package com.odigeo.membership.robots.steps;

import com.google.inject.Inject;
import com.odigeo.membership.robots.mocks.InspectableWorld;
import com.odigeo.membership.robots.mocks.WorldName;
import com.odigeo.membership.robots.mocks.bookingapiservice.BookingApiServiceWorld;
import com.odigeo.membership.robots.mocks.bookingsearchapiservice.BookingSearchApiServiceWorld;
import com.odigeo.membership.robots.mocks.membershipofferservice.MembershipOfferServiceWorld;
import com.odigeo.membership.robots.mocks.membershipsearchapi.MembershipSearchApiWorld;
import com.odigeo.membership.robots.mocks.membershipservice.MembershipServiceWorld;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertTrue;

@ScenarioScoped
public class GenericSteps {

    private final Map<WorldName, InspectableWorld> worlds = new EnumMap<>(WorldName.class);

    @Inject
    public GenericSteps(MembershipServiceWorld membershipServiceWorld, BookingApiServiceWorld bookingApiServiceWorld,
                        MembershipSearchApiWorld membershipSearchApiWorld, BookingSearchApiServiceWorld bookingSearchApiServiceWorld,
                        MembershipOfferServiceWorld membershipOfferServiceWorld) {
        worlds.put(WorldName.MEMBERSHIP, membershipServiceWorld);
        worlds.put(WorldName.BOOKING_API, bookingApiServiceWorld);
        worlds.put(WorldName.BOOKING_SEARCH_API, bookingSearchApiServiceWorld);
        worlds.put(WorldName.MEMBERSHIP_SEARCH_API, membershipSearchApiWorld);
        worlds.put(WorldName.MEMBERSHIP_OFFER, membershipOfferServiceWorld);
    }

    @And("^the (\\w+) endpoint of (\\w+) service was called (\\d+) time[s]?$")
    public void checkServiceCalledNTimes(String endpoint, String service, Integer times) {
        WorldName worldName = WorldName.valueOf(service);
        assertTrue(worlds.get(worldName).wasCalled(endpoint, times));
    }

    /* final group in the regex, ([\w\s-\.]+),  matches letters, digits, whitespace, dash/minus, and period/decimal point */
    @And("^the (\\w+) endpoint of (\\w+) service was called with (\\w+) value \"([\\w\\s-.]+)\"$")
    public void checkServiceCalledWithValue(String endpoint, String service, String rule, String expectedValue) {
        WorldName worldName = WorldName.valueOf(service);
        assertTrue(worlds.get(worldName).validateAnyReceivedRequestWithValue(endpoint, rule, expectedValue));
    }

    @And("^the (\\w+) endpoint of (\\w+) service was called with (\\w+) value \"([\\w\\s-.]+)\" (\\d+) time[s]?$")
    public void checkServiceCalledWithValueNTimes(String endpoint, String service, String rule, String expectedValue, Integer times) {
        WorldName worldName = WorldName.valueOf(service);
        assertTrue(worlds.get(worldName).validateReceivedRequestsWithValueMatchCount(endpoint, rule, expectedValue, times));
    }

    @And("^the (\\w+) endpoint of (\\w+) service was never called$")
    public void checkServiceNotCalled(String endpoint, String service) {
        checkServiceCalledNTimes(endpoint, service, 0);
    }

    @Given("^(\\w+) endpoint of (\\w+) service will throw an exception$")
    public void nextCallToEndpointThrowsException(String endpoint, String service) {
        WorldName worldName = WorldName.valueOf(service);
        //SEARCH_MEMBERSHIPS is called 6 times per execution
        int callCount = "SEARCH_MEMBERSHIPS".equals(endpoint) ? 6 : 1;
        for (int i = 0; i < callCount; i++) {
            worlds.get(worldName).throwExceptionForNextCallToEndpoint(endpoint);
        }
    }

    @And("^after (\\d+) seconds$")
    public void afterSeconds(int seconds) throws InterruptedException {
        TimeUnit.SECONDS.sleep(seconds);
    }
}
