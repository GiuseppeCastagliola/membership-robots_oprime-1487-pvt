package com.odigeo.membership.robots.mocks.membershipofferservice;

import com.odigeo.membership.robots.mocks.EndpointName;

public enum MembershipOfferServiceEndpointName implements EndpointName {
    GET_OFFER
}
