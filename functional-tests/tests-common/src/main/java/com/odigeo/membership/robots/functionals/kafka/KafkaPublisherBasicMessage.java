package com.odigeo.membership.robots.functionals.kafka;

import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.commons.messaging.KafkaPublisher;
import com.odigeo.commons.messaging.PublishMessageException;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;

import java.util.concurrent.TimeUnit;

import static java.util.Objects.nonNull;

public abstract class KafkaPublisherBasicMessage {

    protected abstract KafkaPublisher<BasicMessage> getPublisher();
    protected abstract Logger getLogger();
    protected abstract long getKafkaPublisherWaitTime();

    public void sendMessage(BasicMessage basicMessage) {
        try {
            getPublisher().publish(basicMessage, this::getCallBack);
            getLogger().info("Booking id {} sent, going to sleep {} seconds", basicMessage.getKey(), getKafkaPublisherWaitTime());
            TimeUnit.SECONDS.sleep(getKafkaPublisherWaitTime());
        } catch (InterruptedException | PublishMessageException e) {
            getLogger().error("Publish {} Failed", basicMessage.getKey(), e);
        }
    }

    private void getCallBack(RecordMetadata recordMetadata, Exception e) {
        if (nonNull(e)) {
            getLogger().error("Publisher callback error", e);
        }
        getLogger().info("RecordMetadata: Topic {} Offset {} Partition {}", recordMetadata.topic(), recordMetadata.offset(), recordMetadata.partition());
    }

}
