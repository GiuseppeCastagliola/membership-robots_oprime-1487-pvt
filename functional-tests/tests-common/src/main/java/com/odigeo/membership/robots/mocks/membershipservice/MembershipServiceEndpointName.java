package com.odigeo.membership.robots.mocks.membershipservice;

import com.odigeo.membership.robots.mocks.EndpointName;

public enum MembershipServiceEndpointName implements EndpointName {
    CREATE_PENDING_TO_COLLECT,
    EXPIRE_MEMBERSHIP,
    CONSUME_MEMBERSHIP_REMNANT_BALANCE,
    INSERT_RECURRING_ID,
    ACTIVATE_MEMBERSHIP,
    DEACTIVATE_MEMBERSHIP,
    GET_BOOKING_TRACKING,
    DELETE_BOOKING_TRACKING,
    SAVE_BOOKING_TRACKING,
    ENABLE_AUTO_RENEWAL
}
