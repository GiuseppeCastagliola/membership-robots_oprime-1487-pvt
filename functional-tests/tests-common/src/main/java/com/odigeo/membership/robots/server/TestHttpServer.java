package com.odigeo.membership.robots.server;

import com.odigeo.commons.test.bootstrap.JettyServer;
import org.apache.log4j.Logger;

abstract class TestHttpServer {
    private static final Logger LOGGER = Logger.getLogger(JaxRsServiceHttpServer.class);
    private static final int MAX_NUM_OF_RETRIES = 6;
    final int port;
    JettyServer server;

    TestHttpServer(int port) {
        this.port = port;
    }

    void checkedStop() throws ServerStopException {
        if (server != null) {
            int doneRetries = 0;
            while (server.getLocalPort() == port && doneRetries < MAX_NUM_OF_RETRIES) {
                LOGGER.warn("Retrying the stop of the server with port " + port);
                server.stop();
                doneRetries++;
            }
            if (server.getLocalPort() == port) {
                throw new ServerStopException("The server with port " + port + " could not be stopped");
            }
        }
    }
}
