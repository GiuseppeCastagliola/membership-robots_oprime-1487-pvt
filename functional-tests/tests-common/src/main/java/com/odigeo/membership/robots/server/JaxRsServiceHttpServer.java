package com.odigeo.membership.robots.server;

import com.odigeo.commons.test.bootstrap.JettyServer;
import com.odigeo.commons.test.bootstrap.ServerBuilder;
import com.odigeo.commons.test.bootstrap.rest.ApplicationContext;
import com.odigeo.commons.test.bootstrap.rest.JaxRsApplication;
import org.apache.log4j.Logger;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;

import java.net.BindException;
import java.util.Objects;

public class JaxRsServiceHttpServer extends TestHttpServer {
    private static final Logger LOGGER = Logger.getLogger(JaxRsServiceHttpServer.class);
    private static final int MAX_RETRIES_SERVER = 5;
    private final String path;
    private int retries;

    public JaxRsServiceHttpServer(String path, int port) {
        super(port);
        this.path = path;
    }

    private ServletContextHandler createServletHandler(Object serviceToinject) {
        ServletContextHandler contextHandler = new ServletContextHandler();
        contextHandler.setInitParameter("resteasy.scan", "false");
        contextHandler.setContextPath(path);
        ServletHolder servlet = new ServletHolder(new HttpServletDispatcher());
        servlet.setInitParameter("javax.ws.rs.Application", JaxRsApplication.class.getName());
        contextHandler.addServlet(servlet, "/*");
        JaxRsApplication.setApplicationContext(contextHandler.getServletContext(), new ApplicationContext().addSingleton(serviceToinject));
        return contextHandler;
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    public void startServer() throws ServerStopException {
        checkedStop();
        server = (JettyServer) new ServerBuilder().port(port).listenInAllInterfaces(true).stopAtShutdown(true).build();
        try {
            server.start();
        } catch (RuntimeException e) {
            retryStartServer(e);
            throw e;
        }
    }


    public boolean serverNotCreated() {
        return Objects.isNull(server);
    }

    public void addService(Object service) {
        if (server != null) {
            server.addAndStartHandler(createServletHandler(service));
        }
    }

    public void clearServices() {
        if (server != null) {
            server.clearHandlers();
        }
    }

    @SuppressWarnings("PMD.AvoidCatchingGenericException")
    private void retryStartServer(Exception ex) {
        if (ex.getCause() instanceof BindException) {
            LOGGER.error("Retrying start server...");
            while (retries < MAX_RETRIES_SERVER) {
                if (server != null) {
                    try {
                        retries++;
                        server.stop();
                        server.start();
                    } catch (RuntimeException e) {
                        LOGGER.error("Number of retries: " + retries);
                    }
                }
            }
        }
    }
}
