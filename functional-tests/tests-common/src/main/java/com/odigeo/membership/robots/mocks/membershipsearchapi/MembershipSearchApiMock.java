package com.odigeo.membership.robots.mocks.membershipsearchapi;

import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.search.MemberAccountSearchRequest;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MemberAccountResponse;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.robots.mocks.MockException;
import com.odigeo.membership.robots.mocks.ServiceMock;
import com.odigeo.membership.robots.utils.FunctionalUtils;
import com.odigeo.membership.robots.utils.requestprocessors.SearchMembershipsProcessor;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.odigeo.membership.robots.mocks.membershipsearchapi.MembershipSearchApiEndpointName.GET_MEMBERSHIP;
import static com.odigeo.membership.robots.mocks.membershipsearchapi.MembershipSearchApiEndpointName.SEARCH_MEMBERSHIPS;

public class MembershipSearchApiMock extends ServiceMock<MembershipSearchApiEndpointName> implements MembershipSearchApi {

    private static final long REQUEST_TIMEOUT = 500L;

    @Override
    public MemberAccountResponse getMemberAccount(Long aLong, boolean b) throws MembershipServiceException {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public List<MemberAccountResponse> searchAccounts(MemberAccountSearchRequest memberAccountSearchRequest) throws MembershipServiceException {
        throw new UnsupportedOperationException(MockException.NO_MOCKED_METHOD);
    }

    @Override
    public MembershipResponse getMembership(Long membershipId, boolean withMemberAccount) throws MembershipServiceException {
        List<Object> memberships = saveRequestAndFindResponseEntities(membershipId, GET_MEMBERSHIP);
        if (isExceptionResponse(GET_MEMBERSHIP)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }
        return memberships.stream().map(MembershipResponse.class::cast).findFirst().orElse(null);
    }

    @Override
    public List<MembershipResponse> searchMemberships(MembershipSearchRequest membershipSearchRequest) throws MembershipServiceException {
        List<Object> memberships = saveRequestAndFindResponseEntities(membershipSearchRequest, SEARCH_MEMBERSHIPS);

        if (isExceptionResponse(SEARCH_MEMBERSHIPS)) {
            FunctionalUtils.triggerRequestTimeout(REQUEST_TIMEOUT);
        }

        return memberships.stream()
                .map(MembershipResponse.class::cast)
                .filter(membership -> SearchMembershipsProcessor.process(membership, membershipSearchRequest))
                .map(membership -> addMemberAccount(membershipSearchRequest, membership))
                .collect(Collectors.toList());
    }

    private MembershipResponse addMemberAccount(MembershipSearchRequest membershipSearchRequest, MembershipResponse membership) {

        /* Currently this block is never executed as the SearchMembershipsDTOs created in ExternalModuleManagerBean do not set withMemberAccount() */
        if (membershipSearchRequest.isWithMemberAccount()) {
            membership.setMemberAccount(
                    Optional.ofNullable(silentGetMemberAccount(membership.getMemberAccountId()))
                            .orElseThrow(() -> new IllegalStateException("No Member Account found for Membership " + membership.getId()))
            );
        }
        return membership;
    }

    private MemberAccountResponse silentGetMemberAccount(Long id) {
        return findResponseEntities(MembershipSearchApiEndpointName.SEARCH_ACCOUNTS).stream()
                .map(MemberAccountResponse.class::cast)
                .filter(memberAccount -> memberAccount.getId() == id)
                .findFirst()
                .orElse(null);
    }
}
