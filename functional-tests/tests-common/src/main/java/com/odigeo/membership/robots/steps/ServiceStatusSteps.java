package com.odigeo.membership.robots.steps;

import com.google.inject.Inject;
import com.odigeo.membership.robots.ServerConfiguration;
import com.odigeo.membership.robots.mocks.MockException;
import com.odigeo.membership.robots.utils.FunctionalUtils.RobotAdminOperations;
import com.odigeo.membership.robots.utils.FunctionalUtils.RobotJobStatus;
import com.odigeo.technology.docker.ContainerException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;

import static com.odigeo.membership.robots.utils.FunctionalUtils.RobotAdminOperations.EXECUTE_ONCE;
import static com.odigeo.membership.robots.utils.FunctionalUtils.RobotAdminOperations.GET_ROBOT_STATUS;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.testng.Assert.assertEquals;


@ScenarioScoped
public class ServiceStatusSteps {

    private static final Logger LOGGER = Logger.getLogger(ServiceStatusSteps.class);
    private static final long DEFAULT_TIMEOUT = 380L;
    private static final String BOOKING_UPDATES_CONSUMER = "BookingUpdatesConsumer";

    private final ServerConfiguration serverConfiguration;
    private String responseContent;
    private int statusCode;

    @Inject
    public ServiceStatusSteps(ServerConfiguration serverConfiguration) {
        this.serverConfiguration = serverConfiguration;
    }

    @When("^a call to the (.*) url is done$")
    public void callEngineeringCheckUrl(String resource) throws IOException {
        try {
            executeHttpRequest(buildUri(resource));
        } catch (ContainerException e) {
            LOGGER.error("Error getting dockerHost from DockerHostRetriever", e);
        }
    }


    @When("^the (\\w+) is executed")
    public void runRobot(String robotName) throws IOException, InterruptedException {
        callRobotDashboard(robotName, EXECUTE_ONCE, BOOKING_UPDATES_CONSUMER.equals(robotName) ? RobotJobStatus.RUNNING.name() : RobotJobStatus.STOPPED.name());
    }

    @When("^the (\\w+) is shutdown")
    public void shutdownRobot(String robotName) throws IOException, InterruptedException {
        callRobotDashboard(robotName, RobotAdminOperations.SHUTDOWN_ROBOTS, "OK");
    }

    @When("^the (Membership(?:Expiry)Robot) is restarted")
    public void restartRobot(String robotName) throws IOException, InterruptedException {
        callRobotDashboard(robotName, RobotAdminOperations.RESTART_ROBOTS, "OK");
    }

    @Then("^the (\\w+) status is (RUNNING|SCHEDULED|STOPPED)")
    public void checkRobotStatus(String robotName, String robotStatus) throws IOException {
        try {
            executeHttpRequest(buildRobotUri(GET_ROBOT_STATUS, robotName));
            checkResponseContent(robotStatus);
        } catch (ContainerException e) {
            LOGGER.error("Error getting dockerHost from DockerHostRetriever", e);
        }
    }

    @Then("^the response has status code (\\d+)$")
    public void checkHealthCheckResponseStatus(int expectedStatusCode) {
        assertEquals(statusCode, expectedStatusCode);
    }

    @Then("^the response content is (\\w+)$")
    public void checkResponseContent(String expectedResponseContent) {
        assertEquals(responseContent, expectedResponseContent);
    }

    private void callRobotDashboard(String robotName, RobotAdminOperations operation, String expectedResponseContent) throws IOException, InterruptedException {
        Instant start = Instant.now();
        try {
            executeHttpRequest(buildRobotUri(operation, robotName));
            if (HTTP_OK != statusCode) {
                throw new MockException();
            } else {
                while (!expectedResponseContent.equals(responseContent)) {
                    if (Duration.between(start, Instant.now()).getSeconds() >= DEFAULT_TIMEOUT) {
                        throw new MockException();
                    }
                    Thread.sleep(200);
                    executeHttpRequest(buildRobotUri(GET_ROBOT_STATUS, robotName));
                }
            }
        } catch (ContainerException e) {
            LOGGER.error("Error getting dockerHost from DockerHostRetriever", e);
        }
    }

    private void executeHttpRequest(String resource) throws IOException {
        statusCode = -1;
        responseContent = "";
        try (CloseableHttpResponse response = HttpClients.createDefault().execute(new HttpGet(resource))) {
            statusCode = response.getStatusLine().getStatusCode();
            responseContent = IOUtils.toString(response.getEntity().getContent(), String.valueOf(StandardCharsets.UTF_8)).trim();
        }
    }

    private String buildUri(String resource) throws ContainerException {
        return "http://"
                + serverConfiguration.getServer()
                + ServerConfiguration.ENGINEERING_CONTEXT
                + resource;
    }

    private String buildRobotUri(RobotAdminOperations operation, String robotName) throws ContainerException {
        return buildUri(String.format("robots/RobotsAdmin/%s?robotId=%s", operation.getOperationName(), robotName));
    }
}
