package com.odigeo.membership.robots.utils.requestprocessors;

import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MembershipResponse;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.util.Objects.isNull;

public class SearchMembershipsProcessor {

    public static boolean process(MembershipResponse entity, MembershipSearchRequest request) {

        return Optional.of(entity)
                .filter(e -> doesNotExpireAfter(e, request.getToExpirationDate()))
                .filter(e -> doesNotExpireBefore(e, request.getFromExpirationDate()))
                .filter(e -> isNotActivatedBefore(e, request.getFromActivationDate()))
                .filter(e -> statusEquals(e, request.getStatus()))
                .filter(e -> balanceNotLessThan(e, request.getMinBalance()))
                .filter(e -> autoRenewalStatusEquals(e, request.getAutoRenewal()))
                .filter(e -> memberAccountIdEquals(e, request.getMemberAccountId()))
                .isPresent();
    }

    private static boolean doesNotExpireAfter(MembershipResponse responseEntity, String toExpirationDate) {
        return isNull(toExpirationDate) || isNull(responseEntity.getExpirationDate())
                || !LocalDateTime.parse(responseEntity.getExpirationDate()).toLocalDate().isAfter(LocalDate.parse(toExpirationDate));
    }

    private static boolean doesNotExpireBefore(MembershipResponse responseEntity, String fromExpirationDate) {
        return isNull(fromExpirationDate) || isNull(responseEntity.getExpirationDate())
                || !LocalDateTime.parse(responseEntity.getExpirationDate()).toLocalDate().isBefore(LocalDate.parse(fromExpirationDate));
    }

    private static boolean isNotActivatedBefore(MembershipResponse responseEntity, String fromActivationDate) {
        return isNull(fromActivationDate)
                || !LocalDateTime.parse(responseEntity.getActivationDate()).toLocalDate().isBefore(LocalDate.parse(fromActivationDate));
    }

    private static boolean statusEquals(MembershipResponse responseEntity, String status) {
        return isNull(status)
                || status.equalsIgnoreCase(responseEntity.getStatus());
    }

    private static boolean balanceNotLessThan(MembershipResponse requestEntity, BigDecimal minBalance) {
        return isNull(minBalance)
                || requestEntity.getBalance().compareTo(minBalance) > -1;
    }

    private static boolean autoRenewalStatusEquals(MembershipResponse responseEntity, String autoRenewalStatus) {
        return isNull(autoRenewalStatus)
                || autoRenewalStatus.equalsIgnoreCase(responseEntity.getAutoRenewal());
    }

    private static boolean memberAccountIdEquals(MembershipResponse responseEntity, Long memberAccountId) {
        return isNull(memberAccountId)
                || memberAccountId.equals(responseEntity.getMemberAccountId());
    }
}
