package com.odigeo.membership.robots.mocks.bookingsearchapiservice;

import com.odigeo.membership.robots.mocks.EndpointName;

public enum BookingSearchApiServiceEndpointName implements EndpointName {
    SEARCH_BOOKINGS
}
