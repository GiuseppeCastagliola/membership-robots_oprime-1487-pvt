package com.odigeo.membership.robots.mocks.membershipofferservice;

import com.google.inject.Singleton;
import com.odigeo.membership.robots.mocks.ServiceServer;

@Singleton
public class MembershipOfferServiceServer extends ServiceServer {

    private static final String MEMBERSHIP_OFFER_PATH = "/membership-offer-services";
    private static final int MEMBERSHIP_OFFER_PORT = 2333;

    public MembershipOfferServiceServer() {
        super(MEMBERSHIP_OFFER_PATH, MEMBERSHIP_OFFER_PORT);
    }
}
