package com.odigeo.membership.robots.mocks;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import com.google.inject.Inject;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;

public class ServiceMock<T extends EndpointName> {
    private static final Logger LOGGER = Logger.getLogger(ServiceMock.class);
    private final Multimap<T, Object> receivedRequests;
    private final Multimap<T, BiFunction<Integer, Object, Object>> mockedResponseGenerators;

    @Inject
    private ResponseMocks responseMocks;

    public ServiceMock() {
        receivedRequests = LinkedListMultimap.create();
        mockedResponseGenerators = LinkedListMultimap.create();
    }

    public static BiFunction<Integer, Object, Object> alwaysReturn(final Object response) {
        return (n, o) -> response;
    }

    Collection<Object> getReceivedRequest(T endpointName) {
        return receivedRequests.get(endpointName);
    }

    boolean wasCalled(T endpointName, int times) {
        LOGGER.info("receivedRequests.get(endpointName).size(); " + receivedRequests.get(endpointName).size() + " expected: " + times);
        return receivedRequests.get(endpointName).size() == times;
    }

    void addMockedResponseGenerator(T endpointName, BiFunction<Integer, Object, Object> responseGenerator) {
        mockedResponseGenerators.put(endpointName, responseGenerator);
    }

    protected <U> void addMockedResponseEntities(EndpointName endpointName, Iterable<U> responseEntities) {
        responseMocks.putAll(endpointName, responseEntities);
    }

    protected <U> void updateMockedResponseEntities(EndpointName endpointName, Iterable<U> responseEntities) {
        List<Object> replacedMocks = responseMocks.updateEntities(endpointName, responseEntities);
        if (replacedMocks.isEmpty()) {
            LOGGER.debug("No existing mockedEntities found for endpoint " + endpointName + " in " + this.getClass());
        }
    }

    <U> void addMockedErrorResponse(EndpointName endpointName, Long membershipId, U errorResponse) {
        responseMocks.putErrorResponse(endpointName, membershipId, errorResponse);
    }

    void throwExceptionForNextCallToEndpoint(T endpointName) {
        responseMocks.throwExceptionForNextCallToEndpoint(endpointName);
    }

    protected boolean isExceptionResponse(EndpointName endpointName) {
        return responseMocks.isExceptionResponse(endpointName);
    }

    protected Optional<Object> findMockedErrorResponse(EndpointName endpointName, Long membershipId) {
        return responseMocks.getErrorResponse(endpointName, membershipId);
    }

    protected List<Object> saveRequestAndFindResponseEntities(Object request, T endpointName) {
        saveRequest(endpointName, request);
        return findResponseEntities(endpointName);
    }

    protected void saveRequest(T endpointName, Object request) {
        receivedRequests.put(endpointName, request);
    }

    protected List<Object> findResponseEntities(EndpointName endpointName) {
        return new ArrayList<>(responseMocks.getMockedResponseEntities(endpointName));
    }

    protected void deleteMockedResponseEntities(EndpointName endpointName) {
        responseMocks.deleteMockedEntities(endpointName);
    }

    /*
     * The following 2 methods were inherited from Functionals in membership-renewal module.
     * Not currently used here, but have left them in case responseGenerators are utilised for this module in the future
     */
    protected Object saveRequestAndFindResponse(Object request, T endpointName) {
        LOGGER.debug("--> Received call to " + endpointName + " in " + this.getClass().getSimpleName() + " with request " + request);
        int numberOfRequestsReceived = receivedRequests.get(endpointName).size();
        receivedRequests.put(endpointName, request);
        Collection<BiFunction<Integer, Object, Object>> responseGenerators = mockedResponseGenerators.get(endpointName);
        if (responseGenerators.isEmpty()) {
            LOGGER.info("\t(!) Not found mocked response for " + endpointName + ". Returning null");
        }
        Object response = responseGenerators.stream()
                .map(generator -> generator.apply(numberOfRequestsReceived, request))
                .reduce(null, (o, o2) -> o2 != null ? o2 : o);
        LOGGER.debug("<-- Mocked response " + response);
        return response;
    }

    void replaceMockedResponseGenerator(T endpointName, BiFunction<Integer, Object, Object> responseGenerator) {
        mockedResponseGenerators.removeAll(endpointName);
        mockedResponseGenerators.put(endpointName, responseGenerator);
    }

}
