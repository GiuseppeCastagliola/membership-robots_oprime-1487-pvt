package com.odigeo.membership.robots.steps;

import com.google.inject.Inject;
import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.membership.robots.functionals.kafka.BookingUpdatesKafkaPublisher;
import com.odigeo.membership.robots.functionals.kafka.ChargebackMovementStoredKafkaPublisher;
import com.odigeo.membership.robots.utils.FunctionalUtils;
import cucumber.api.DataTable;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Map;

@ScenarioScoped
public class KafkaSteps {

    private static final String BOOKING_ID = "bookingId";
    private static final String STATUS_MOVEMENT = "status";
    private static final String ACTION_MOVEMENT = "action";

    private final BookingUpdatesKafkaPublisher bookingUpdatesKafkaPublisher;
    private final ChargebackMovementStoredKafkaPublisher chargebackMovementStoredKafkaPublisher;

    @Inject
    public KafkaSteps(BookingUpdatesKafkaPublisher bookingUpdatesKafkaPublisher, ChargebackMovementStoredKafkaPublisher chargebackMovementStoredKafkaPublisher) {
        this.bookingUpdatesKafkaPublisher = bookingUpdatesKafkaPublisher;
        this.chargebackMovementStoredKafkaPublisher = chargebackMovementStoredKafkaPublisher;
    }

    @When("following booking ids are published in BOOKING_UPDATES_v1")
    public void publishBookingId(DataTable bookingIds) {
        bookingIds.asList(String.class).forEach(bookingId -> bookingUpdatesKafkaPublisher.sendMessage(createBookingUpdateMessage(bookingId)));
    }

    @When("following movement with action chargeback is published in CHARGEBACK_MOVEMENT_STORED")
    public void publishMovementChargeback(DataTable movement) {
        Map<String, String> movementProperties = FunctionalUtils.asMapWithNullsAndEmptyStrings(movement);
        chargebackMovementStoredKafkaPublisher.sendMessage(createChargebackMovementStoreMessage(movementProperties));
    }

    private BasicMessage createChargebackMovementStoreMessage(Map<String, String> movementProperties) {
        BasicMessage basicMessage = new BasicMessage();
        basicMessage.setKey(movementProperties.get(BOOKING_ID));
        basicMessage.addExtraParameter(BOOKING_ID, movementProperties.get(BOOKING_ID));
        basicMessage.addExtraParameter(STATUS_MOVEMENT, movementProperties.get(STATUS_MOVEMENT));
        basicMessage.addExtraParameter(ACTION_MOVEMENT, movementProperties.get(ACTION_MOVEMENT));
        return basicMessage;
    }

    private BasicMessage createBookingUpdateMessage(String bookingId) {
        BasicMessage message = new BasicMessage();
        message.setKey(bookingId);
        return message;
    }
}
