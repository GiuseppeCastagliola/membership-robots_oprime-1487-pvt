package com.odigeo.membership.robots.mocks;

import com.odigeo.membership.robots.server.JaxRsServiceHttpServer;
import com.odigeo.membership.robots.server.ServerStopException;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public abstract class InspectableWorld<T extends ServiceMock<M>, S extends JaxRsServiceHttpServer, M extends EndpointName> {

    private static final Logger LOGGER = Logger.getLogger(InspectableWorld.class);
    private final T serviceMock;
    private final S serviceServer;
    private final Map<String, Predicate<Object>> serviceValidators = new HashMap<>();
    private final Map<String, BiPredicate<Object, Object>> serviceValueValidators = new HashMap<>();

    public InspectableWorld(T serviceMock, S serviceServer) {
        this.serviceMock = serviceMock;
        this.serviceServer = serviceServer;
    }

    public void install() throws ServerStopException {
        LOGGER.debug("Installing world " + this.getClass());
        if (this.serviceServer.serverNotCreated()) {
            this.serviceServer.startServer();
        }
        this.serviceServer.addService(this.serviceMock);
    }

    public void uninstall() {
        if (Objects.nonNull(this.serviceServer)) {
            this.serviceServer.clearServices();
        }
        for (EndpointName endpointName : findEndpointNames()) {
            this.serviceMock.deleteMockedResponseEntities(endpointName);
        }
    }

    private Predicate<Object> getValidatorFor(String rule) {
        Predicate<Object> foundValidator = serviceValidators.get(rule);
        LOGGER.debug("Selecting validator " + foundValidator + " for rule: " + rule);
        if (foundValidator == null) {
            LOGGER.info("\t(!) Not found validator for rule " + rule);
            foundValidator = o -> false;
        }
        return foundValidator;
    }

    protected void addValidatorFor(String rule, Predicate<Object> validator) {
        serviceValidators.put(rule, validator);
    }

    private BiPredicate<Object, Object> getValueValidatorFor(String rule) {
        BiPredicate<Object, Object> foundValidator = serviceValueValidators.get(rule);
        LOGGER.debug("Selecting validator " + foundValidator + " for rule: " + rule);
        if (foundValidator == null) {
            LOGGER.info("\t(!) Not found validator for rule " + rule);
            foundValidator = (o, p) -> false;
        }
        return foundValidator;
    }

    protected void addValueValidatorFor(String rule, BiPredicate<Object, Object> valueValidator) {
        serviceValueValidators.put(rule, valueValidator);
    }

    public void addMockedResponseGenerator(M endpointName, BiFunction<Integer, Object, Object> responseGenerator) {
        this.serviceMock.addMockedResponseGenerator(endpointName, responseGenerator);
    }

    public <U> void addMockedResponseEntities(M endpointName, Iterable<U> responseEntities) {
        this.serviceMock.addMockedResponseEntities(endpointName, responseEntities);
    }

    public <U> void addMockedErrorResponse(M endpointName, Long membershipId, U errorResponse) {
        this.serviceMock.addMockedErrorResponse(endpointName, membershipId, errorResponse);
    }

    public <U> List<Object> getStoredMockResponsesForEndpoint(M endpointName) {
        return this.serviceMock.findResponseEntities(endpointName);
    }

    public <U> void updateMockedResponseEntities(EndpointName endpointName, Iterable<U> responseEntities) {
        this.serviceMock.updateMockedResponseEntities(endpointName, responseEntities);
    }

    private Collection<Object> getReceivedRequest(String endpointName) {
        M endpoint = findEndpointName(endpointName);
        return this.serviceMock.getReceivedRequest(endpoint);
    }

    public boolean wasCalled(String endpointName, int times) {
        M endpoint = findEndpointName(endpointName);
        return this.serviceMock.wasCalled(endpoint, times);
    }

    protected void debugAssert(Object expectedValue, Object actualValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("on " + this + ": expected: " + expectedValue + " actual: " + actualValue);
        }
    }

    public abstract M findEndpointName(String endpointName);

    public abstract M[] findEndpointNames();

    public boolean validateAnyReceivedRequest(String endpointName, String rule) {
        Collection<Object> receivedRequests = this.getReceivedRequest(endpointName);
        return receivedRequests.stream().anyMatch(this.getValidatorFor(rule));
    }

    public boolean validateAnyReceivedRequestWithValue(String endpointName, String rule, String value) {
        Collection<Object> receivedRequests = this.getReceivedRequest(endpointName);
        return receivedRequests.stream().anyMatch(request -> this.getValueValidatorFor(rule).test(request, value));
    }

    public boolean validateReceivedRequestsWithValueMatchCount(String endpointName, String rule, String value, int times) {
        Collection<Object> receivedRequests = this.getReceivedRequest(endpointName);
        return receivedRequests.stream().filter(request -> this.getValueValidatorFor(rule).test(request, value)).count() == times;
    }

    public void throwExceptionForNextCallToEndpoint(String endpointName) {
        M endpoint = findEndpointName(endpointName);
        this.serviceMock.throwExceptionForNextCallToEndpoint(endpoint);
    }
}
