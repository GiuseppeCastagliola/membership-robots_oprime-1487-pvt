package com.odigeo.membership.robots;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.odigeo.commons.test.docker.Artifact;
import com.odigeo.commons.test.docker.ContainerInfoBuilderFactory;
import com.odigeo.commons.test.docker.LogFilesTransfer;
import com.odigeo.technology.docker.ContainerComposer;
import com.odigeo.technology.docker.ContainerException;
import com.odigeo.technology.docker.ContainerInfo;
import com.odigeo.technology.docker.ContainerInfoBuilder;
import com.odigeo.technology.docker.DockerModule;
import com.odigeo.technology.docker.ImageController;
import com.odigeo.technology.docker.SocketPingChecker;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Guice;

@CucumberOptions(plugin = {"pretty", "json:target/cucumber/cucumber.json", "html:target/cucumber/"},
        strict = true)
@Guice(modules = DockerModule.class)
public class ServiceTest extends AbstractTestNGCucumberTests {

    private static final Logger LOGGER = Logger.getLogger(ServiceTest.class);
    private static final int KAFKA_PORT = 9092;
    private static final int ZOOKEEPER_PORT = 2181;
    private static final String LOCAL_IP = "192.168.0.1";
    private static final String DOCKER_SOCK_PATH = "/var/run/docker.sock";

    @Inject
    private ContainerComposer containerComposer;
    @Inject
    private LogFilesTransfer logFilesTransfer;
    @Inject
    private ContainerInfoBuilderFactory containerInfoBuilderFactory;
    @Inject
    private ImageController imageController;

    private Artifact artifact = new Artifact(System.getProperty("module.groupId"), System.getProperty("module.name"), System.getProperty("module.version"));

    @BeforeClass
    public void setUp() throws Exception {
        ConfigurationEngine.init();
        try {
            ContainerInfo zookeeperContainerInfo = new ContainerInfoBuilder("wurstmeister/zookeeper:3.4.6")
                    .setName("zookeeper")
                    .addPortMapping(ZOOKEEPER_PORT, ZOOKEEPER_PORT)
                    .setPingChecker(new SocketPingChecker("localhost", ZOOKEEPER_PORT))
                    .addEvironmentVariable("ZOOKEEPER_CLIENT_PORT", String.valueOf(ZOOKEEPER_PORT))
                    .build();
            ContainerInfo kafkaContainerInfo = new ContainerInfoBuilder("wurstmeister/kafka:2.11-0.9.0.1")
                    .setName("kafka")
                    .addPortMapping(KAFKA_PORT, KAFKA_PORT)
                    .setPingChecker(new SocketPingChecker("localhost", KAFKA_PORT))
                    .addDependency(zookeeperContainerInfo)
                    .addEvironmentVariable("KAFKA_ZOOKEEPER_CONNECT", LOCAL_IP + ":" + ZOOKEEPER_PORT)
                    .addEvironmentVariable("KAFKA_BROKER_ID", "1")
                    .addEvironmentVariable("KAFKA_ADVERTISED_HOST_NAME", LOCAL_IP)
                    .addEvironmentVariable("KAFKA_ADVERTISED_PORT", String.valueOf(KAFKA_PORT))
                    .addVolumeMapping(DOCKER_SOCK_PATH, DOCKER_SOCK_PATH)
                    .build();
            ContainerInfo moduleContainer = containerInfoBuilderFactory
                    .newModule(artifact)
                    .addDependency(kafkaContainerInfo)
                    .build();
            containerComposer.addServiceAndDependencies(moduleContainer)
                    .composeUp(true);
        } catch (ContainerException e) {
            LOGGER.error("error", e);
            logFilesTransfer.copyToExternalFolder(artifact);
            throw e;
        } catch (IllegalArgumentException e) {
            LOGGER.error("error", e);
            throw e;
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        containerComposer.composeDown();
        logFilesTransfer.copyToExternalFolder(artifact);
        imageController.removeFuncionalTestsImage(System.getProperty("module.name"));
    }
}
