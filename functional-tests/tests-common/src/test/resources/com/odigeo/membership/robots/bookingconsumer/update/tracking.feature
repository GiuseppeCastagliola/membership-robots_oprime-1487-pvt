Feature: membership booking tracking logic

  Scenario Outline: manage booking not tracked, after a booking update, if membership is activated:
  - if the booking is CONTRACT and is balanced, save membership booking tracking (membership update balance)
  - if the booking is CONTRACT and is unbalanced, update booking to balance it
  - if the booking is not CONTRACT, nothing to do

    Given the following booking exists:
      | bookingId       | 6653452         |
      | membershipId    | 909090          |
      | memberAccountId | 1111            |
      | currencyCode    | EUR             |
      | bookingStatus   | <bookingStatus> |

    And booking 6653452 has the following feeContainers:
      | feeContainerId | feeContainerType | totalAmount |
      | 3243223        | MEMBERSHIP_PERKS | -28         |

    And feeContainer 3243223 in booking 6653452 has the following fees:
      | label     | subCode | amount        |
      | AVOID_FEE | AB35    | <avoidAmount> |
      | COST_FEE  | AB32    | <costAmount>  |
      | PERKS     | AB34    | <perksAmount> |

    And the following membership exists:
      | name             | Pablo               |
      | lastNames        | Gomez               |
      | userId           | 65000               |
      | membershipId     | 909090              |
      | memberAccountId  | 1111                |
      | membershipStatus | <membershipStatus>  |
      | balance          | <membershipBalance> |

    When the BookingUpdatesConsumer is executed
    And the BookingUpdatesConsumer status is RUNNING
    And following booking ids are published in BOOKING_UPDATES_v1
      | 6653452 |

    Then the GET_BOOKING endpoint of BOOKING_API service was <getBookingCalls>
    And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was <getMembershipCalls>
    And the GET_BOOKING_TRACKING endpoint of MEMBERSHIP service was <getTrackingCalls>
    And the DELETE_BOOKING_TRACKING endpoint of MEMBERSHIP service was <deleteTrackingCalls>
    And the SAVE_BOOKING_TRACKING endpoint of MEMBERSHIP service was <addTrackingCalls>
    And the UPDATE_BOOKING endpoint of BOOKING_API service was <updateBookingCalls>

    Examples:
      | bookingStatus | membershipStatus | membershipBalance | avoidAmount | costAmount | perksAmount | getBookingCalls | getMembershipCalls | getTrackingCalls | deleteTrackingCalls | addTrackingCalls | updateBookingCalls |
      | CONTRACT      | ACTIVATED        | 0                 | 0           | -13        | -25         | called 1 times  | called 1 time      | called 2 time    | never called        | called 1 time    | never called       |
      | CONTRACT      | ACTIVATED        | 10                | -10         | -13        | -25         | called 1 times  | called 1 time      | called 2 time    | never called        | called 1 time    | never called       |
      | CONTRACT      | ACTIVATED        | 0                 | -30         | -1         | -25         | called 1 times  | called 1 time      | called 2 time    | never called        | never called     | called 1 time      |
      | CONTRACT      | ACTIVATED        | 10                | 0           | -13        | -20         | called 1 times  | called 1 time      | called 2 time    | never called        | never called     | called 1 time      |
      | DIDNOTBUY     | ACTIVATED        | 0                 | 0           | -10        | -24         | called 1 times  | called 1 time      | called 1 time    | never called        | never called     | never called       |
      | CONTRACT      | EXPIRED          | 0                 | -10         | -18        | -35         | called 1 times  | called 1 time      | never called     | never called        | never called     | never called       |

  Scenario Outline: Manage an already tracked booking, after a booking update, if membership is activated:
  - booking CONTRACT, fees are not changed, nothing to do
  - booking not in CONTRACT, delete the tracked booking (update membership balance)
  - booking CONTRACT, fees are changed, the booking now had fees amount different from the tracked one in membership,
  in this case the tracked booking will be deleted (membership balance updated) and then if booking is balanced, will be saved in membership (balance updated again)
  otherwise, the booking will be updated with balanced fees amount (and in prod this means that the booking will be processed again because of pull of the message, and so finally tracked)

    Given the following booking exists:
      | bookingId       | 6653452         |
      | membershipId    | 909090          |
      | memberAccountId | 1111            |
      | currencyCode    | EUR             |
      | bookingStatus   | <bookingStatus> |

    And booking 6653452 has the following feeContainers:
      | feeContainerId | feeContainerType | totalAmount |
      | 3243223        | MEMBERSHIP_PERKS | -28         |

    And feeContainer 3243223 in booking 6653452 has the following fees:
      | label     | subCode | amount        |
      | AVOID_FEE | AB35    | <avoidAmount> |
      | COST_FEE  | AB32    | <costAmount>  |
      | PERKS     | AB34    | <perksAmount> |

    And the following membership exists:
      | name             | Pablo               |
      | lastNames        | Gomez               |
      | userId           | 65000               |
      | membershipId     | 909090              |
      | memberAccountId  | 1111                |
      | membershipStatus | <membershipStatus>  |
      | balance          | <membershipBalance> |

    And the following membership booking tracking exists:
      | bookingId      | 6653452              |
      | membershipId   | 909090               |
      | bookingDate    | 2021-01-01T12:45:00  |
      | avoidFeeAmount | <trackedAvoidAmount> |
      | costFeeAmount  | <trackedCostAmount>  |
      | perksAmount    | <trackedPerksAmount> |

    When the BookingUpdatesConsumer is executed
    And the BookingUpdatesConsumer status is RUNNING
    And following booking ids are published in BOOKING_UPDATES_v1
      | 6653452 |

    Then the GET_BOOKING endpoint of BOOKING_API service was <getBookingCalls>
    And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was <getMembershipCalls>
    And the GET_BOOKING_TRACKING endpoint of MEMBERSHIP service was <getTrackingCalls>
    And the DELETE_BOOKING_TRACKING endpoint of MEMBERSHIP service was <deleteTrackingCalls>
    And the SAVE_BOOKING_TRACKING endpoint of MEMBERSHIP service was <addTrackingCalls>
    And the UPDATE_BOOKING endpoint of BOOKING_API service was <updateBookingCalls>

    Examples:
      | bookingStatus | membershipStatus | membershipBalance | trackedAvoidAmount | trackedCostAmount | trackedPerksAmount | avoidAmount | costAmount | perksAmount | getBookingCalls | getMembershipCalls | getTrackingCalls | deleteTrackingCalls | addTrackingCalls | updateBookingCalls |
      | CONTRACT      | ACTIVATED        | 0                 | -10                | -1                | -25                | -10         | -13        | -25         | called 1 times  | called 2 time      | called 2 time    | called 1 time       | called 1 time    | never called       |
      | CONTRACT      | ACTIVATED        | 10                | 0                  | -13               | -25                | -10         | -13        | -25         | called 1 times  | called 2 time      | called 2 time    | called 1 time       | called 1 time    | never called       |
      | CONTRACT      | ACTIVATED        | 0                 | -20                | -5                | -25                | -30         | -1         | -25         | called 1 times  | called 2 time      | called 2 time    | called 1 time       | never called     | called 1 time      |
      | CONTRACT      | ACTIVATED        | 0                 | 0                  | -10               | -24                | 0           | -10        | -24         | called 1 times  | called 1 time      | called 2 time    | never called        | never called     | never called       |
      | DIDNOTBUY     | ACTIVATED        | 0                 | 0                  | -10               | -24                | 0           | -10        | -24         | called 1 times  | called 1 time      | called 1 time    | called 1 time       | never called     | never called       |
      | CONTRACT      | EXPIRED          | 0                 | -1                 | -2                | -33                | -10         | -18        | -35         | called 1 times  | called 1 time      | never called     | never called        | never called     | never called       |