Feature: membership booking tracking logic

  Scenario Outline: manage booking not tracked, after a booking update, rules described in tracking feature,
  in this feature we want to check the retries after an api call failure

    Given the following booking exists:
      | bookingId       | 6653452  |
      | membershipId    | 909090   |
      | memberAccountId | 1111     |
      | currencyCode    | EUR      |
      | bookingStatus   | CONTRACT |

    And booking 6653452 has the following feeContainers:
      | feeContainerId | feeContainerType | totalAmount |
      | 3243223        | MEMBERSHIP_PERKS | -28         |

    And feeContainer 3243223 in booking 6653452 has the following fees:
      | label     | subCode | amount        |
      | AVOID_FEE | AB35    | <avoidAmount> |
      | COST_FEE  | AB32    | <costAmount>  |
      | PERKS     | AB34    | <perksAmount> |

    And the following membership exists:
      | name             | Pablo     |
      | lastNames        | Gomez     |
      | userId           | 65000     |
      | membershipId     | 909090    |
      | memberAccountId  | 1111      |
      | membershipStatus | ACTIVATED |
      | balance          | 0         |

    And <endpoint> endpoint of <service> service will throw an exception

    When the BookingUpdatesConsumer is executed
    And the BookingUpdatesConsumer status is RUNNING
    And following booking ids are published in BOOKING_UPDATES_v1
      | 6653452 |
    And after 1 seconds

    Then the GET_BOOKING endpoint of BOOKING_API service was <getBookingCalls>
    And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was <getMembershipCalls>
    And the GET_BOOKING_TRACKING endpoint of MEMBERSHIP service was <getTrackingCalls>
    And the DELETE_BOOKING_TRACKING endpoint of MEMBERSHIP service was <deleteTrackingCalls>
    And the SAVE_BOOKING_TRACKING endpoint of MEMBERSHIP service was <addTrackingCalls>
    And the UPDATE_BOOKING endpoint of BOOKING_API service was <updateBookingCalls>

    Examples:
      | endpoint              | service               | avoidAmount | costAmount | perksAmount | getBookingCalls | getMembershipCalls | getTrackingCalls | deleteTrackingCalls | addTrackingCalls | updateBookingCalls |
      | GET_BOOKING           | BOOKING_API           | 0           | -13        | -25         | called 2 times  | called 1 time      | called 2 time    | never called        | called 1 time    | never called       |
      | GET_MEMBERSHIP        | MEMBERSHIP_SEARCH_API | 0           | -13        | -25         | called 2 times  | called 2 time      | called 2 time    | never called        | called 1 time    | never called       |
      | GET_BOOKING_TRACKING  | MEMBERSHIP            | 0           | -13        | -25         | called 2 times  | called 2 time      | called 3 time    | never called        | called 1 time    | never called       |
      | SAVE_BOOKING_TRACKING | MEMBERSHIP            | 0           | -13        | -25         | called 2 times  | called 2 time      | called 4 time    | never called        | called 2 time    | never called       |
      | UPDATE_BOOKING        | BOOKING_API           | -30         | -1         | -25         | called 2 times  | called 2 time      | called 4 time    | never called        | never called     | called 2 time      |

  Scenario Outline: Manage an already tracked booking, after a booking update, rules described in tracking feature,
  in this feature we want to check the retries after an api call failure

    Given the following booking exists:
      | bookingId       | 6653452         |
      | membershipId    | 909090          |
      | memberAccountId | 1111            |
      | currencyCode    | EUR             |
      | bookingStatus   | <bookingStatus> |

    And booking 6653452 has the following feeContainers:
      | feeContainerId | feeContainerType | totalAmount |
      | 3243223        | MEMBERSHIP_PERKS | -28         |

    And feeContainer 3243223 in booking 6653452 has the following fees:
      | label     | subCode | amount        |
      | AVOID_FEE | AB35    | <avoidAmount> |
      | COST_FEE  | AB32    | <costAmount>  |
      | PERKS     | AB34    | <perksAmount> |

    And the following membership exists:
      | name             | Pablo     |
      | lastNames        | Gomez     |
      | userId           | 65000     |
      | membershipId     | 909090    |
      | memberAccountId  | 1111      |
      | membershipStatus | ACTIVATED |
      | balance          | 0         |

    And the following membership booking tracking exists:
      | bookingId      | 6653452              |
      | membershipId   | 909090               |
      | bookingDate    | 2021-01-01T12:45:00  |
      | avoidFeeAmount | <trackedAvoidAmount> |
      | costFeeAmount  | <trackedCostAmount>  |
      | perksAmount    | <trackedPerksAmount> |

    And <endpoint> endpoint of <service> service will throw an exception

    When the BookingUpdatesConsumer is executed
    And the BookingUpdatesConsumer status is RUNNING
    And following booking ids are published in BOOKING_UPDATES_v1
      | 6653452 |
    And after 1 seconds

    Then the GET_BOOKING endpoint of BOOKING_API service was <getBookingCalls>
    And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was <getMembershipCalls>
    And the GET_BOOKING_TRACKING endpoint of MEMBERSHIP service was <getTrackingCalls>
    And the DELETE_BOOKING_TRACKING endpoint of MEMBERSHIP service was <deleteTrackingCalls>
    And the SAVE_BOOKING_TRACKING endpoint of MEMBERSHIP service was <addTrackingCalls>
    And the UPDATE_BOOKING endpoint of BOOKING_API service was <updateBookingCalls>

    Examples:
      | endpoint                | service               | bookingStatus | trackedAvoidAmount | trackedCostAmount | trackedPerksAmount | avoidAmount | costAmount | perksAmount | getBookingCalls | getMembershipCalls | getTrackingCalls | deleteTrackingCalls | addTrackingCalls | updateBookingCalls |
      | GET_BOOKING             | BOOKING_API           | CONTRACT      | -10                | -1                | -25                | -10         | -13        | -25         | called 2 times  | called 2 time      | called 2 time    | called 1 time       | called 1 time    | never called       |
      | GET_MEMBERSHIP          | MEMBERSHIP_SEARCH_API | CONTRACT      | -10                | -1                | -25                | -10         | -13        | -25         | called 2 times  | called 3 time      | called 2 time    | called 1 time       | called 1 time    | never called       |
      | GET_BOOKING_TRACKING    | MEMBERSHIP            | CONTRACT      | -10                | -1                | -25                | -10         | -13        | -25         | called 2 times  | called 3 time      | called 3 time    | called 1 time       | called 1 time    | never called       |
      | DELETE_BOOKING_TRACKING | MEMBERSHIP            | CONTRACT      | -10                | -1                | -25                | -10         | -13        | -25         | called 2 times  | called 3 time      | called 3 time    | called 2 time       | called 1 time    | never called       |
      | DELETE_BOOKING_TRACKING | MEMBERSHIP            | DIDNOTBUY     | 0                  | -10               | -24                | 0           | -10        | -24         | called 2 times  | called 2 time      | called 2 time    | called 2 time       | never called     | never called       |
      | UPDATE_BOOKING          | BOOKING_API           | CONTRACT      | -20                | -5                | -25                | -30         | -1         | -25         | called 2 times  | called 3 time      | called 4 time    | called 1 time       | never called     | called 2 time      |
