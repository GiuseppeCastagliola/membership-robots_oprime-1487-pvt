Feature: activate

  Scenario Outline: activate membership only if booking and membership criteria are satisfied
    Given the following booking exists:
      | bookingId                  | 6653452             |
      | membershipId               | 909090              |
      | memberAccountId            | 1111                |
      | isBookingSubscriptionPrime | <isSubscription>    |
      | currencyCode               | EUR                 |
      | bookingStatus              | <bookingStatus>     |
      | subscriptionPrice          | <subscriptionPrice> |

    And the following membership exists:
      | name             | Pablo              |
      | lastNames        | Gomez              |
      | userId           | 65000              |
      | membershipId     | 909090             |
      | memberAccountId  | 1111               |
      | membershipStatus | <membershipStatus> |

    When the BookingUpdatesConsumer is executed
    And the BookingUpdatesConsumer status is RUNNING
    And following booking ids are published in BOOKING_UPDATES_v1
      | 6653452 |

    Then the GET_BOOKING endpoint of BOOKING_API service was <getBookingCalls>
    And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was <getMembershipCalls>
    And the ACTIVATE_MEMBERSHIP endpoint of MEMBERSHIP service was <activateCalls>

    Examples:
      | isSubscription | bookingStatus | subscriptionPrice | membershipStatus    | getBookingCalls | getMembershipCalls | activateCalls |
      | true           | CONTRACT      | 10                | PENDING_TO_ACTIVATE | called 1 time   | called 1 time      | called 1 time |
      | true           | CONTRACT      | 10                | ACTIVATED           | called 1 time   | called 1 time      | never called  |
      | true           | INIT          | 10                | PENDING_TO_ACTIVATE | called 1 time   | never called       | never called  |
      | false          | CONTRACT      | 10                | PENDING_TO_ACTIVATE | called 1 time   | never called       | never called  |

