Feature: Recurring logic, to save the recurring id the booking should:
  - be prime booking
  - be subscription or renewal booking
  - bookingStatus equals to CONTRACT or REQUEST
  - have a collection attempt with recurring id and a validMovement (paid, refunded...)
  and the Membership associated, shouldn't have already a recurring id saved

  Scenario Outline: save recurring only if booking and membership criteria are satisfied
    Given the following booking exists:
      | bookingId                       | 6653452                  |
      | membershipId                    | 909090                   |
      | memberAccountId                 | 1111                     |
      | isBookingSubscriptionPrime      | <isSubscription>         |
      | isRenewalBooking                | <isRenewal>              |
      | currencyCode                    | EUR                      |
      | withRecurringAndValidCollection | <validCollectionAttempt> |
      | bookingStatus                   | <bookingStatus>          |
    And the following booking exists:
      | bookingId       | 6090909 |
      | membershipId    | 0       |
      | memberAccountId | 0       |

    And the following membership exists:
      | name            | Pablo         |
      | lastNames       | Gomez         |
      | membershipId    | 909090        |
      | memberAccountId | 1111          |
      | recurringId     | <recurringId> |

    When the BookingUpdatesConsumer is executed
    And the BookingUpdatesConsumer status is RUNNING
    And following booking ids are published in BOOKING_UPDATES_v1
      | 6653452 |
      | 6090909 |

    Then the GET_BOOKING endpoint of BOOKING_API service was <getBookingCalls>
    And the GET_MEMBERSHIP endpoint of MEMBERSHIP_SEARCH_API service was <getMembershipCalls>
    And the INSERT_RECURRING_ID endpoint of MEMBERSHIP service was <saveRecurringCalls>
    And the INSERT_RECURRING_ID endpoint of MEMBERSHIP service was called with membershipIdRecurringAdd value "909090" <recurringRequestTimes> times

    Examples:
      | bookingStatus | validCollectionAttempt | isSubscription | isRenewal | recurringId | getBookingCalls | getMembershipCalls | saveRecurringCalls | recurringRequestTimes |
      | CONTRACT      | true                   | true           | false     | NULL_VALUE  | called 2 times  | called 1 time      | called 1 time      | 1                     |
      | REQUEST       | true                   | false          | true      | NULL_VALUE  | called 2 times  | called 1 time      | called 1 time      | 1                     |
      | INIT          | true                   | true           | false     | NULL_VALUE  | called 2 times  | never called       | never called       | 0                     |
      | REQUEST       | true                   | false          | false     | NULL_VALUE  | called 2 times  | never called       | never called       | 0                     |
      | CONTRACT      | false                  | false          | true      | NULL_VALUE  | called 2 times  | never called       | never called       | 0                     |
      | REQUEST       | true                   | false          | true      | REC_ID123   | called 2 times  | called 1 time      | never called       | 0                     |
