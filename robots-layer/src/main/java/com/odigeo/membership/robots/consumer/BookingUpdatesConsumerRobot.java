package com.odigeo.membership.robots.consumer;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.commons.messaging.Consumer;
import com.odigeo.commons.messaging.JsonDeserializer;
import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.robots.AbstractRobot;
import com.odigeo.robots.RobotExecutionResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BookingUpdatesConsumerRobot extends AbstractRobot {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingUpdatesConsumerRobot.class);
    private static final String BOOKING_UPDATES_TOPIC = "BOOKING_UPDATES_v1";
    private static final int NUMBER_OF_THREADS = 1;

    private final Consumer<BasicMessage> consumer;
    private final BookingUpdatesMessageProcessor bookingUpdatesMessageProcessor;

    @Inject
    public BookingUpdatesConsumerRobot(BookingUpdatesMessageProcessor bookingUpdatesMessageProcessor, @Assisted String robotId) {
        super(robotId);
        this.consumer = new KafkaConsumer<>(new JsonDeserializer<>(BasicMessage.class), BOOKING_UPDATES_TOPIC);
        this.bookingUpdatesMessageProcessor = bookingUpdatesMessageProcessor;
    }

    @Override
    public RobotExecutionResult execute(Map<String, String> map) {
        try {
            consumer.start(bookingUpdatesMessageProcessor, NUMBER_OF_THREADS);
            waitUntilInterrupted();
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
        return RobotExecutionResult.OK;
    }

    private void waitUntilInterrupted() throws InterruptedException {
        while (!isInterrupted()) {
            TimeUnit.SECONDS.sleep(5L);
        }
        consumer.shutdown();
        bookingUpdatesMessageProcessor.stop();
    }
}
