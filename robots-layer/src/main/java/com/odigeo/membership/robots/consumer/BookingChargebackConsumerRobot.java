package com.odigeo.membership.robots.consumer;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.commons.messaging.Consumer;
import com.odigeo.commons.messaging.JsonDeserializer;
import com.odigeo.commons.messaging.KafkaConsumer;
import com.odigeo.robots.AbstractRobot;
import com.odigeo.robots.RobotExecutionResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class BookingChargebackConsumerRobot extends AbstractRobot {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingChargebackConsumerRobot.class);
    private static final String MESSAGE_INTERRUPTED_CONSUMER = "An error occurred while trying to cancel the consumer job: {}";
    private static final String CHARGEBACK_MOVEMENT_STORED_TOPIC = "CHARGEBACK_MOVEMENT_STORED";
    private static final int NUMBER_OF_THREADS = 1;

    private final Consumer<BasicMessage> consumer;
    private final BookingChargebackMessageProcessor processor;

    @Inject
    public BookingChargebackConsumerRobot(@Assisted String robotId, BookingChargebackMessageProcessor processor) {
        super(robotId);
        this.consumer = new KafkaConsumer<>(new JsonDeserializer<>(BasicMessage.class), CHARGEBACK_MOVEMENT_STORED_TOPIC);
        this.processor = processor;
    }

    @Override
    public RobotExecutionResult execute(Map<String, String> map) {
        consumer.start(processor, NUMBER_OF_THREADS);
        return RobotExecutionResult.OK;
    }

    @Override
    public void interrupt() {
        try {
            consumer.shutdown();
        } catch (InterruptedException e) {
            LOGGER.warn(MESSAGE_INTERRUPTED_CONSUMER, e.getMessage(), e);
            Thread.currentThread().interrupt();
        } finally {
            super.interrupt();
        }
    }
}
