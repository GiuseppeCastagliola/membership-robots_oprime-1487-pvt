package com.odigeo.membership.robots.criteria;

import com.google.inject.Singleton;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.function.Predicate;

import static com.odigeo.membership.robots.expiration.MembershipExpirationServiceBean.MembershipStatus.ACTIVATED;
import static com.odigeo.membership.robots.expiration.MembershipExpirationServiceBean.MembershipStatus.PENDING_TO_ACTIVATE;

@Singleton
public class MembershipPredicateProvider {
    private static final Predicate<BigDecimal> IS_POSITIVE = x -> x.compareTo(BigDecimal.ZERO) > 0;
    private static final Predicate<BigDecimal> IS_NEGATIVE = x -> x.compareTo(BigDecimal.ZERO) < 0;
    private static final Predicate<BigDecimal> IS_NOT_ZERO = x -> x.compareTo(BigDecimal.ZERO) != 0;

    private Predicate<MembershipDTO> loggedPredicate(String conditionText, Predicate<MembershipDTO> membershipDTOPredicate) {
        return LoggingPredicateProvider.loggingPredicate(membershipDTO -> "Membership " + membershipDTO.getId() + StringUtils.SPACE + conditionText, membershipDTOPredicate);
    }

    public Predicate<MembershipDTO> isPendingToActivate() {
        return loggedPredicate("isPendingToActivate", membershipDto -> PENDING_TO_ACTIVATE.name().equals(membershipDto.getStatus()));
    }

    public Predicate<MembershipDTO> isActivated() {
        return loggedPredicate("isActivated", membershipDto -> ACTIVATED.name().equals(membershipDto.getStatus()));
    }

    protected Predicate<MembershipDTO> isAutoRenewalDisabled() {
        return membershipDto -> StringUtils.equals(membershipDto.getAutoRenewal(), "DISABLED");
    }

    protected Predicate<MembershipDTO> isBasicFree() {
        return membershipDTO -> StringUtils.equals(membershipDTO.getMembershipType(), "BASIC_FREE");
    }

    public Predicate<MembershipDTO> hasRecurringId() {
        return loggedPredicate("hasRecurringId", membershipDTO -> StringUtils.isNotEmpty(membershipDTO.getRecurringId()));
    }

    public Predicate<MembershipDTO> isBasicFreeToProcess() {
        return isActivated().and(isBasicFree()).and(isAutoRenewalDisabled());
    }

    Predicate<MembershipDTO> areFeesUnbalanced(BookingDTO bookingDTO) {
        return loggedPredicate("areFeesUnbalanced bookingId " + bookingDTO.getId(),
                exceededAvoidFees(bookingDTO).or(costFeeExistsAndPositiveBalance(bookingDTO)));
    }

    private Predicate<MembershipDTO> costFeeExistsAndPositiveBalance(BookingDTO bookingDTO) {
        return membershipDTO -> IS_POSITIVE.test(balancePlusAvoidFees(bookingDTO, membershipDTO))
                && IS_NOT_ZERO.test(bookingDTO.getTotalCostFeesAmount());
    }

    private Predicate<MembershipDTO> exceededAvoidFees(BookingDTO bookingDTO) {
        return membershipDTO -> IS_NEGATIVE.test(balancePlusAvoidFees(bookingDTO, membershipDTO));
    }

    private BigDecimal balancePlusAvoidFees(BookingDTO bookingDTO, MembershipDTO membershipDTO) {
        return membershipDTO.getBalance().add(bookingDTO.getTotalAvoidFeesAmount());
    }
}
