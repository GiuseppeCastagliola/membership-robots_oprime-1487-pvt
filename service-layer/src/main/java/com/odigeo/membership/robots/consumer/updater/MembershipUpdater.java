package com.odigeo.membership.robots.consumer.updater;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.exception.MembershipForbiddenException;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipDTOBuilder;
import com.odigeo.membership.robots.dto.SearchMemberAccountsDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.ProductCategoryBookingDTO;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.mapper.request.BookingRequestMapper;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import static com.odigeo.membership.robots.expiration.MembershipExpirationServiceBean.MembershipStatus.ACTIVATED;
import static com.odigeo.membership.robots.manager.bookingapi.product.ProductMembershipPrimeBookingItemType.MEMBERSHIP_SUBSCRIPTION;

@Singleton
public class MembershipUpdater {
    private static final Predicate<ApiCallWrapper<MembershipDTO, MembershipDTO>> ACTIVATION_RESPONSE_SUCCESSFUL = wrapper -> Optional.ofNullable(wrapper)
            .filter(ApiCall::isSuccessful)
            .map(ApiCallWrapper::getResponse)
            .map(MembershipDTO::getStatus)
            .filter(status -> ACTIVATED.name().equals(status))
            .isPresent();
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipUpdater.class);
    private final ExternalModuleManager externalModuleManager;
    private final MembershipRobotsMetrics metrics;
    private final BookingRequestMapper bookingRequestMapper;

    @Inject
    public MembershipUpdater(ExternalModuleManager externalModuleManager, MembershipRobotsMetrics metrics, BookingRequestMapper bookingRequestMapper) {
        this.externalModuleManager = externalModuleManager;
        this.metrics = metrics;
        this.bookingRequestMapper = bookingRequestMapper;
    }

    public MembershipDTO saveRecurringId(MembershipDTO membershipDTO, BookingDTO bookingDTO) {
        metrics.incrementCounter(MetricsName.RECURRING_ATTEMPTS);
        LOGGER.info("Trying save recurring (Booking {}, Membership {})", bookingDTO.getId(), membershipDTO.getId());
        ApiCallWrapper<MembershipDTO, MembershipDTO> saveRecurringWrapper = externalModuleManager.saveRecurringId(membershipDTO, bookingDTO);
        if (saveRecurringWrapper.isSuccessful()) {
            metrics.incrementCounter(MetricsName.INSERT_RECURRING_SUCCESS);
            return saveRecurringWrapper.getResponse();
        }
        handleRecurringSavingFails(saveRecurringWrapper, membershipDTO.getId());
        return membershipDTO;
    }

    public MembershipDTO activateMembership(MembershipDTO membershipDTO, BookingDTO bookingDTO) {
        metrics.incrementCounter(MetricsName.ACTIVATE_ATTEMPTS);
        LOGGER.info("Trying activate (Booking {}, Membership {})", bookingDTO.getId(), membershipDTO.getId());
        List<MembershipDTO> alreadySubscribed = searchActivatedMembershipsByUserIdAndWebsite(membershipDTO);
        if (CollectionUtils.isNotEmpty(alreadySubscribed)) {
            metrics.incrementCounter(MetricsName.ALREADY_SUBSCRIBED);
            LOGGER.warn("User {} have already an active subscription: {}, membership {} of booking {} will be never activated",
                    membershipDTO.getUserId(), alreadySubscribed.get(0).getId(), membershipDTO.getId(), bookingDTO.getId());
            return membershipDTO;
        }
        ApiCallWrapper<MembershipDTO, MembershipDTO> activateWrapper = externalModuleManager
                .activateMembership(MembershipDTOBuilder.builderFromDto(membershipDTO).balance(getProductPrice(bookingDTO)).build(), bookingDTO);
        return handleActivationResponse(membershipDTO, activateWrapper);
    }

    public BookingTrackingDTO trackBookingAndUpdateBalance(BookingDTO bookingDTO) {
        metrics.incrementCounter(MetricsName.TRACKING_ATTEMPTS);
        LOGGER.info("Trying tracking booking (Booking {}, Membership {})", bookingDTO.getId(), bookingDTO.getMembershipId());
        ApiCallWrapper<BookingTrackingDTO, BookingTrackingDTO> trackingDTOApiCallWrapper = externalModuleManager
                .trackBookingAndUpdateBalance(bookingRequestMapper.trackingDtoFromBookingDTO(bookingDTO));
        if (trackingDTOApiCallWrapper.isFailed()) {
            metrics.incrementCounter(MetricsName.INSERT_TRACKING_FAILED);
            throw buildRetryableException(trackingDTOApiCallWrapper.getThrowable(),
                    "Unknown fail saving booking tracking, bookingId " + bookingDTO.getId() + " membershipId " + bookingDTO.getMembershipId());
        }
        metrics.incrementCounter(MetricsName.INSERT_TRACKING_SUCCESS);
        return trackingDTOApiCallWrapper.getResponse();
    }

    public void deleteBookingTracking(BookingDTO bookingDTO) {
        metrics.incrementCounter(MetricsName.DELETE_TRACKING_ATTEMPTS);
        LOGGER.info("Trying delete tracking booking (Booking {}, Membership {})", bookingDTO.getId(), bookingDTO.getMembershipId());
        ApiCallWrapper<Void, BookingDTO> deleteBookingTrackingWrapper = externalModuleManager.deleteBookingTrackingAndUpdateBalance(bookingDTO);
        if (deleteBookingTrackingWrapper.isFailed()) {
            metrics.incrementCounter(MetricsName.DELETE_TRACKING_FAILED);
            throw buildRetryableException(deleteBookingTrackingWrapper.getThrowable(),
                    "Unknown fail deleting booking tracking, bookingId " + bookingDTO.getId() + " membershipId " + bookingDTO.getMembershipId());
        }
        metrics.incrementCounter(MetricsName.DELETE_TRACKING_SUCCESS);
    }

    public MembershipDTO enableAutoRenewal(MembershipDTO membershipDTO) {
        ApiCallWrapper<MembershipDTO, MembershipDTO> apiCallWrapper = externalModuleManager.enableMembershipAutoRenewal(membershipDTO);
        if (apiCallWrapper.isFailed()) {
            metrics.incrementCounter(MetricsName.BASIC_FREE_ENABLE_AUTO_RENEWAL_FAILED);
            throw buildRetryableException(apiCallWrapper.getThrowable(),
                    "Unknown fail enabling autoRenewal for basicFree, membershipId " + membershipDTO.getId());
        }
        return apiCallWrapper.getResponse();
    }

    private MembershipDTO handleActivationResponse(MembershipDTO membershipDTO, ApiCallWrapper<MembershipDTO, MembershipDTO> activateWrapper) {
        if (ACTIVATION_RESPONSE_SUCCESSFUL.test(activateWrapper)) {
            metrics.incrementCounter(MetricsName.ACTIVATION_SUCCESS);
            return activateWrapper.getResponse();
        }
        metrics.incrementCounter(MetricsName.ACTIVATION_FAILED);
        throw buildRetryableException(activateWrapper.getThrowable(), "Unknown fail activating membership " + membershipDTO.getId());
    }

    private void handleRecurringSavingFails(ApiCallWrapper<MembershipDTO, MembershipDTO> saveRecurringWrapper, long membershipId) {
        Throwable saveRecurringException = saveRecurringWrapper.getThrowable();
        if (saveRecurringException instanceof MembershipForbiddenException) {
            metrics.incrementCounter(MetricsName.RECURRING_ALREADY_EXISTS);
            LOGGER.info("Recurring id was already saved for membership id {}", membershipId, saveRecurringException);
        } else {
            metrics.incrementCounter(MetricsName.INSERT_RECURRING_FAILED);
            throw buildRetryableException(saveRecurringException, "Unknown fail saving recurring id of membership " + membershipId);
        }
    }

    private List<MembershipDTO> searchActivatedMembershipsByUserIdAndWebsite(MembershipDTO membershipDTO) {
        ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> listActivatedMembershipsDTOApiCallWrapper = externalModuleManager
                .searchMemberships(SearchMembershipsDTO.builder()
                        .website(membershipDTO.getWebsite())
                        .status(ACTIVATED.name())
                        .withMemberAccount(true)
                        .searchMemberAccountsDTO(SearchMemberAccountsDTO.builder().userId(membershipDTO.getUserId()).build())
                        .build());
        if (listActivatedMembershipsDTOApiCallWrapper.isSuccessful()) {
            return listActivatedMembershipsDTOApiCallWrapper.getResponse();
        }
        throw buildRetryableException(listActivatedMembershipsDTOApiCallWrapper.getThrowable(),
                "Unknown error searching activated memberships for user id : " + membershipDTO.getUserId());
    }

    private BigDecimal getProductPrice(BookingDTO bookingDTO) {
        return bookingDTO.getBookingProducts().stream()
                .filter(product -> product.getProductType().equals(MEMBERSHIP_SUBSCRIPTION.name()))
                .findFirst()
                .map(ProductCategoryBookingDTO::getPrice)
                .orElse(BigDecimal.ZERO);
    }

    private RetryableException buildRetryableException(Throwable throwable, String unknownCauseMessage) {
        return new RetryableException(Optional.ofNullable(throwable)
                .orElseThrow(() -> new RetryableException(unknownCauseMessage)));
    }
}
