package com.odigeo.membership.robots.consumer;

import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.exceptions.consumer.BookingNotFoundException;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import org.apache.commons.lang3.StringUtils;

import java.util.function.Supplier;

public interface Processor {

    default <T, U> T supplyDtoOrThrowException(Supplier<ApiCallWrapper<T, U>> dtoSupplier) {
        ApiCallWrapper<T, U> apiCallWrapper = dtoSupplier.get();
        if (apiCallWrapper.isSuccessful()) {
            return apiCallWrapper.getResponse();
        }
        if (isBookingApiNotFoundResponse(apiCallWrapper)) {
            throw new BookingNotFoundException(apiCallWrapper.getMessage());
        }
        throw new RetryableException(apiCallWrapper.getThrowable());
    }

    default <T, U> boolean isBookingApiNotFoundResponse(ApiCallWrapper<T, U> apiCallWrapper) {
        return StringUtils.contains(apiCallWrapper.getMessage(), "com.edreams.base.MissingElementException");
    }

    void process(long id);
}
