package com.odigeo.membership.robots.consumer;

import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

public interface MembershipBookingTrackingProcessorFactory {
    MembershipBookingTrackingProcessor create(BookingDTO bookingDTO, MembershipDTO membershipDTO);
}
