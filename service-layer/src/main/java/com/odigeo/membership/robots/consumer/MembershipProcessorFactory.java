package com.odigeo.membership.robots.consumer;

import com.odigeo.membership.robots.dto.booking.BookingDTO;

public interface MembershipProcessorFactory {
    MembershipProcessor create(BookingDTO bookingDTO);
}
