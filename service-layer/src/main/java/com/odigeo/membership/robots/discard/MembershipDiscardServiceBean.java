package com.odigeo.membership.robots.discard;

import com.google.inject.Inject;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.report.Report;
import com.odigeo.membership.robots.report.Reporter;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class MembershipDiscardServiceBean implements MembershipDiscardService {

    private final ExternalModuleManager externalModuleManager;
    private final Reporter reporter;

    @Inject
    public MembershipDiscardServiceBean(ExternalModuleManager externalModuleManager, Reporter reporter) {
        this.externalModuleManager = externalModuleManager;
        this.reporter = reporter;
    }

    @Override
    public void discardMemberships(List<Long> membershipIds) {
        Report report = new Report();
        searchMemberships(() -> externalModuleManager.retrieveMembershipsByIds(membershipIds))
                .map(externalModuleManager::discardMembership)
                .peek(wrapper -> externalModuleManager.createPendingToCollectFromPreviousMembership(
                        externalModuleManager.getMembershipSubscriptionOffer(wrapper.getResponse())))
                .forEach(wrapper -> reporter.feedReport(wrapper, report));
        reporter.generateReportLog(report);
    }

    private Stream<MembershipDTO> searchMemberships(Supplier<ApiCallWrapper<List<MembershipDTO>, ?>> supplyMemberships) {
        return Optional.ofNullable(supplyMemberships.get().getResponse()).orElseGet(Collections::emptyList).stream();
    }

}
