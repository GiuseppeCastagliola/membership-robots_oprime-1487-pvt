package com.odigeo.membership.robots.expiration;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.ExpirationConfiguration;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.fees.RemnantFeeService;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.report.Report;
import com.odigeo.membership.robots.report.Reporter;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.odigeo.membership.robots.fees.ProcessRemnantFeeFilterPredicate.PROCESS_REMNANT_FEE_CHECK;

@Singleton
public class MembershipExpirationServiceBean implements MembershipExpirationService {

    private static final BigDecimal ONE_CENT = new BigDecimal("0.01");
    private static final String ENABLED = "ENABLED";

    private final ExternalModuleManager externalModuleManager;
    private final ExpirationConfiguration expirationConfiguration;
    private final Reporter reporter;
    private final RemnantFeeService remnantFeeService;


    public enum MembershipStatus {
        PENDING_TO_ACTIVATE, PENDING_TO_COLLECT, ACTIVATED, DEACTIVATED, EXPIRED
    }

    @Inject
    public MembershipExpirationServiceBean(ExpirationConfiguration expirationConfiguration, ExternalModuleManager externalModuleManager,
                                           RemnantFeeService remnantFeeService, Reporter reporter) {
        this.expirationConfiguration = expirationConfiguration;
        this.externalModuleManager = externalModuleManager;
        this.remnantFeeService = remnantFeeService;
        this.reporter = reporter;
    }

    @Override
    public void expireMemberships() {
        Report report = new Report();
        getAllMembershipsToExpireStream(report)
                .map(externalModuleManager::expireMembership)
                .peek(wrapper -> reporter.feedReport(wrapper, report))
                .filter(PROCESS_REMNANT_FEE_CHECK)
                .map(callWrapper -> externalModuleManager.getPrimeSubscriptionBookingForMembership(callWrapper.getResponse()))
                .map(remnantFeeService::processPrimeSubscriptionBookingFees)
                .map(remnantFeeService::consumeMembershipRemnantBalance)
                .forEach(wrapper -> reporter.feedReport(wrapper, report));
        reporter.generateReportLog(report);
    }

    private Stream<MembershipDTO> getAllMembershipsToExpireStream(Report report) {
        return Stream.concat(
                getMembershipsToExpireAfterPendingToCollectCreation(searchMemberships(this::retrieveRenewableMemberships), report),
                searchMemberships(this::retrieveNotRenewableMemberships)
        );
    }

    private Stream<MembershipDTO> getMembershipsToExpireAfterPendingToCollectCreation(Stream<MembershipDTO> renewableMemberships, Report report) {
        return renewableMemberships.map(externalModuleManager::getMembershipSubscriptionOffer)
                .map(externalModuleManager::createPendingToCollectFromPreviousMembership)
                .peek(apiCall -> reporter.feedReport(apiCall, report))
                .filter(ApiCall::isSuccessful)
                .map(ApiCallWrapper::getRequest);
    }

    private Stream<MembershipDTO> searchMemberships(Supplier<ApiCallWrapper<List<MembershipDTO>, ?>> supplyMemberships) {
        return Optional.ofNullable(supplyMemberships.get().getResponse()).orElseGet(Collections::emptyList).stream();
    }

    private ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> retrieveRenewableMemberships() {
        return externalModuleManager.searchMemberships(searchBuilderByPeriod().status(MembershipStatus.ACTIVATED.name()).autoRenewal(ENABLED).build());
    }

    private ApiCallWrapper<List<MembershipDTO>, List<SearchMembershipsDTO>> retrieveNotRenewableMemberships() {
        final List<SearchMembershipsDTO> searchMembershipsDTOs = Arrays.asList(
                searchDtoDisabledRenewal(),
                searchDtoDeactivated(),
                searchDtoPendingToCollect(),
                searchDtoPendingToActivate(),
                searchDtoExpiredPositiveBalance());

        final Set<MembershipDTO> totalResult = searchMembershipsDTOs.stream()
                .map(externalModuleManager::searchMemberships)
                .map(callWrapper -> Optional.ofNullable(callWrapper.getResponse()).orElse(Collections.emptyList()))
                .flatMap(List::stream)
                .collect(Collectors.toSet());

        return new ApiCallWrapperBuilder<List<MembershipDTO>, List<SearchMembershipsDTO>>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS)
                .request(searchMembershipsDTOs).response(new ArrayList<>(totalResult)).build();
    }

    private SearchMembershipsDTO.SearchMembershipsDTOBuilder searchBuilderByPeriod() {
        LocalDate startingFrom = StringUtils.isEmpty(expirationConfiguration.getStartingFrom()) ? LocalDate.now() : LocalDate.parse(expirationConfiguration.getStartingFrom());
        return SearchMembershipsDTO.builder()
                .withMemberAccount(false).toExpirationDate(startingFrom)
                .fromExpirationDate(startingFrom.minusDays(expirationConfiguration.getDaysInThePast()));
    }

    private SearchMembershipsDTO searchDtoPendingToCollect() {
        return searchBuilderByPeriod().status(MembershipStatus.PENDING_TO_COLLECT.name()).build();
    }

    private SearchMembershipsDTO searchDtoDeactivated() {
        return searchBuilderByPeriod().status(MembershipStatus.DEACTIVATED.name()).build();
    }

    private SearchMembershipsDTO searchDtoDisabledRenewal() {
        return searchBuilderByPeriod().status(MembershipStatus.ACTIVATED.name()).autoRenewal("DISABLED").build();
    }

    private SearchMembershipsDTO searchDtoPendingToActivate() {
        return searchBuilderByPeriod().status(MembershipStatus.PENDING_TO_ACTIVATE.name()).build();
    }

    private SearchMembershipsDTO searchDtoExpiredPositiveBalance() {
        return searchBuilderByPeriod().minBalance(ONE_CENT)
                .fromActivationDate(LocalDate.of(2019, Month.OCTOBER, 1))
                .status(MembershipStatus.EXPIRED.name()).build();
    }
}
