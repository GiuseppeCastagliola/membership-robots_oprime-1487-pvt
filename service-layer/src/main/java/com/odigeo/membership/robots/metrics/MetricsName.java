package com.odigeo.membership.robots.metrics;

public enum MetricsName {

    ACTIVATE_ATTEMPTS("membership-robots.activation-attempts"),
    TRACKING_ATTEMPTS("membership-robots.tracking-attempts"),
    RECURRING_ATTEMPTS("membership-robots.recurring-attempts"),
    TRACKING_REBALANCE_ATTEMPTS("membership-robots.delete-tracking-attempts"),
    BASIC_FREE_UPDATES("membership-robots.basic-free-updates-attempts"),
    BASIC_FREE_UPDATES_ALREADY_WITH_SUBSCRIPTION("membership-robots.basic-free-updates-already-have-subscription"),
    BASIC_FREE_UPDATES_FAILED("membership-robots.basic-free-updates-failed"),
    BASIC_FREE_UPDATES_SUCCESS("membership-robots.basic-free-updates-success"),
    BASIC_FREE_ENABLE_AUTO_RENEWAL_FAILED("membership-robots.basic-free-auto-renewal-failed"),
    BOOKING_PROCESSING_COMPLETED("membership-robots.booking-processing-completed"),
    PRIME_BOOKING("membership-robots.booking-prime-processing"),
    BOOKING_PROCESSING_FAILED("membership-robots.booking-processing-failed-attempt"),
    BOOKING_NOT_FOUND_FAIL("membership-robots.booking-not-found-failed-attempt"),
    TOTAL_PROCESSED_BOOKING("membership-robots.booking-processing"),
    TOTAL_BOOKING_FAILED("membership-robots.booking-processing-failed"),
    INSERT_RECURRING_FAILED("membership-robots.insert-recurring-failed"),
    INSERT_RECURRING_SUCCESS("membership-robots.insert-recurring-success"),
    RECURRING_ALREADY_EXISTS("membership-robots.insert-recurring-already-exists"),
    ACTIVATION_SUCCESS("membership-robots.membership-activation-success"),
    ALREADY_SUBSCRIBED("membership-robots.membership-activation-duplicate"),
    ACTIVATION_FAILED("membership-robots.membership-activation-failed"),
    TOTAL_PROCESSED_CHARGEBACK("membership-robots.chargeback_processing"),
    TOTAL_PROCESSED_PRIME_STANDALONE_CHARGEBACK("membership-robots.chargeback_standalone_prime_processing"),
    TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK("membership-robots.membership_deactivated_chargeback"),
    TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE("membership-robots.membership_consume_remnant_balance"),
    INSERT_TRACKING_SUCCESS("membership-robots.insert-tracking-success"),
    INSERT_TRACKING_FAILED("membership-robots.insert-tracking-success"),
    DELETE_TRACKING_ATTEMPTS("membership-robots.delete-tracking-attempt"),
    DELETE_TRACKING_SUCCESS("membership-robots.delete-tracking-success"),
    DELETE_TRACKING_FAILED("membership-robots.delete-tracking-failed"),
    UPDATE_BOOKING_ATTEMPTS("membership-robots.update-booking-attempt"),
    UPDATE_BOOKING_FAILED("membership-robots.update-booking-failed"),
    UPDATE_BOOKING_SUCCESS("membership-robots.update-booking-success");


    public enum TagName {
        ATTEMPT_NUMBER, CAUSE, STATUS
    }

    private String id;

    MetricsName(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
