package com.odigeo.membership.robots.mapper.response;

import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import org.mapstruct.Mapper;

@Mapper
public interface MembershipOfferResponseMapper {
    MembershipOfferDTO subscriptionOfferToMembershipOfferDto(MembershipSubscriptionOffer membershipSubscriptionOffer);
}
