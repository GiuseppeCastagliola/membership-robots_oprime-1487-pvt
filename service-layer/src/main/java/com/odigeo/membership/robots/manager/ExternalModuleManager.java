package com.odigeo.membership.robots.manager;

import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

import java.util.List;

public interface ExternalModuleManager {
    ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingToCollectFromPreviousMembership(ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper);

    ApiCallWrapper<BookingDTO, MembershipDTO> getPrimeSubscriptionBookingForMembership(MembershipDTO membership);

    ApiCallWrapper<MembershipOfferDTO, MembershipDTO> getMembershipSubscriptionOffer(MembershipDTO previousMembership);

    ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchMemberships(SearchMembershipsDTO searchMembershipsDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembership(MembershipDTO membershipDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> discardMembership(MembershipDTO membershipDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> enableMembershipAutoRenewal(MembershipDTO membershipDTO);

    ApiCallWrapper<List<MembershipDTO>, List<Long>> retrieveMembershipsByIds(List<Long> membershipIds);

    ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookings(ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> bookingSummariesWrapperBuilder);

    ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBooking(long id, ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> updateBookingWrapperBuilder);

    ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantMembershipBalance(MembershipDTO expiredMembership);

    ApiCallWrapper<BookingTrackingDTO, BookingDTO> getBookingTracking(BookingDTO bookingDTO);

    ApiCallWrapper<BookingTrackingDTO, BookingTrackingDTO> trackBookingAndUpdateBalance(BookingTrackingDTO bookingTrackingDTO);

    ApiCallWrapper<Void, BookingDTO> deleteBookingTrackingAndUpdateBalance(BookingDTO bookingDTO);

    ApiCallWrapper<MembershipDTO, Long> getMembership(Long membershipId);

    ApiCallWrapper<MembershipDTO, MembershipDTO> saveRecurringId(MembershipDTO membershipDTO, BookingDTO bookingDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> activateMembership(MembershipDTO membershipDTO, BookingDTO bookingDTO);
}
