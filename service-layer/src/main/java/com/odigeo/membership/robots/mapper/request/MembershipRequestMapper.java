package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.request.tracking.MembershipBookingTrackingRequest;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.format.DateTimeFormatter;

@Mapper
public interface MembershipRequestMapper {

    final class Fields {
        private static final String MEMBERSHIP_ID = "membershipId";
        private static final String OPERATION = "operation";
        private static final String MEMBERSHIP_RENEWAL = "membershipRenewal";
        private static final String MEMBERSHIP_STATUS = "membershipStatus";
        private static final String USER_CREDIT_CARD_ID = "userCreditCardId";
        private static final String RECURRING_ID = "recurringId";

        private Fields() {
        }
    }

    @Mapping(target = Fields.MEMBERSHIP_ID, source = "id")
    @Mapping(target = Fields.OPERATION, constant = "EXPIRE_MEMBERSHIP")
    @Mapping(target = Fields.MEMBERSHIP_RENEWAL, ignore = true)
    @Mapping(target = Fields.MEMBERSHIP_STATUS, ignore = true)
    @Mapping(target = Fields.USER_CREDIT_CARD_ID, ignore = true)
    @Mapping(target = Fields.RECURRING_ID, ignore = true)
    UpdateMembershipRequest dtoToExpireRequest(MembershipDTO membershipDTO);

    @Mapping(target = Fields.MEMBERSHIP_ID, source = "id")
    @Mapping(target = Fields.OPERATION, constant = "DISCARD_MEMBERSHIP")
    @Mapping(target = Fields.MEMBERSHIP_RENEWAL, ignore = true)
    @Mapping(target = Fields.MEMBERSHIP_STATUS, ignore = true)
    @Mapping(target = Fields.USER_CREDIT_CARD_ID, ignore = true)
    @Mapping(target = Fields.RECURRING_ID, ignore = true)
    UpdateMembershipRequest dtoToDiscardRequest(MembershipDTO membershipDTO);

    @Mapping(target = Fields.MEMBERSHIP_ID, source = "id")
    @Mapping(target = Fields.OPERATION, constant = "DEACTIVATE_MEMBERSHIP")
    @Mapping(target = Fields.MEMBERSHIP_RENEWAL, ignore = true)
    @Mapping(target = Fields.MEMBERSHIP_STATUS, ignore = true)
    @Mapping(target = Fields.USER_CREDIT_CARD_ID, ignore = true)
    @Mapping(target = Fields.RECURRING_ID, ignore = true)
    UpdateMembershipRequest dtoToDeactivateRequest(MembershipDTO membershipDTO);

    @Mapping(target = Fields.MEMBERSHIP_ID, source = "id")
    @Mapping(target = Fields.OPERATION, constant = "ENABLE_AUTO_RENEWAL")
    @Mapping(target = Fields.MEMBERSHIP_RENEWAL, ignore = true)
    @Mapping(target = Fields.MEMBERSHIP_STATUS, ignore = true)
    @Mapping(target = Fields.USER_CREDIT_CARD_ID, ignore = true)
    @Mapping(target = Fields.RECURRING_ID, ignore = true)
    UpdateMembershipRequest dtoToEnableAutoRenewalRequest(MembershipDTO membershipDTO);

    @Mapping(target = Fields.MEMBERSHIP_ID, source = "id")
    @Mapping(target = Fields.OPERATION, constant = "INSERT_RECURRING_ID")
    @Mapping(target = Fields.RECURRING_ID, source = Fields.RECURRING_ID)
    @Mapping(target = Fields.MEMBERSHIP_RENEWAL, ignore = true)
    @Mapping(target = Fields.MEMBERSHIP_STATUS, ignore = true)
    @Mapping(target = Fields.USER_CREDIT_CARD_ID, ignore = true)
    UpdateMembershipRequest dtoToRecurringRequest(MembershipDTO membershipDTO);

    @Mapping(target = "withWebsite", source = "membershipDTO.website")
    @Mapping(target = "withSubscriptionPrice", source = "membershipOfferDTO.price")
    @Mapping(target = "withRecurringId", source = "membershipDTO.recurringId")
    @Mapping(target = "withSourceType", source = "membershipDTO.sourceType")
    @Mapping(target = "withMembershipType", source = "membershipDTO.membershipType", qualifiedByName = "toTargetMembershipType")
    @Mapping(target = "withExpirationDate", source = "newExpirationDate")
    @Mapping(target = "withMonthsToRenewal", source = "membershipOfferDTO.duration")
    @Mapping(target = "withMemberAccountId", source = "membershipDTO.memberAccountId")
    @Mapping(target = "withCurrencyCode", source = "membershipOfferDTO.currencyCode")
    CreatePendingToCollectRequest.Builder dtoToCreatePendingToCollectRequestBuilder(MembershipDTO membershipDTO, MembershipOfferDTO membershipOfferDTO, String newExpirationDate);

    @Mapping(target = Fields.MEMBERSHIP_ID, source = "id")
    @Mapping(target = Fields.OPERATION, constant = "CONSUME_MEMBERSHIP_REMNANT_BALANCE")
    @Mapping(target = Fields.MEMBERSHIP_RENEWAL, ignore = true)
    @Mapping(target = Fields.MEMBERSHIP_STATUS, ignore = true)
    @Mapping(target = Fields.USER_CREDIT_CARD_ID, ignore = true)
    @Mapping(target = Fields.RECURRING_ID, ignore = true)
    UpdateMembershipRequest dtoToResetBalanceRequest(MembershipDTO membershipDTO);


    default MembershipBookingTrackingRequest dtoToBookingTrackingRequest(BookingTrackingDTO bookingTrackingDTO) {
        return MembershipBookingTrackingRequest.builder(bookingTrackingDTO.getBookingId(), bookingTrackingDTO.getMembershipId())
                .bookingDate(DateTimeFormatter.ISO_DATE_TIME.format(bookingTrackingDTO.getBookingDate()))
                .perksAmount(bookingTrackingDTO.getPerksAmount())
                .costFeeAmount(bookingTrackingDTO.getCostFeeAmount())
                .avoidFeeAmount(bookingTrackingDTO.getAvoidFeeAmount())
                .build();
    }

    @Named("toTargetMembershipType")
    static String toTargetMembershipType(String sourceMembershipType) {
        if ("BASIC_FREE".equalsIgnoreCase(sourceMembershipType)) {
            return "BASIC";
        }
        return sourceMembershipType;
    }

}
                                                                
