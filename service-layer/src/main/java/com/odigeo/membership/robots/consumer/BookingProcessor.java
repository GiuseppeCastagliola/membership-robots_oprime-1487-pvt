package com.odigeo.membership.robots.consumer;

import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.Striped;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.criteria.BookingPredicateProvider;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManager;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.function.Supplier;

@Singleton
public class BookingProcessor implements Processor {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingProcessor.class);
    private static final Supplier<String> THREAD_NAME = () -> Thread.currentThread().getName();
    public static final int STRIPES = 1024;
    private final MembershipProcessorFactory membershipProcessorFactory;
    private final BookingApiManager bookingApiManager;
    private final BookingPredicateProvider bookingPredicateProvider;
    private final MembershipRobotsMetrics membershipRobotsMetrics;
    private final List<ConditionalConsumer<MembershipProcessor, BookingDTO>> conditionalConsumers;
    private final Striped<Lock> stripedLocksByBookingId;
    private final Striped<Lock> stripedLocksByMembershipId;

    @Inject
    protected BookingProcessor(BookingApiManager bookingApiManager, BookingPredicateProvider bookingPredicateProvider, MembershipProcessorFactory membershipProcessorFactory, MembershipRobotsMetrics membershipRobotsMetrics) {
        this.bookingApiManager = bookingApiManager;
        this.bookingPredicateProvider = bookingPredicateProvider;
        this.membershipProcessorFactory = membershipProcessorFactory;
        this.membershipRobotsMetrics = membershipRobotsMetrics;
        this.conditionalConsumers = ImmutableList.of(
                ConditionalConsumer.ofBooking(MembershipProcessor::addActivationCandidate, bookingPredicateProvider.eligibleForActivation()),
                ConditionalConsumer.ofBooking(MembershipProcessor::addBasicFreeUpdateCandidate, bookingPredicateProvider.eligibleForBasicFreeUpdate()),
                ConditionalConsumer.ofBooking(MembershipProcessor::addTrackingCandidate, bookingPredicateProvider.eligibleForTracking()),
                ConditionalConsumer.ofBooking(MembershipProcessor::addRebalanceNotContractBookingTrackingCandidate, bookingPredicateProvider.eligibleForTrackingUpdate()),
                ConditionalConsumer.ofBooking(MembershipProcessor::addRecurringSavingCandidate, bookingPredicateProvider.eligibleForRecurring()));
        stripedLocksByBookingId = Striped.lazyWeakLock(STRIPES);
        stripedLocksByMembershipId = Striped.lazyWeakLock(STRIPES);
    }

    @Override
    public void process(long bookingId) {
        Lock lockByBookingId = stripedLocksByBookingId.get(bookingId);
        try {
            LOGGER.info("Trying lock on bookingId {} by thread {} ", bookingId, THREAD_NAME.get());
            lockByBookingId.lock();
            LOGGER.info("Lock on bookingId {} by thread {} ", bookingId, THREAD_NAME.get());
            Optional.ofNullable(getBookingDTOOrTriggerRetry(bookingId))
                    .filter(bookingPredicateProvider.isNotTestAndIsPrime())
                    .ifPresent(this::validateBookingAndRunMembershipProcessor);
        } finally {
            lockByBookingId.unlock();
            LOGGER.info("Lock unlocked on bookingId {} by {} ", bookingId, THREAD_NAME.get());
        }
    }

    private void validateBookingAndRunMembershipProcessor(BookingDTO bookingDto) {
        membershipRobotsMetrics.incrementCounter(MetricsName.PRIME_BOOKING);
        MembershipProcessor membershipProcessor = membershipProcessorFactory.create(bookingDto);
        conditionalConsumers.forEach(consumer -> consumer.accept(membershipProcessor, bookingDto));
        processMembershipLockedById(bookingDto.getMembershipId(), membershipProcessor);
    }

    private void processMembershipLockedById(Long membershipId, MembershipProcessor membershipProcessor) {
        Lock lockByMembershipId = stripedLocksByMembershipId.get(membershipId);
        try {
            LOGGER.info("Trying lock on membershipId {} by {} ", membershipId, THREAD_NAME.get());
            lockByMembershipId.lock();
            LOGGER.info("Lock on membershipId {} by {} ", membershipId, THREAD_NAME.get());
            membershipProcessor.process(membershipId);
        } finally {
            lockByMembershipId.unlock();
            LOGGER.info("Lock unlocked on membershipId {} by {} ", membershipId, THREAD_NAME.get());
        }
    }

    private BookingDTO getBookingDTOOrTriggerRetry(long bookingId) {
        final ApiCallWrapperBuilder<BookingDTO, BookingDTO> bookingDetailWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
                .request(BookingDTO.builder().id(bookingId).build());
        return supplyDtoOrThrowException(() -> bookingApiManager.getBooking(bookingDetailWrapperBuilder));
    }
}
