package com.odigeo.membership.robots.manager.bookingapi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.BadCredentialsException;
import com.odigeo.bookingapi.v12.BookingApiService;
import com.odigeo.bookingapi.v12.InvalidParametersException;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.BookingApiConfiguration;
import com.odigeo.membership.robots.configuration.BookingSearchApiConfiguration;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.MembershipSubscriptionBookingException;
import com.odigeo.membership.robots.exceptions.bookingapi.BookingApiException;
import com.odigeo.membership.robots.mapper.request.BookingRequestMapper;
import com.odigeo.membership.robots.mapper.response.BookingResponseMapper;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Singleton
public class BookingApiManagerBean implements BookingApiManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingApiManager.class);
    private static final String BOOKING_API_ERROR_MSG = "Error in BookingApi module when executing: %s for bookingId: %s";
    private static final String SEARCH_BOOKINGS_ERROR_MESSAGE = "Error in BookingSearchApi module when executing searchBookings() for membershipId %d";
    private static final String SEARCH_BOOKINGS_FOR_SUBSCRIPTION_BOOKING_ERROR_MESSAGE = "Error searching for Prime Subscription BookingSummary for membershipId %s";
    private static final String GET_BOOKING_SUCCESS_MESSAGE = "%s booking found for userId %s";
    private static final String GET_BOOKING_FAILURE_MESSAGE = "No booking detail found for bookingId %s";
    private static final String SEARCH_BOOKINGS_SUCCESS_MESSAGE = "%s %s bookingSummaries found for membershipId %s";
    private static final String PRIME_SUBSCRIPTION_BOOKINGS_SUCCESS_MESSAGE = "Prime subscription booking found for membershipId %s";
    private static final String UPDATE_BOOKING_SUCCESS_MESSAGE = "updateBooking successfully completed for bookingId: %s";
    private static final String INVALID_PRIME_BOOKINGS_QUANTITY_EXCEPTION = "Invalid number of prime bookings (%s) found for membershipId: %s";
    private static final String PRIME = "Prime";
    private static final String NON_PRIME = "non-Prime";
    private static final String COLON_DELIMITER = " : ";

    private final BookingApiService bookingApiService;
    private final BookingSearchApiService bookingSearchApiService;
    private final BookingApiConfiguration bookingApiConfiguration;
    private final BookingSearchApiConfiguration bookingSearchApiConfiguration;
    private final BookingRequestMapper bookingRequestMapper;
    private final BookingResponseMapper bookingResponseMapper;

    @Inject
    public BookingApiManagerBean(BookingApiService bookingApiService, BookingSearchApiService bookingSearchApiService, BookingApiConfiguration bookingApiConfiguration,
                                 BookingSearchApiConfiguration bookingSearchApiConfiguration, BookingRequestMapper bookingRequestMapper,
                                 BookingResponseMapper bookingResponseMapper) {
        this.bookingApiService = bookingApiService;
        this.bookingSearchApiService = bookingSearchApiService;
        this.bookingApiConfiguration = bookingApiConfiguration;
        this.bookingSearchApiConfiguration = bookingSearchApiConfiguration;
        this.bookingRequestMapper = bookingRequestMapper;
        this.bookingResponseMapper = bookingResponseMapper;
    }

    @Override
    public ApiCallWrapper<BookingDTO, BookingDTO> getBooking(ApiCallWrapperBuilder<BookingDTO, BookingDTO> bookingDetailWrapperBuilder) {
        long bookingId = bookingDetailWrapperBuilder.getRequest().getId();
        LOGGER.info("BookingApiManager::getBooking with bookingId {}", bookingId);

        try {
            BookingDetail bookingDetail = bookingApiService.getBooking(bookingApiConfiguration.getUser(), bookingApiConfiguration.getPassword(), Locale.getDefault(), bookingId);
            BookingDTO booking = bookingResponseMapper.bookingDetailResponseToDto(bookingDetail);

            if (successfulCall(bookingDetail)) {
                String successMessage = format(GET_BOOKING_SUCCESS_MESSAGE, booking.getId(), booking.getUserId());
                bookingDetailWrapperBuilder.result(ApiCall.Result.SUCCESS).response(booking).message(successMessage);
            } else {
                bookingDetailWrapperBuilder.result(ApiCall.Result.FAIL).response(booking);
                if (nonNull(bookingDetail)) {
                    bookingDetailWrapperBuilder.message(ObjectUtils.defaultIfNull(bookingDetail.getErrorMessage(), bookingDetail.getMessage()));
                } else {
                    bookingDetailWrapperBuilder.message(format(GET_BOOKING_FAILURE_MESSAGE, bookingId));
                }
            }

        } catch (InvalidParametersException | BadCredentialsException | UndeclaredThrowableException e) {
            String errorMessage = format(BOOKING_API_ERROR_MSG, "getBooking()", bookingId);
            LOGGER.error(errorMessage, e);
            bookingDetailWrapperBuilder.result(ApiCall.Result.FAIL).throwable(new BookingApiException(errorMessage, e)).message(exceptionMessage(errorMessage, e));
        }

        final ApiCallWrapper<BookingDTO, BookingDTO> result = bookingDetailWrapperBuilder.build();
        result.logResultAndMessageWithPrefix(LOGGER, String.join(COLON_DELIMITER, result.getEndpoint().name(), String.valueOf(bookingId)));
        return result;
    }

    @Override
    public ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookings(ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> bookingSummariesWrapperBuilder) {
        SearchBookingsRequest searchBookingsRequest = bookingSummariesWrapperBuilder.getRequest();
        LOGGER.info("BookingApiManager::searchBookings with SearchBookingsRequest {}", searchBookingsRequest);
        String bookingType = Boolean.parseBoolean(searchBookingsRequest.getIsBookingSubscriptionPrime()) ? PRIME : NON_PRIME;
        try {
            SearchBookingsPageResponse<List<BookingSummary>> searchBookingsResponse = bookingSearchApiService
                    .searchBookings(bookingSearchApiConfiguration.getUser(), bookingSearchApiConfiguration.getPassword(), Locale.getDefault(), searchBookingsRequest);

            //null response object should pass null to the DTO mapper, but a null getBookings() result can be an empty list.
            List<BookingDTO> bookingSummaries = isNull(searchBookingsResponse)
                    ? bookingResponseMapper.bookingSummariesResponseToDto(null)
                    : bookingResponseMapper.bookingSummariesResponseToDto(Optional.ofNullable(searchBookingsResponse.getBookings()).orElse(emptyList()));

            if (successfulCall(searchBookingsResponse)) {
                String successMessage = format(SEARCH_BOOKINGS_SUCCESS_MESSAGE, bookingSummaries.size(), bookingType, searchBookingsRequest.getMembershipId());
                bookingSummariesWrapperBuilder.result(ApiCall.Result.SUCCESS).response(bookingSummaries).message(successMessage);
            } else {
                bookingSummariesWrapperBuilder.result(ApiCall.Result.FAIL).response(bookingSummaries);
                Optional.ofNullable(searchBookingsResponse)
                        .flatMap(response -> Optional.ofNullable(response.getErrorMessage()))
                        .ifPresent(bookingSummariesWrapperBuilder::message);
            }
        } catch (InvalidParametersException | BadCredentialsException | UndeclaredThrowableException e) {
            String exceptionMessage = format(SEARCH_BOOKINGS_ERROR_MESSAGE, searchBookingsRequest.getMembershipId());
            LOGGER.error(exceptionMessage, e);
            bookingSummariesWrapperBuilder.result(ApiCall.Result.FAIL).throwable(new BookingApiException(exceptionMessage, e))
                    .message(exceptionMessage(exceptionMessage, e));
        }

        final ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> result = bookingSummariesWrapperBuilder.build();
        result.logResultAndMessageWithPrefix(LOGGER, String.join(COLON_DELIMITER, result.getEndpoint().name(), bookingType, String.valueOf(result.getRequest().getMembershipId())));
        return result;
    }

    @Override
    public ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBooking(long bookingId, ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> apiCallWrapperBuilder) {
        UpdateBookingRequest updateBookingRequest = apiCallWrapperBuilder.getRequest();
        LOGGER.info("BookingApiManager::updateBooking with bookingId {}, updateBookingRequest {}", bookingId, updateBookingRequest);
        try {
            BookingDetail bookingDetail = bookingApiService
                    .updateBooking(bookingApiConfiguration.getUser(), bookingApiConfiguration.getPassword(), Locale.getDefault(), bookingId, updateBookingRequest);
            BookingDTO booking = bookingResponseMapper.bookingDetailResponseToDto(bookingDetail);

            if (successfulCall(bookingDetail)) {
                apiCallWrapperBuilder.result(ApiCall.Result.SUCCESS).response(booking).message(format(UPDATE_BOOKING_SUCCESS_MESSAGE, bookingId));
            } else {
                apiCallWrapperBuilder.result(ApiCall.Result.FAIL).response(booking);
                Optional.ofNullable(bookingDetail).ifPresent(bookingDetailResponse -> {
                    Optional.ofNullable(bookingDetail.getMessage()).ifPresent(apiCallWrapperBuilder::message);
                    Optional.ofNullable(bookingDetail.getErrorMessage()).ifPresent(apiCallWrapperBuilder::message);
                });
            }
        } catch (InvalidParametersException | BadCredentialsException | UndeclaredThrowableException e) {
            String errorMessage = format(BOOKING_API_ERROR_MSG, "updateBooking()", bookingId);
            LOGGER.error(errorMessage, e);
            apiCallWrapperBuilder.result(ApiCall.Result.FAIL).throwable(new BookingApiException(errorMessage, e)).message(exceptionMessage(errorMessage, e));
        }

        final ApiCallWrapper<BookingDTO, UpdateBookingRequest> result = apiCallWrapperBuilder.build();
        result.logResultAndMessageWithPrefix(LOGGER, String.join(COLON_DELIMITER, result.getEndpoint().name(), String.valueOf(bookingId)));
        return result;
    }

    @Override
    public ApiCallWrapper<BookingDTO, MembershipDTO> getPrimeSubscriptionBookingForMembership(MembershipDTO membership) {
        ApiCallWrapperBuilder<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, MembershipDTO>(ApiCall.Endpoint.GET_BOOKING).request(membership);
        try {
            BookingDTO primeSubscriptionBookingSummary = getPrimeSubscriptionBookingSummaryForMembership(membership);
            BookingDTO primeSubscriptionBookingDetail = getBookingDetailFromBookingSummary(primeSubscriptionBookingSummary);
            BookingDTO primeSubscriptionBookingDto = bookingResponseMapper.bookingResponsesToDto(primeSubscriptionBookingSummary, primeSubscriptionBookingDetail);

            primeSubscriptionBookingWrapperBuilder.result(ApiCall.Result.SUCCESS).response(primeSubscriptionBookingDto)
                    .message(format(PRIME_SUBSCRIPTION_BOOKINGS_SUCCESS_MESSAGE, membership.getId()));
        } catch (MembershipSubscriptionBookingException e) {
            LOGGER.error(format(SEARCH_BOOKINGS_ERROR_MESSAGE, membership.getId()), e);
            primeSubscriptionBookingWrapperBuilder.result(ApiCall.Result.FAIL).throwable(e).message(e.getMessage());
        }

        final ApiCallWrapper<BookingDTO, MembershipDTO> result = primeSubscriptionBookingWrapperBuilder.build();
        result.logResultAndMessageWithPrefix(LOGGER, String.join(COLON_DELIMITER, result.getEndpoint().name(), String.valueOf(membership.getId()), isNull(result.getResponse()) ? "booking=null" : String.valueOf(result.getResponse().getId())));
        return result;
    }

    private BookingDTO getPrimeSubscriptionBookingSummaryForMembership(MembershipDTO membership) throws MembershipSubscriptionBookingException {
        final SearchBookingsRequest primeSubscriptionBookingSummaryRequest = bookingRequestMapper.dtoToPrimeSubscriptionBookingSummaryRequest(membership).build();
        final ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> bookingSummariesWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(
                ApiCall.Endpoint.SEARCH_BOOKINGS).request(primeSubscriptionBookingSummaryRequest);

        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> bookingSummariesWrapper = searchBookings(bookingSummariesWrapperBuilder);

        handlePrimeSubscriptionBookingsErrors(bookingSummariesWrapper);
        return bookingSummariesWrapper.getResponse().get(0);
    }

    private BookingDTO getBookingDetailFromBookingSummary(BookingDTO primeSubscriptionBookingSummary) throws MembershipSubscriptionBookingException {
        final ApiCallWrapperBuilder<BookingDTO, BookingDTO> primeSubscriptionBookingDetailWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
                .request(primeSubscriptionBookingSummary);
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDetailCallWrapper = getBooking(primeSubscriptionBookingDetailWrapperBuilder);

        BookingDTO response = bookingDetailCallWrapper.getResponse();
        if (!bookingDetailCallWrapper.isSuccessful() || isNull(response)) {
            throw new MembershipSubscriptionBookingException(format(GET_BOOKING_FAILURE_MESSAGE, primeSubscriptionBookingSummary.getId()));
        }
        return response;
    }

    private boolean successfulCall(com.odigeo.bookingsearchapi.v1.responses.BaseResponse response) {
        return nonNull(response) && isNull(response.getErrorMessage());
    }

    private boolean successfulCall(com.odigeo.bookingapi.v12.responses.BaseResponse response) {
        return nonNull(response) && isNull(response.getErrorMessage());
    }

    private String exceptionMessage(String customErrorMessage, Exception e) {
        return String.join(System.lineSeparator(), customErrorMessage, e.getMessage());
    }

    private void handlePrimeSubscriptionBookingsErrors(ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchPrimeBookingSummaries) throws MembershipSubscriptionBookingException {
        long membershipId = searchPrimeBookingSummaries.getRequest().getMembershipId();

        if (!searchPrimeBookingSummaries.isSuccessful()) {
            throw new MembershipSubscriptionBookingException(format(SEARCH_BOOKINGS_FOR_SUBSCRIPTION_BOOKING_ERROR_MESSAGE, membershipId), searchPrimeBookingSummaries.getThrowable());
        }
        if (searchPrimeBookingSummaries.getResponse().size() != 1) {
            String errorMessage = format(INVALID_PRIME_BOOKINGS_QUANTITY_EXCEPTION, searchPrimeBookingSummaries.getResponse().size(), membershipId);
            throw new MembershipSubscriptionBookingException(errorMessage);
        }
    }
}
