package com.odigeo.membership.robots.manager.membership;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.mapper.request.MembershipRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import io.vavr.control.Try;

@Singleton
public class MembershipModuleManagerTrackingBean implements MembershipModuleManagerTracking {
    public static final String BOOKING_TRACKING_PREFIX_MSG = "Booking tracking ";
    private final MembershipService membershipService;
    private final MembershipRequestMapper membershipRequestMapper;
    private final MembershipResponseMapper membershipResponseMapper;

    @Inject
    public MembershipModuleManagerTrackingBean(MembershipService membershipService, MembershipRequestMapper membershipRequestMapper, MembershipResponseMapper membershipResponseMapper) {
        this.membershipService = membershipService;
        this.membershipRequestMapper = membershipRequestMapper;
        this.membershipResponseMapper = membershipResponseMapper;
    }

    @Override
    public ApiCallWrapper<BookingTrackingDTO, BookingDTO> getBookingTracking(BookingDTO bookingDTO) {
        ApiCallWrapperBuilder<BookingTrackingDTO, BookingDTO> builder = new ApiCallWrapperBuilder<BookingTrackingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING_TRACKING).request(bookingDTO);
        Try.of(() -> membershipService.getBookingTracking(bookingDTO.getId()))
                .onFailure(throwable -> builder.throwable(throwable)
                        .result(ApiCall.Result.FAIL)
                        .message(throwable.getMessage()))
                .onSuccess(response -> builder.result(ApiCall.Result.SUCCESS)
                        .response(membershipResponseMapper.bookingTrackingResponseToDTO(response))
                        .message(BOOKING_TRACKING_PREFIX_MSG + bookingDTO.getId() + " retrieved!"));
        return builder.build();
    }

    @Override
    public ApiCallWrapper<BookingTrackingDTO, BookingTrackingDTO> trackBookingAndUpdateBalance(BookingTrackingDTO bookingTrackingDTO) {
        ApiCallWrapperBuilder<BookingTrackingDTO, BookingTrackingDTO> builder = new ApiCallWrapperBuilder<BookingTrackingDTO, BookingTrackingDTO>(ApiCall.Endpoint.TRACK_BOOKING).request(bookingTrackingDTO);
        Try.of(() -> membershipService.addBookingTrackingUpdateBalance(membershipRequestMapper.dtoToBookingTrackingRequest(bookingTrackingDTO)))
                .onFailure(throwable -> builder.throwable(throwable)
                        .result(ApiCall.Result.FAIL)
                        .message(throwable.getMessage()))
                .onSuccess(response -> builder.result(ApiCall.Result.SUCCESS)
                        .response(membershipResponseMapper.bookingTrackingResponseToDTO(response))
                        .message(BOOKING_TRACKING_PREFIX_MSG + bookingTrackingDTO.getBookingId() + " saved!"));
        return builder.build();
    }

    @Override
    public ApiCallWrapper<Void, BookingDTO> deleteBookingTrackingAndUpdateBalance(BookingDTO bookingDTO) {
        ApiCallWrapperBuilder<Void, BookingDTO> builder = new ApiCallWrapperBuilder<Void, BookingDTO>(ApiCall.Endpoint.DELETE_BOOKING_TRACKING).request(bookingDTO);
        Try.of(() -> {
            membershipService.deleteBookingTrackingUpdateBalance(bookingDTO.getId());
            return null;
        })
                .onFailure(throwable -> builder.throwable(throwable)
                        .result(ApiCall.Result.FAIL)
                        .message(throwable.getMessage()))
                .onSuccess(response -> builder.result(ApiCall.Result.SUCCESS)
                        .message(BOOKING_TRACKING_PREFIX_MSG + bookingDTO.getId() + " deleted!"));
        return builder.build();
    }
}
