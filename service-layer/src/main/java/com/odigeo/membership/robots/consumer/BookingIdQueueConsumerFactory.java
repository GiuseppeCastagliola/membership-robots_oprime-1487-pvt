package com.odigeo.membership.robots.consumer;

import java.util.concurrent.BlockingQueue;

public interface BookingIdQueueConsumerFactory {
    BookingIdQueueConsumer create(BlockingQueue<Long> bookingIdQueue);
}
