package com.odigeo.membership.robots.manager.bookingapi.product;

import java.util.Arrays;

public enum ProductMembershipPrimeBookingItemType {

    MEMBERSHIP_SUBSCRIPTION,
    MEMBERSHIP_RENEWAL;

    public static boolean isProductMembershipPrime(String product) {
        return Arrays.stream(ProductMembershipPrimeBookingItemType.values())
                .anyMatch(productMembershipPrime -> productMembershipPrime.name().equals(product));
    }
}
