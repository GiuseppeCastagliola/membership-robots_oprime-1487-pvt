package com.odigeo.membership.robots.consumer.updater;

import com.google.inject.Inject;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

public class UpdaterBean implements Updater {
    private final BookingUpdater bookingUpdater;
    private final MembershipUpdater membershipUpdater;

    @Inject
    public UpdaterBean(BookingUpdater bookingUpdater, MembershipUpdater membershipUpdater) {
        this.bookingUpdater = bookingUpdater;
        this.membershipUpdater = membershipUpdater;
    }

    @Override
    public MembershipDTO saveRecurringId(MembershipDTO membershipDTO, BookingDTO bookingDTO) {
        return membershipUpdater.saveRecurringId(membershipDTO, bookingDTO);
    }

    @Override
    public MembershipDTO activateMembership(MembershipDTO membershipDTO, BookingDTO bookingDTO) {
        return membershipUpdater.activateMembership(membershipDTO, bookingDTO);
    }

    @Override
    public BookingTrackingDTO trackBookingAndUpdateBalance(BookingDTO bookingDTO) {
        return membershipUpdater.trackBookingAndUpdateBalance(bookingDTO);
    }

    @Override
    public void deleteBookingTracking(BookingDTO bookingDTO) {
        membershipUpdater.deleteBookingTracking(bookingDTO);
    }

    @Override
    public void balanceBooking(BookingDTO bookingDTO, MembershipDTO membershipDTO) {
        bookingUpdater.updateBookingWithUnbalancedFees(bookingDTO, membershipDTO);
    }

    @Override
    public MembershipDTO updateBookingBasicFreeFirstBooking(BookingDTO bookingDTO, MembershipDTO membershipDTO) {
        if (bookingUpdater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO)) {
            return membershipUpdater.enableAutoRenewal(membershipDTO);
        } else {
            return membershipDTO;
        }
    }
}
