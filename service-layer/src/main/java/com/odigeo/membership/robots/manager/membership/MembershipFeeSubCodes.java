package com.odigeo.membership.robots.manager.membership;

public enum MembershipFeeSubCodes {
    AE10("AE10"),
    AE11("AE11"),
    AC12("AC12"),
    AVOID_FEES("AB35"),
    COST_FEE("AB32"),
    PERKS("AB34");

    private String feeCode;

    MembershipFeeSubCodes(String feeCode) {
        this.feeCode = feeCode;
    }

    public String getFeeCode() {
        return feeCode;
    }
}
