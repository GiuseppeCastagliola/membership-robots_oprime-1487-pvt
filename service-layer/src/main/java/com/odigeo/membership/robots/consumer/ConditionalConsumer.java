package com.odigeo.membership.robots.consumer;

import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

public final class ConditionalConsumer<T, U> implements BiConsumer<T, U> {
    private final Consumer<T> consumer;
    private final Predicate<U> predicate;

    private ConditionalConsumer(Consumer<T> consumer, Predicate<U> predicate) {
        this.consumer = consumer;
        this.predicate = predicate;
    }

    static ConditionalConsumer<MembershipDTO, MembershipDTO> ofMembership(Consumer<MembershipDTO> membershipDTOConsumer, Predicate<MembershipDTO> membershipDTOPredicate) {
        return new ConditionalConsumer<>(membershipDTOConsumer, membershipDTOPredicate);
    }

    static ConditionalConsumer<MembershipProcessor, BookingDTO> ofBooking(Consumer<MembershipProcessor> membershipProcessorConsumer, Predicate<BookingDTO> bookingDTOPredicate) {
        return new ConditionalConsumer<>(membershipProcessorConsumer, bookingDTOPredicate);
    }

    static ConditionalConsumer<MembershipBookingTrackingProcessor, MembershipDTO> ofMembershipBookingTracking(Consumer<MembershipBookingTrackingProcessor> membershipBookingTrackingProcessorConsumer, Predicate<MembershipDTO> membershipDTOPredicate) {
        return new ConditionalConsumer<>(membershipBookingTrackingProcessorConsumer, membershipDTOPredicate);
    }

    @Override
    public void accept(T consumerArgument, U predicateArgument) {
        if (predicate.test(predicateArgument)) {
            consumer.accept(consumerArgument);
        }
    }

}
