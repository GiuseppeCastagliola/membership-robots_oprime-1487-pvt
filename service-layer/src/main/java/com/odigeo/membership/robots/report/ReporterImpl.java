package com.odigeo.membership.robots.report;

import com.google.inject.Singleton;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.function.Function;

@Singleton
public class ReporterImpl implements Reporter {
    private static final String FORMAT_MESSAGE = "[Membership %s %s %s]";
    private static final Function<ApiCallWrapper<MembershipDTO, MembershipDTO>, String> LOG_MEMBERSHIP_DTO_WRAPPER = callWrapper -> String
                        .format(FORMAT_MESSAGE, getMembershipIdAndSource(callWrapper), callWrapper.getEndpoint().name(), callWrapper.getResult().name());
    private static final Logger LOGGER = LoggerFactory.getLogger(ReporterImpl.class);

    private static String getMembershipIdAndSource(ApiCallWrapper<MembershipDTO, MembershipDTO> callWrapper) {
        long id;
        String source;
        MembershipDTO request = callWrapper.getRequest();

        if (ApiCall.Result.FAIL.equals(callWrapper.getResult())) {
            id = request.getId();
            source = " (request)";
        } else {
            Optional<MembershipDTO> response = Optional.ofNullable(callWrapper.getResponse());
            id = response.orElse(request).getId();
            source = response.isPresent() ? " (response)" : " (request)";
        }

        return id + source;
    }

    @Override
    public void generateReportLog(Report report) {
        LOGGER.info("EXPIRATION SUCCESS REPORT: {} ", report.getSuccessJoiner());
        LOGGER.error("EXPIRATION ERROR REPORT: {}", report.getErrorJoiner());
    }

    @Override
    public void feedReport(ApiCallWrapper<MembershipDTO, MembershipDTO> callWrapper, Report report) {
        LOG_MEMBERSHIP_DTO_WRAPPER.andThen(callWrapper.isSuccessful() ? report.getSuccessJoiner()::add : report.getErrorJoiner()::add).apply(callWrapper);
    }
}
