package com.odigeo.membership.robots.metrics;

import com.google.inject.Singleton;
import com.odigeo.commons.monitoring.metrics.Metric.Builder;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;

@Singleton
public class MembershipRobotsMetrics {
    private static final String METRICS_REGISTRY_NAME = "membership-robots";

    public void incrementCounter(MetricsName name) {
        MetricsUtils.incrementCounter(new Builder(name.getId()).build(), METRICS_REGISTRY_NAME);
    }

    public void incrementCounter(MetricsName name, MetricsName.TagName tagName, String tagValue) {
        MetricsUtils.incrementCounter(new Builder(name.getId()).tag(tagName.name(), tagValue).build(), METRICS_REGISTRY_NAME);
    }
}
