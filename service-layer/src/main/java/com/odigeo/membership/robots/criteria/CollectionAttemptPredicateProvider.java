package com.odigeo.membership.robots.criteria;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Singleton;
import com.odigeo.membership.robots.dto.booking.CollectionAttemptDTO;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.function.Predicate;

import static com.odigeo.membership.robots.criteria.LoggingPredicateProvider.loggingPredicate;

@Singleton
public class CollectionAttemptPredicateProvider {

    private Predicate<CollectionAttemptDTO> loggedPredicate(String conditionText, Predicate<CollectionAttemptDTO> collectionAttemptDTOPredicate) {
        return loggingPredicate(collectionAttemptDTO -> "CollectionAttempt " + collectionAttemptDTO.getId() + StringUtils.SPACE + conditionText, collectionAttemptDTOPredicate);
    }

    @VisibleForTesting
    protected enum MovementsStatus {
        PAID, REFUNDED, CHARGEBACKED
    }

    @VisibleForTesting
    protected enum MovementsAction {
        CONFIRM, DIRECTY_PAYMENT, MANUAL_REFUND, CHARGEBACK
    }

    public Predicate<CollectionAttemptDTO> hasRecurringId() {
        return loggedPredicate("hasRecurringId", collectionAttemptDTO -> StringUtils.isNotEmpty(collectionAttemptDTO.getRecurringId()));
    }

    public Predicate<CollectionAttemptDTO> isLastMovementStatusAndActionValid() {
        return loggedPredicate("isLastMovementStatusAndActionValid", isLastMovementPaidAndConfirmedOrDirectyPayment()
                .or(isLastMovementRefunded())
                .or(isLastMovementChargedBack()));
    }

    protected Predicate<CollectionAttemptDTO> isLastMovementPaidAndConfirmedOrDirectyPayment() {
        return attempt -> attempt.getLastMovement().getStatus().equals(MovementsStatus.PAID.name())
                && Arrays.asList(MovementsAction.CONFIRM.name(), MovementsAction.DIRECTY_PAYMENT.name())
                .contains(attempt.getLastMovement().getAction());

    }

    protected Predicate<CollectionAttemptDTO> isLastMovementRefunded() {
        return attempt -> attempt.getLastMovement().getStatus().equals(MovementsStatus.REFUNDED.name())
                && Arrays.asList(MovementsAction.MANUAL_REFUND.name(), MovementsAction.DIRECTY_PAYMENT.name())
                .contains(attempt.getLastMovement().getAction());
    }

    protected Predicate<CollectionAttemptDTO> isLastMovementChargedBack() {
        return attempt -> attempt.getLastMovement().getStatus().equals(MovementsStatus.CHARGEBACKED.name())
                && StringUtils.equals(attempt.getLastMovement().getAction(), MovementsAction.CHARGEBACK.name());
    }


}
