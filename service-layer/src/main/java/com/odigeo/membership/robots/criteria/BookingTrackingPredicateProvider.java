package com.odigeo.membership.robots.criteria;

import com.google.inject.Singleton;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

import java.util.Objects;
import java.util.function.Predicate;

import static com.odigeo.membership.robots.criteria.LoggingPredicateProvider.loggingPredicate;
import static java.util.Objects.nonNull;

@Singleton
public class BookingTrackingPredicateProvider {

    private Predicate<BookingTrackingDTO> loggedPredicate(String conditionText, Predicate<BookingTrackingDTO> bookingTrackingDTOPredicate) {
        return loggingPredicate(bookingTrackingDTO ->
                nonNull(bookingTrackingDTO) ? "TrackingBooking (booking: " + bookingTrackingDTO.getBookingId()
                        + ", membership: " + bookingTrackingDTO.getMembershipId() + ") " + conditionText
                        : "TrackingBooking not present " + conditionText, bookingTrackingDTOPredicate);
    }

    public Predicate<BookingTrackingDTO> isAlreadyTrackedAndFeesAreChanged(BookingDTO bookingDTO) {
        return loggedPredicate("isAlreadyTrackedAndFeesAreChanged", isAlreadyTracked().and(
                avoidFeeChanged(bookingDTO)
                        .or(costFeeChanged(bookingDTO))
                        .or(perksChanged(bookingDTO)))
        );
    }

    private Predicate<BookingTrackingDTO> isAlreadyTracked() {
        return Objects::nonNull;
    }

    private Predicate<BookingTrackingDTO> avoidFeeChanged(BookingDTO bookingDTO) {
        return loggedPredicate("avoidFeeChanged", bookingTrackingDTO -> bookingDTO.getTotalAvoidFeesAmount()
                .compareTo(bookingTrackingDTO.getAvoidFeeAmount()) != 0);
    }

    private Predicate<BookingTrackingDTO> perksChanged(BookingDTO bookingDTO) {
        return loggedPredicate("perksChanged", bookingTrackingDTO -> bookingDTO.getTotalPerksAmount()
                .compareTo(bookingTrackingDTO.getPerksAmount()) != 0);
    }

    private Predicate<BookingTrackingDTO> costFeeChanged(BookingDTO bookingDTO) {
        return loggedPredicate("costFeeChanged", bookingTrackingDTO -> bookingDTO.getTotalCostFeesAmount()
                .compareTo(bookingTrackingDTO.getCostFeeAmount()) != 0);
    }

}
