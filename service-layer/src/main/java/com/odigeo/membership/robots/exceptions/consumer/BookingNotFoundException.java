package com.odigeo.membership.robots.exceptions.consumer;

public class BookingNotFoundException extends RuntimeException {

    public BookingNotFoundException(Throwable throwable) {
        super(throwable);
    }

    public BookingNotFoundException(String message) {
        super(message);
    }
}
