package com.odigeo.membership.robots.criteria;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.BookingStatus;
import com.odigeo.membership.robots.dto.booking.ProductCategoryBookingDTO;
import com.odigeo.membership.robots.manager.bookingapi.product.ProductMembershipPrimeBookingItemType;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.Optional;
import java.util.function.Predicate;

import static com.odigeo.membership.robots.criteria.LoggingPredicateProvider.loggingPredicate;
import static com.odigeo.membership.robots.manager.bookingapi.product.ProductMembershipPrimeBookingItemType.MEMBERSHIP_SUBSCRIPTION;

@Singleton
public class BookingPredicateProvider {
    @Inject
    private CollectionAttemptPredicateProvider collectionAttemptCriteria;

    private Predicate<BookingDTO> loggedPredicate(String conditionText, Predicate<BookingDTO> bookingDTOPredicate) {
        return loggingPredicate(bookingDto -> "Booking " + bookingDto.getId() + StringUtils.SPACE + conditionText, bookingDTOPredicate);
    }

    public Predicate<BookingDTO> isContract() {
        return loggedPredicate("isContract", bookingDTO -> StringUtils.equals(BookingStatus.CONTRACT.name(), bookingDTO.getBookingStatus()));
    }

    public Predicate<BookingDTO> isRequest() {
        return loggedPredicate("isRequest", bookingDTO -> StringUtils.equals(BookingStatus.REQUEST.name(), bookingDTO.getBookingStatus()));
    }


    public Predicate<BookingDTO> hasValidMovementForRecurring() {
        return loggedPredicate("hasValidMovement", bookingDTO -> Optional.of(bookingDTO)
                .map(BookingDTO::getCollectionAttempts)
                .orElse(Collections.emptyList())
                .stream()
                .filter(collectionAttemptCriteria.hasRecurringId())
                .anyMatch(collectionAttemptCriteria.isLastMovementStatusAndActionValid()));
    }

    public Predicate<BookingDTO> isBookingWithSubscription() {
        return loggedPredicate("isBookingWithSubscription", bookingDTO -> bookingDTO.getBookingProducts()
                .stream()
                .map(ProductCategoryBookingDTO::getProductType)
                .anyMatch(productType -> productType.equals(MEMBERSHIP_SUBSCRIPTION.name())));
    }

    public Predicate<BookingDTO> isBookingWithRenewalOrSubscription() {
        return loggedPredicate("isBookingWithRenewalOrSubscription", bookingDTO -> bookingDTO.getBookingProducts()
                .stream()
                .map(ProductCategoryBookingDTO::getProductType)
                .anyMatch(ProductMembershipPrimeBookingItemType::isProductMembershipPrime));
    }

    public Predicate<BookingDTO> arePerksPresent() {
        return loggedPredicate("arePerksPresent", bookingDTO -> bookingDTO.getFeeContainers().stream()
                .anyMatch(feeContainerDTO -> StringUtils.equals(feeContainerDTO.getFeeContainerType(), "MEMBERSHIP_PERKS")));
    }

    public Predicate<BookingDTO> isPrimeBooking() {
        return loggedPredicate("isPrimeBooking", bookingDto -> Optional.ofNullable(bookingDto.getMembershipId())
                .map(membershipId -> membershipId > 0).orElse(false));
    }

    public Predicate<BookingDTO> isTestBooking() {
        return loggedPredicate("isTestBooking", BookingDTO::isTestBooking);
    }

    public Predicate<BookingDTO> isNotTestAndIsPrime() {
        return isTestBooking().negate().and(isPrimeBooking());
    }


    public Predicate<BookingDTO> hasValidStatusForRecurring() {
        return isContract().or(isRequest());
    }

    public Predicate<BookingDTO> eligibleForTracking() {
        return loggedPredicate("eligibleForTracking", isContract().and(arePerksPresent()));
    }

    public Predicate<BookingDTO> eligibleForTrackingUpdate() {
        return loggedPredicate("eligibleForTrackingUpdate", isContract().negate().and(arePerksPresent()));
    }

    public Predicate<BookingDTO> eligibleForActivation() {
        return loggedPredicate("eligibleForActivation", isContract().and(isBookingWithSubscription()));
    }

    public Predicate<BookingDTO> eligibleForRecurring() {
        return loggedPredicate("eligibleForRecurring", isBookingWithRenewalOrSubscription()
                .and(hasValidStatusForRecurring())
                .and(hasValidMovementForRecurring()));
    }

    public Predicate<BookingDTO> eligibleForBasicFreeUpdate() {
        return loggedPredicate("eligibleForBasicFreeUpdate", isBookingWithSubscription().negate().and(eligibleForTracking()));
    }
}
