package com.odigeo.membership.robots.consumer;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.robots.configuration.ConsumerRetriesConfig;
import com.odigeo.membership.robots.exceptions.consumer.BookingNotFoundException;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.FailsafeExecutor;
import net.jodah.failsafe.RetryPolicy;
import net.jodah.failsafe.event.ExecutionAttemptedEvent;
import net.jodah.failsafe.event.ExecutionCompletedEvent;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Objects.nonNull;

public class BookingIdQueueConsumer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingIdQueueConsumer.class);
    private static final long PULL_WAITING_SECONDS = 10L;
    private RetryPolicy<Object> retryPolicy;
    private RetryPolicy<Object> bookingNotFoundRetryPolicy;
    private final BlockingQueue<Long> bookingIdQueue;
    @VisibleForTesting
    protected final AtomicBoolean interrupt;
    private final Processor bookingProcessor;
    private final MembershipRobotsMetrics membershipRobotsMetrics;

    @Inject
    protected BookingIdQueueConsumer(@Assisted BlockingQueue<Long> bookingIdQueue, Processor bookingProcessor,
                                     MembershipRobotsMetrics membershipRobotsMetrics, ConsumerRetriesConfig retriesConfig) {
        this.bookingIdQueue = bookingIdQueue;
        this.bookingProcessor = bookingProcessor;
        this.membershipRobotsMetrics = membershipRobotsMetrics;
        interrupt = new AtomicBoolean(false);
        ChronoUnit chronoUnit = ChronoUnit.valueOf(retriesConfig.getChronoUnit());
        retryPolicy = new RetryPolicy<>()
                .handle(RetryableException.class)
                .withBackoff(retriesConfig.getDelay(), retriesConfig.getMaxDelay(), chronoUnit)
                .withJitter(Duration.of(retriesConfig.getJitter(), chronoUnit))
                .withMaxRetries(retriesConfig.getMaxRetries());
        bookingNotFoundRetryPolicy = new RetryPolicy<>()
                .handle(BookingNotFoundException.class)
                .withDelay(Duration.of(retriesConfig.getNotFoundDelay(), chronoUnit))
                .withJitter(Duration.of(retriesConfig.getNotFoundJitter(), chronoUnit))
                .withMaxRetries(retriesConfig.getNotFoundMaxRetries());
    }

    @Override
    public void run() {
        while (!interrupt.get()) {
            FailsafeExecutor<Object> failsafeExecutor = Failsafe.with(retryPolicy, bookingNotFoundRetryPolicy);
            try {
                Long bookingId = bookingIdQueue.poll(PULL_WAITING_SECONDS, TimeUnit.SECONDS);
                if (nonNull(bookingId)) {
                    LOGGER.info("Bookings id queue consumer pull from queue booking: {}", bookingId);
                    membershipRobotsMetrics.incrementCounter(MetricsName.TOTAL_PROCESSED_BOOKING);
                    setupPolicyListeners(bookingId);
                    failsafeExecutor.onSuccess(success -> successListener(bookingId, success))
                            .runAsync(() -> bookingProcessor.process(bookingId));
                } else {
                    LOGGER.info("No messages to pull in the queue for last {} seconds", PULL_WAITING_SECONDS);
                }
            } catch (InterruptedException e) {
                LOGGER.error(e.getMessage(), e);
                Thread.currentThread().interrupt();
                break;
            }
        }
        LOGGER.info("Bookings id queue consumer interrupted");
        interrupt.set(Boolean.FALSE);
    }

    private void setupPolicyListeners(Long bookingId) {
        retryPolicy
                .onRetriesExceeded(event -> retriesExceededListener(bookingId, event))
                .onFailedAttempt(failure -> failedAttemptListener(bookingId, failure));
        bookingNotFoundRetryPolicy
                .onRetriesExceeded(event -> retriesExceededListener(bookingId, event))
                .onFailedAttempt(failure -> failedNotFoundAttemptListener(bookingId, failure));
    }

    private void successListener(Long bookingId, ExecutionCompletedEvent<Object> success) {
        membershipRobotsMetrics.incrementCounter(MetricsName.BOOKING_PROCESSING_COMPLETED);
        LOGGER.info("Booking {} processing completed in {} at attempt #{}", bookingId, success.getElapsedTime().toMillis(), success.getAttemptCount());
    }

    private void retriesExceededListener(Long bookingId, ExecutionCompletedEvent<Object> failure) {
        membershipRobotsMetrics.incrementCounter(MetricsName.TOTAL_BOOKING_FAILED, MetricsName.TagName.CAUSE, acceptedTagFromMessage(failure.getFailure().getMessage()));
        LOGGER.warn("Booking {} retries exceeded after {} attempts, will not be scheduled again", bookingId, failure.getAttemptCount());
        LOGGER.warn("Booking {} last exception message: {} - {}", bookingId, failure.getFailure().getMessage(), failure.getFailure());
    }

    private void failedNotFoundAttemptListener(Long bookingId, ExecutionAttemptedEvent<Object> failure) {
        membershipRobotsMetrics.incrementCounter(MetricsName.BOOKING_NOT_FOUND_FAIL, MetricsName.TagName.ATTEMPT_NUMBER, String.valueOf(failure.getAttemptCount()));
        LOGGER.warn("Booking {} not found at attempt #{}, exception message: {} - {}", bookingId, failure.getAttemptCount(), failure.getLastFailure().getMessage(), failure.getLastFailure());
    }

    private void failedAttemptListener(Long bookingId, ExecutionAttemptedEvent<Object> failure) {
        membershipRobotsMetrics.incrementCounter(MetricsName.BOOKING_PROCESSING_FAILED, MetricsName.TagName.ATTEMPT_NUMBER, String.valueOf(failure.getAttemptCount()));
        LOGGER.warn("Booking {} processing failed at attempt #{}, exception message: {} - {}", bookingId, failure.getAttemptCount(), failure.getLastFailure().getMessage(), failure.getLastFailure());
    }

    private String acceptedTagFromMessage(String message) {
        return StringUtils.replaceChars(message, ".", "_");
    }

    void stop() {
        interrupt.set(Boolean.TRUE);
    }

    @VisibleForTesting
    void setRetryPolicy(RetryPolicy<Object> retryPolicy) {
        this.retryPolicy = retryPolicy;
    }

    @VisibleForTesting
    void setBookingNotFoundRetryPolicy(RetryPolicy<Object> retryPolicy) {
        this.bookingNotFoundRetryPolicy = retryPolicy;
    }

}
