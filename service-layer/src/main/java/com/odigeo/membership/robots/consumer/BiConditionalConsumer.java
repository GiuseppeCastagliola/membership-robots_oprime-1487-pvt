package com.odigeo.membership.robots.consumer;

import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Consumer;

final class BiConditionalConsumer<T, U, V> {
    private final Consumer<T> consumer;
    private final BiPredicate<U, V> predicate;
    private BiConditionalConsumer<T, U, V> alternativeConsumer;

    private BiConditionalConsumer(Consumer<T> consumer, BiPredicate<U, V> predicate) {
        this.consumer = consumer;
        this.predicate = predicate;
    }

    static BiConditionalConsumer<BookingDTO, MembershipDTO, BookingTrackingDTO> ofMembershipBookingTracking(Consumer<BookingDTO> bookingDTOConsumer, BiPredicate<MembershipDTO, BookingTrackingDTO> membershipDTOPredicate) {
        return new BiConditionalConsumer<>(bookingDTOConsumer, membershipDTOPredicate);
    }

    BiConditionalConsumer<T, U, V> orElseThen(BiConditionalConsumer<T, U, V> alternativeConsumer) {
        this.alternativeConsumer = alternativeConsumer;
        return this;
    }

    void accept(T consumerArgument, U predicateFirstArgument, V predicateSecondArgument) {
        if (predicate.test(predicateFirstArgument, predicateSecondArgument)) {
            consumer.accept(consumerArgument);
        } else {
            Optional.ofNullable(alternativeConsumer)
                    .ifPresent(altConsumer -> altConsumer.accept(consumerArgument, predicateFirstArgument, predicateSecondArgument));
        }
    }
}
