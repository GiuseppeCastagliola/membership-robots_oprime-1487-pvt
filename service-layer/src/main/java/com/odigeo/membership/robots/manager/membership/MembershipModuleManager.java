package com.odigeo.membership.robots.manager.membership;

import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;

public interface MembershipModuleManager {
    ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembership(MembershipDTO membershipDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> discardMembership(MembershipDTO membershipDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> deactivateMembership(MembershipDTO membershipDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> enableAutoRenewal(MembershipDTO membershipDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> saveRecurringId(MembershipDTO membershipWithRecurring);

    ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingToCollect(ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper);

    ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantMembershipBalance(MembershipDTO membershipToConsume);

    ApiCallWrapper<MembershipDTO, MembershipDTO> activateMembership(MembershipDTO membershipToActivate, BookingDTO bookingDTO);
}
