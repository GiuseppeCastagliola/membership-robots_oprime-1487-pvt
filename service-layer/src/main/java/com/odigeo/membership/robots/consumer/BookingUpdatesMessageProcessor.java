package com.odigeo.membership.robots.consumer;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.commons.messaging.MessageProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;

@Singleton
public class BookingUpdatesMessageProcessor implements MessageProcessor<BasicMessage> {
    private static final int QUEUE_SIZE = 10;
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingUpdatesMessageProcessor.class);
    private final BlockingQueue<Long> bookingIdUpdated;
    private final BookingIdQueueConsumer bookingIdQueueConsumer;
    private CompletableFuture<Void> consumerFuture;

    @Inject
    public BookingUpdatesMessageProcessor(BookingIdQueueConsumerFactory queueConsumerFactory) {
        bookingIdUpdated = new LinkedBlockingQueue<>(QUEUE_SIZE);
        bookingIdQueueConsumer = queueConsumerFactory.create(bookingIdUpdated);
    }

    @VisibleForTesting
    protected BookingUpdatesMessageProcessor(BlockingQueue<Long> bookingIdUpdated, BookingIdQueueConsumer bookingIdQueueConsumer) {
        this.bookingIdUpdated = bookingIdUpdated;
        this.bookingIdQueueConsumer = bookingIdQueueConsumer;
    }

    @Override
    public void onMessage(BasicMessage basicMessage) {
        putBookingIdInProcessingQueue(Long.parseLong(basicMessage.getKey()));
        LOGGER.info("BOOKING_UPDATES_v1 message pulled from kafka queue, booking id {}, pushed in queue, queue size: {}", basicMessage.getKey(), bookingIdUpdated.size());
    }

    public void stop() throws InterruptedException {
        while (!bookingIdUpdated.isEmpty()) {
            LOGGER.info("Waiting queue consumption before interrupt consumer");
            TimeUnit.SECONDS.sleep(5L);
        }
        bookingIdQueueConsumer.stop();
    }

    public void putBookingIdInProcessingQueue(long bookingId) {
        startConsumerThreadIfNotRunning();
        try {
            bookingIdUpdated.put(bookingId);
        } catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
            Thread.currentThread().interrupt();
        }
    }

    private void startConsumerThreadIfNotRunning() {
        if (isNull(consumerFuture) || consumerFuture.isDone()) {
            consumerFuture = CompletableFuture.runAsync(bookingIdQueueConsumer);
            LOGGER.info("New consumer thread launched {}", consumerFuture);
        }
    }
}
