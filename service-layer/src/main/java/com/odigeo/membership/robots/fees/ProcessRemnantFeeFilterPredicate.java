package com.odigeo.membership.robots.fees;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.Optional;
import java.util.function.Predicate;

public final class ProcessRemnantFeeFilterPredicate {

    static final String INIT = "INIT";
    static final String EXPIRED = "EXPIRED";
    static final LocalDateTime CUTOVER_DATE = LocalDate.of(2019, Month.SEPTEMBER, 30).atTime(LocalTime.MAX);
    private static final Predicate<ApiCallWrapper<MembershipDTO, MembershipDTO>> SUCCESSFUL_CALL = ApiCall::isSuccessful;
    private static final Predicate<ApiCallWrapper<MembershipDTO, MembershipDTO>> IS_NOT_NULL_AND_EXPIRED = membershipWrapper -> Optional.ofNullable(membershipWrapper.getResponse())
            .map(MembershipDTO::getStatus)
            .map(EXPIRED::equals)
            .orElse(Boolean.FALSE);
    private static final Predicate<ApiCallWrapper<MembershipDTO, MembershipDTO>> ACTIVATED_AFTER_CUTOVER = membershipWrapper -> Optional.ofNullable(membershipWrapper.getResponse())
            .map(MembershipDTO::getActivationDate)
            .map(CUTOVER_DATE::isBefore)
            .orElse(Boolean.FALSE);
    private static final Predicate<ApiCallWrapper<MembershipDTO, MembershipDTO>> WITH_POSITIVE_BALANCE = membershipWrapper -> Optional.ofNullable(membershipWrapper.getResponse())
            .map(MembershipDTO::getBalance)
            .map(balance -> balance.compareTo(BigDecimal.ZERO) > 0)
            .orElse(Boolean.FALSE);
    private static final Predicate<ApiCallWrapper<MembershipDTO, MembershipDTO>> IS_NOT_INIT_RENEWAL = membershipWrapper -> Optional.ofNullable(membershipWrapper.getResponse())
            .map(MembershipDTO::getProductStatus)
            .map(productStatus -> !INIT.equalsIgnoreCase(productStatus))
            .orElse(Boolean.TRUE);
    public static final Predicate<ApiCallWrapper<MembershipDTO, MembershipDTO>> PROCESS_REMNANT_FEE_CHECK = SUCCESSFUL_CALL
            .and(IS_NOT_NULL_AND_EXPIRED)
            .and(ACTIVATED_AFTER_CUTOVER)
            .and(WITH_POSITIVE_BALANCE)
            .and(IS_NOT_INIT_RENEWAL);

    private ProcessRemnantFeeFilterPredicate() {
    }
}
