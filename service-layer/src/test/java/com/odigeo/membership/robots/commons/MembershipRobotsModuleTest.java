package com.odigeo.membership.robots.commons;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.util.Providers;
import com.odigeo.bookingapi.v12.BookingApiService;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.offer.api.MembershipOfferService;
import com.odigeo.membership.robots.configuration.MembershipRobotsModule;
import com.odigeo.membership.robots.consumer.MembershipProcessorFactory;
import com.odigeo.membership.robots.mapper.request.MembershipOfferRequestMapper;
import com.odigeo.membership.robots.mapper.request.MembershipRequestMapper;
import com.odigeo.membership.robots.mapper.response.BookingResponseMapper;
import com.odigeo.membership.robots.mapper.response.MembershipOfferResponseMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import com.odigeo.membership.robots.mapper.response.consumer.DeactivateMembershipByChargebackMapper;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class MembershipRobotsModuleTest {

    @Test
    public void testModuleConfiguration() {
        ConfigurationEngine.init(new MembershipRobotsModule(),
                binder -> binder.bind(MembershipService.class).toProvider(Providers.of(null)),
                binder -> binder.bind(MembershipOfferService.class).toProvider(Providers.of(null)),
                binder -> binder.bind(MembershipSearchApi.class).toProvider(Providers.of(null)),
                binder -> binder.bind(BookingApiService.class).toProvider(Providers.of(null)),
                binder -> binder.bind(BookingSearchApiService.class).toProvider(Providers.of(null)),
                binder -> binder.bind(MembershipProcessorFactory.class).toProvider(Providers.of(null))
                );
        assertNotNull(ConfigurationEngine.getInstance(MembershipRequestMapper.class));
        assertNotNull(ConfigurationEngine.getInstance(MembershipResponseMapper.class));
        assertNotNull(ConfigurationEngine.getInstance(MembershipOfferResponseMapper.class));
        assertNotNull(ConfigurationEngine.getInstance(MembershipOfferRequestMapper.class));
        assertNotNull(ConfigurationEngine.getInstance(BookingResponseMapper.class));
        assertNotNull(ConfigurationEngine.getInstance(DeactivateMembershipByChargebackMapper.class));
    }
}