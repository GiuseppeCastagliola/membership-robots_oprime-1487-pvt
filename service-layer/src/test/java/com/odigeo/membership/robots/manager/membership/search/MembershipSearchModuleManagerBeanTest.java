package com.odigeo.membership.robots.manager.membership.search;

import com.odigeo.membership.MembershipSearchApi;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.response.search.MembershipResponse;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipDTOBuilder;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.mapper.request.MembershipSearchRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipSearchModuleManagerBeanTest {

    private static final Long MEMBERSHIP_ID = 123L;
    private static final MembershipDTO MEMBERSHIP_DTO = new MembershipDTOBuilder().id(MEMBERSHIP_ID).build();
    private static final String INTERNAL_ERROR_MSG = "FAILED";
    private static final MembershipInternalServerErrorException INTERNAL_SERVER_ERROR_EXCEPTION = new MembershipInternalServerErrorException(INTERNAL_ERROR_MSG);
    private static final UndeclaredThrowableException UNDECLARED_THROWABLE_EXCEPTION = new UndeclaredThrowableException(INTERNAL_SERVER_ERROR_EXCEPTION);


    @Mock
    private MembershipSearchApi membershipSearchApi;

    private MembershipSearchModuleManager membershipSearchModuleManager;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        membershipSearchModuleManager = new MembershipSearchModuleManagerBean(membershipSearchApi, Mappers.getMapper(MembershipSearchRequestMapper.class), Mappers.getMapper(MembershipResponseMapper.class));
    }

    @Test
    public void testSearchMemberships() {
        given(membershipSearchApi.searchMemberships(any(MembershipSearchRequest.class))).willReturn(Collections.emptyList());
        final ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchMemberships = membershipSearchModuleManager.searchMemberships(SearchMembershipsDTO.builder().build());
        assertTrue(searchMemberships.isSuccessful());
        assertNull(searchMemberships.getThrowable());
        assertNotNull(searchMemberships.getResponse());
    }

    @Test
    public void testSearchMembershipsWithException() {
        given(membershipSearchApi.searchMemberships(any(MembershipSearchRequest.class))).willThrow(INTERNAL_SERVER_ERROR_EXCEPTION);
        final ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchMemberships = membershipSearchModuleManager.searchMemberships(SearchMembershipsDTO.builder().build());
        assertFalse(searchMemberships.isSuccessful());
        assertEquals(searchMemberships.getThrowable(), INTERNAL_SERVER_ERROR_EXCEPTION);
        assertEquals(searchMemberships.getMessage(), INTERNAL_ERROR_MSG);
        assertNull(searchMemberships.getResponse());
    }

    @Test
    public void testGetMembership() {
        MembershipResponse membershipResponse = new MembershipResponse();
        membershipResponse.setId(MEMBERSHIP_ID);
        given(membershipSearchApi.getMembership(MEMBERSHIP_ID, true)).willReturn(membershipResponse);
        ApiCallWrapper<MembershipDTO, Long> membership = membershipSearchModuleManager.getMembership(MEMBERSHIP_ID);
        assertEquals(membership.getResponse(), MEMBERSHIP_DTO);
        assertEquals(membership.getRequest(), MEMBERSHIP_ID);
        assertEquals(membership.getEndpoint(), ApiCall.Endpoint.GET_MEMBERSHIP);
        assertEquals(membership.getResult(), ApiCall.Result.SUCCESS);

    }

    @Test
    public void testGetMembershipWithExceptions() {
        given(membershipSearchApi.getMembership(MEMBERSHIP_ID, true)).willThrow(UNDECLARED_THROWABLE_EXCEPTION);
        ApiCallWrapper<MembershipDTO, Long> getMembership = membershipSearchModuleManager.getMembership(MEMBERSHIP_ID);
        assertFalse(getMembership.isSuccessful());
        assertEquals(getMembership.getThrowable(), UNDECLARED_THROWABLE_EXCEPTION);
        assertNull(getMembership.getResponse());
    }
}
