package com.odigeo.membership.robots.manager.bookingapi;

import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v12.response.FeeContainerBuilder;
import com.odigeo.bookingapi.v12.BadCredentialsException;
import com.odigeo.bookingapi.v12.BookingApiService;
import com.odigeo.bookingapi.v12.InvalidParametersException;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingBasicInfoBuilder;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingSummaryBuilder;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.BookingApiConfiguration;
import com.odigeo.membership.robots.configuration.BookingSearchApiConfiguration;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.bookingapi.BookingApiException;
import com.odigeo.membership.robots.mapper.request.BookingRequestMapper;
import com.odigeo.membership.robots.mapper.response.BookingResponseMapper;
import org.apache.commons.lang.StringUtils;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class BookingApiManagerBeanTest {
    private static final long BOOKING_ID = 123456789L;
    private static final long BOOKING_ID2 = 98769876L;
    private static final Long MEMBERSHIP_ID = 20202020L;
    private static final MembershipDTO MEMBERSHIP_DTO_ID1 = MembershipDTO.builder().id(MEMBERSHIP_ID).build();
    private static final long USER_ID = 121212L;
    private static final String ERROR_MESSAGE = "ERR_MSG";
    private static final String EXCEPTION_MESSAGE = "exception";
    private static final String CURRENCY_CODE = "EUR";

    @Mock
    private BookingApiService bookingApiService;
    @Mock
    private BookingSearchApiService bookingSearchApiService;
    @Mock
    private BookingApiConfiguration bookingApiConfiguration;
    @Mock
    private BookingSearchApiConfiguration bookingSearchApiConfiguration;

    private ApiCallWrapperBuilder<BookingDTO, BookingDTO> getBookingWrapperBuilder;
    private ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> searchBookingWrapperBuilder;
    private ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> updateBookingWrapperBuilder;
    private SearchBookingsPageResponse<List<BookingSummary>> searchBookingsPageResponse;
    private BookingDetail bookingDetail;

    private BookingApiManager bookingApiManager;

    @DataProvider(name = "bookingApiExceptions")
    static Object[][] bookingApiExceptions() {
        return new Object[][]{
                new Object[]{new UndeclaredThrowableException(new Exception(EXCEPTION_MESSAGE))},
                new Object[]{new InvalidParametersException(EXCEPTION_MESSAGE)},
                new Object[]{new BadCredentialsException(EXCEPTION_MESSAGE)}
        };
    }

    @BeforeMethod
    public void setUp() throws BuilderException, com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        openMocks(this);
        bookingApiManager = new BookingApiManagerBean(bookingApiService, bookingSearchApiService, bookingApiConfiguration, bookingSearchApiConfiguration, Mappers.getMapper(BookingRequestMapper.class), Mappers.getMapper(BookingResponseMapper.class));
        setupSearchPrimeBookingSummaryRequestAndResponse();
        setupGetBookingRequestAndResponse();
        setupUpdateBookingRequestAndResponse();
    }

    @Test
    public void testGetBooking() {
        givenAllBookingApisSuccessfulResponse();
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        assertTrue(bookingDTOApiCallWrapper.isSuccessful());
        assertNull(bookingDTOApiCallWrapper.getThrowable());
        assertEquals(bookingDTOApiCallWrapper.getResponse().getId(), BOOKING_ID);
    }

    @Test
    public void testGetBookingErrorMessage() {
        givenAllBookingApisReturnErrorMessages();
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        assertFalse(bookingDTOApiCallWrapper.isSuccessful());
        assertNull(bookingDTOApiCallWrapper.getThrowable());
        assertEquals(bookingDTOApiCallWrapper.getResult(), ApiCall.Result.FAIL);
        assertEquals(bookingDTOApiCallWrapper.getMessage(), ERROR_MESSAGE);
    }

    @Test
    public void testGetBookingNullResponse() {
        givenBookingApiNullResponse();
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        assertFalse(bookingDTOApiCallWrapper.isSuccessful());
        assertNull(bookingDTOApiCallWrapper.getResponse());
        assertEquals(bookingDTOApiCallWrapper.getResult(), ApiCall.Result.FAIL);
        assertEquals(bookingDTOApiCallWrapper.getMessage(), "No booking detail found for bookingId " + BOOKING_ID);
    }

    @Test
    public void testBookingApiUndeclaredThrowableGetBooking() {
        givenAllBookingApisThrowsException(new UndeclaredThrowableException(new Exception(EXCEPTION_MESSAGE)));
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        andWrapTheException(bookingDTOApiCallWrapper);
    }

    @Test
    public void testBookingApiInvalidParameterGetBooking() {
        givenAllBookingApisThrowsException(new InvalidParametersException(EXCEPTION_MESSAGE));
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        andWrapTheException(bookingDTOApiCallWrapper);
    }

    @Test
    public void testBookingApiBadCredentialsGetBooking() {
        givenAllBookingApisThrowsException(new BadCredentialsException(EXCEPTION_MESSAGE));
        ApiCallWrapper<BookingDTO, BookingDTO> bookingDTOApiCallWrapper = bookingApiManager.getBooking(getBookingWrapperBuilder);
        thenShouldInvokeApiGetBooking();
        andWrapTheException(bookingDTOApiCallWrapper);
    }

    @Test
    public void testSearchBookings() {
        givenAllBookingApisSuccessfulResponse();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertEquals(searchBookingsApiCallWrapper.getResponse().size(), 1);
        BookingDTO bookingDTO = searchBookingsApiCallWrapper.getResponse().get(0);
        assertEquals(bookingDTO.getMembershipId(), MEMBERSHIP_ID);
        assertEquals(bookingDTO.getId(), BOOKING_ID);
    }

    @Test
    public void testSearchNonPrimeBookings() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        setupSearchNonPrimeBookingRequestAndResponse();
        givenAllBookingApisSuccessfulResponse();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertEquals(searchBookingsApiCallWrapper.getMessage(), "1 non-Prime bookingSummaries found for membershipId " + MEMBERSHIP_ID);
        assertEquals(searchBookingsApiCallWrapper.getResult(), ApiCall.Result.SUCCESS);
        BookingDTO bookingDTO = searchBookingsApiCallWrapper.getResponse().get(0);
        assertEquals(bookingDTO.getMembershipId(), MEMBERSHIP_ID);
        assertEquals(bookingDTO.getId(), BOOKING_ID);
    }

    @Test
    public void testSearchBookingsNoBookingsFound() {
        setupSearchBookingRequestAndNoBookingsResponse();
        givenAllBookingApisSuccessfulResponse();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertEquals(searchBookingsApiCallWrapper.getMessage(), "0 Prime bookingSummaries found for membershipId " + MEMBERSHIP_ID);
        assertEquals(searchBookingsApiCallWrapper.getResult(), ApiCall.Result.SUCCESS);
        assertEquals(searchBookingsApiCallWrapper.getResponse().size(), 0);
    }

    @Test
    public void testSearchBookingsNullResponse() {
        setupSearchBookingRequestAndNullResponse();
        givenAllBookingApisSuccessfulResponse();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertEquals(searchBookingsApiCallWrapper.getResult(), ApiCall.Result.FAIL);
        assertNull(searchBookingsApiCallWrapper.getResponse());
    }

    @Test
    public void testGetPrimeSubscriptionBookingForMembership() {
        givenAllBookingApisSuccessfulResponse();
        ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper = bookingApiManager.getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO_ID1);

        thenShouldInvokeApiSearchBookings();
        thenShouldInvokeApiGetBooking();

        BookingDTO primeSubscriptionBooking = primeSubscriptionBookingWrapper.getResponse();
        assertNotNull(primeSubscriptionBooking);
        assertEquals(primeSubscriptionBooking.getId(), BOOKING_ID);
        assertEquals(primeSubscriptionBooking.getMembershipId(), MEMBERSHIP_ID);
        assertEquals(primeSubscriptionBooking.getUserId(), USER_ID);
        assertEquals(primeSubscriptionBooking.getCurrencyCode(), CURRENCY_CODE);
        assertEquals(primeSubscriptionBooking.getFeeContainers().size(), 1);
        assertEquals(primeSubscriptionBooking.getFeeContainers().get(0).getFeeDTOs().size(), 1);
    }

    @Test
    public void testGetPrimeSubscriptionBookingForMembershipBookingSummariesErrorMessage() {
        givenBookingSearchApiReturnsErrorMessage();
        ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper = bookingApiManager.getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO_ID1);

        thenShouldInvokeApiSearchBookings();
        thenShouldNotInvokeApiGetBooking();

        assertNull(primeSubscriptionBookingWrapper.getResponse());
    }

    @Test(dataProvider = "bookingApiExceptions")
    public void testGetPrimeSubscriptionBookingForMembershipBookingSummariesThrowsException(Exception ex) {
        givenBookingSearchApiThrowsException(ex);
        ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper = bookingApiManager.getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO_ID1);

        thenShouldInvokeApiSearchBookings();
        thenShouldNotInvokeApiGetBooking();

        assertNull(primeSubscriptionBookingWrapper.getResponse());
    }

    @Test
    public void testGetPrimeSubscriptionBookingForMembershipNoBookingSummaries() {
        setupSearchBookingRequestAndNoBookingsResponse();
        ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper = bookingApiManager.getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO_ID1);

        thenShouldInvokeApiSearchBookings();
        thenShouldNotInvokeApiGetBooking();

        assertNull(primeSubscriptionBookingWrapper.getResponse());
    }

    @Test
    public void testGetPrimeSubscriptionBookingForMembershipMultipleBookingSummaries() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        setupSearchPrimeBookingSummaryRequestAndMultipleResponses();
        ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper = bookingApiManager.getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO_ID1);

        thenShouldNotInvokeApiGetBooking();

        assertNull(primeSubscriptionBookingWrapper.getResponse());
    }

    @Test
    public void testGetPrimeSubscriptionBookingForMembershipBookingDetailsErrorMessage() {
        givenBookingSearchApiSuccessfulResponse();
        givenBookingApiReturnsErrorMessages();
        ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper = bookingApiManager.getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO_ID1);

        thenShouldInvokeApiSearchBookings();
        thenShouldInvokeApiGetBooking();

        assertNull(primeSubscriptionBookingWrapper.getResponse());
    }

    @Test(dataProvider = "bookingApiExceptions")
    public void testGetPrimeSubscriptionBookingForMembershipBookingDetailsThrowsException(Exception ex) {
        givenBookingSearchApiSuccessfulResponse();
        givenBookingApiThrowsException(ex);
        ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper = bookingApiManager.getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO_ID1);

        thenShouldInvokeApiSearchBookings();
        thenShouldInvokeApiGetBooking();

        assertNull(primeSubscriptionBookingWrapper.getResponse());
    }

    @Test
    public void testGetPrimeSubscriptionBookingForMembershipBookingDetailsNullResponse() {
        givenBookingSearchApiSuccessfulResponse();
        givenBookingApiNullResponse();
        ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBookingWrapper = bookingApiManager.getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO_ID1);

        thenShouldInvokeApiSearchBookings();
        thenShouldInvokeApiGetBooking();

        assertNull(primeSubscriptionBookingWrapper.getResponse());
    }

    private void setupSearchNonPrimeBookingRequestAndResponse() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.FALSE).membershipId(MEMBERSHIP_ID).build();
        searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS).request(searchBookingsRequest);
        searchBookingsPageResponse = new SearchBookingsPageResponse<>();
        List<BookingSummary> bookings = Collections.singletonList(new BookingSummaryBuilder()
                .bookingBasicInfo(new BookingBasicInfoBuilder().id(BOOKING_ID))
                .membershipId(MEMBERSHIP_ID).build(new Random()));
        searchBookingsPageResponse.setBookings(bookings);
    }

    private void setupSearchBookingRequestAndNullResponse() {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.TRUE).membershipId(MEMBERSHIP_ID).build();
        searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS).request(searchBookingsRequest);
        searchBookingsPageResponse = null;
    }

    private void setupSearchBookingRequestAndNoBookingsResponse() {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.TRUE).membershipId(MEMBERSHIP_ID).build();
        searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS).request(searchBookingsRequest);
        searchBookingsPageResponse = new SearchBookingsPageResponse<>();
    }

    @Test
    public void testSearchBookingErrorMessage() {
        givenAllBookingApisReturnErrorMessages();
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        assertFalse(searchBookingsApiCallWrapper.isSuccessful());
        assertNull(searchBookingsApiCallWrapper.getThrowable());
        assertEquals(searchBookingsApiCallWrapper.getMessage(), ERROR_MESSAGE);
    }

    @Test
    public void testBookingApiUndeclaredThrowableSearchBooking() {
        givenAllBookingApisThrowsException(new UndeclaredThrowableException(new Exception(EXCEPTION_MESSAGE)));
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        andWrapTheException(searchBookingsApiCallWrapper);
    }

    @Test
    public void testBookingApiInvalidParameterSearchBooking() {
        givenAllBookingApisThrowsException(new InvalidParametersException(EXCEPTION_MESSAGE));
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        andWrapTheException(searchBookingsApiCallWrapper);
    }

    @Test
    public void testBookingApiBadCredentialsSearchBooking() {
        givenAllBookingApisThrowsException(new BadCredentialsException(EXCEPTION_MESSAGE));
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsApiCallWrapper = bookingApiManager.searchBookings(searchBookingWrapperBuilder);
        thenShouldInvokeApiSearchBookings();
        andWrapTheException(searchBookingsApiCallWrapper);
    }

    @Test
    public void testUpdateBooking() {
        givenAllBookingApisSuccessfulResponse();
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        assertTrue(updateBookingApiCallWrapper.isSuccessful());
        assertNull(updateBookingApiCallWrapper.getThrowable());
        assertEquals(updateBookingApiCallWrapper.getResponse().getId(), BOOKING_ID);
    }

    @Test
    public void testUpdateBookingErrorMessage() {
        givenAllBookingApisReturnErrorMessages();
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        assertFalse(updateBookingApiCallWrapper.isSuccessful());
        assertNull(updateBookingApiCallWrapper.getThrowable());
        assertEquals(updateBookingApiCallWrapper.getMessage(), ERROR_MESSAGE);
    }

    @Test
    public void testBookingApiUndeclaredThrowableUpdateBooking() {
        givenAllBookingApisThrowsException(new UndeclaredThrowableException(new Exception(EXCEPTION_MESSAGE)));
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        andWrapTheException(updateBookingApiCallWrapper);
    }

    @Test
    public void testBookingApiInvalidParameterUpdateBooking() {
        givenAllBookingApisThrowsException(new InvalidParametersException(EXCEPTION_MESSAGE));
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        andWrapTheException(updateBookingApiCallWrapper);
    }

    @Test
    public void testBookingApiBadCredentialsUpdateBooking() {
        givenAllBookingApisThrowsException(new BadCredentialsException(EXCEPTION_MESSAGE));
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingApiCallWrapper = bookingApiManager.updateBooking(BOOKING_ID, updateBookingWrapperBuilder);
        thenShouldInvokeApiUpdateBooking();
        andWrapTheException(updateBookingApiCallWrapper);
    }

    private void setupGetBookingRequestAndResponse() throws BuilderException {
        getBookingWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING).request(BookingDTO.builder().id(BOOKING_ID).build());
        bookingDetail = new BookingDetailBuilder()
                .bookingBasicInfoBuilder(new com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder().id(BOOKING_ID))
                .userId(USER_ID)
                .currencyCode(CURRENCY_CODE)
                .allFeeContainers(Collections.singletonList(new FeeContainerBuilder().fees(Collections.singletonList(new FeeBuilder()))))
                .build(new Random());
    }

    private void setupSearchPrimeBookingSummaryRequestAndResponse() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.TRUE).membershipId(MEMBERSHIP_ID).build();
        searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS).request(searchBookingsRequest);
        searchBookingsPageResponse = new SearchBookingsPageResponse<>();
        List<BookingSummary> bookings = Collections.singletonList(new BookingSummaryBuilder()
                .bookingBasicInfo(new BookingBasicInfoBuilder().id(BOOKING_ID))
                .membershipId(MEMBERSHIP_ID).build(new Random()));
        searchBookingsPageResponse.setBookings(bookings);
    }

    private void setupSearchPrimeBookingSummaryRequestAndMultipleResponses() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        SearchBookingsRequest searchBookingsRequest = new SearchBookingsRequestBuilder().bookingSubscriptionPrime(Boolean.TRUE).membershipId(MEMBERSHIP_ID).build();
        searchBookingWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS).request(searchBookingsRequest);
        searchBookingsPageResponse = new SearchBookingsPageResponse<>();
        List<BookingSummary> bookings = Arrays.asList(
                new BookingSummaryBuilder().bookingBasicInfo(new BookingBasicInfoBuilder().id(BOOKING_ID))
                        .membershipId(MEMBERSHIP_ID).build(new Random()),
                new BookingSummaryBuilder().bookingBasicInfo(new BookingBasicInfoBuilder().id(BOOKING_ID2))
                        .membershipId(MEMBERSHIP_ID).build(new Random()));
        searchBookingsPageResponse.setBookings(bookings);
    }

    private void setupUpdateBookingRequestAndResponse() {
        updateBookingWrapperBuilder = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.UPDATE_BOOKING);
        updateBookingWrapperBuilder.request(new UpdateBookingRequestBuilder().build());
    }

    private void givenAllBookingApisSuccessfulResponse() {
        givenBookingSearchApiSuccessfulResponse();
        givenBookingApiSuccessfulResponse();
    }

    private void givenBookingSearchApiSuccessfulResponse() {
        given(bookingSearchApiService.searchBookings(isNull(), isNull(), any(Locale.class), eq(searchBookingWrapperBuilder.getRequest())))
                .willReturn(searchBookingsPageResponse);
    }

    private void givenBookingApiSuccessfulResponse() {
        given(bookingApiService.getBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID)))
                .willReturn(bookingDetail);
        given(bookingApiService.updateBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest())))
                .willReturn(bookingDetail);
    }

    private void givenAllBookingApisThrowsException(Exception e) {
        givenBookingSearchApiThrowsException(e);
        givenBookingApiThrowsException(e);
    }

    private void givenBookingSearchApiThrowsException(Exception e) {
        when(bookingSearchApiService.searchBookings(isNull(), isNull(), any(Locale.class), any(SearchBookingsRequest.class))).thenThrow(e);
    }

    private void givenBookingApiThrowsException(Exception e) {
        when(bookingApiService.getBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID))).thenThrow(e);
        when(bookingApiService.updateBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest()))).thenThrow(e);
    }

    private void givenAllBookingApisReturnErrorMessages() {
        givenBookingSearchApiReturnsErrorMessage();
        givenBookingApiReturnsErrorMessages();
    }

    private void givenBookingSearchApiReturnsErrorMessage() {
        when(bookingSearchApiService.searchBookings(isNull(), isNull(), any(Locale.class), eq(searchBookingWrapperBuilder.getRequest()))).thenReturn(searchBookingsPageResponse);
        searchBookingsPageResponse.setErrorMessage(ERROR_MESSAGE);
    }

    private void givenBookingApiReturnsErrorMessages() {
        when(bookingApiService.getBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID))).thenReturn(bookingDetail);
        when(bookingApiService.updateBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest()))).thenReturn(bookingDetail);
        bookingDetail.setErrorMessage(ERROR_MESSAGE);
    }

    private void givenBookingApiNullResponse() {
        when(bookingApiService.getBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID))).thenReturn(null);
        when(bookingApiService.updateBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest()))).thenReturn(null);
    }

    private void thenShouldInvokeApiGetBooking() {
        then(bookingApiService).should().getBooking(isNull(), isNull(), any(Locale.class), eq(getBookingWrapperBuilder.getRequest().getId()));
    }

    private void thenShouldNotInvokeApiGetBooking() {
        then(bookingApiService).should(never()).getBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID));
    }

    private void thenShouldInvokeApiSearchBookings() {
        then(bookingSearchApiService).should().searchBookings(isNull(), isNull(), any(Locale.class), eq(searchBookingWrapperBuilder.getRequest()));
    }

    private void thenShouldInvokeApiUpdateBooking() {
        then(bookingApiService).should().updateBooking(isNull(), isNull(), any(Locale.class), eq(BOOKING_ID), eq(updateBookingWrapperBuilder.getRequest()));
    }

    private void andWrapTheException(ApiCallWrapper apiCallWrapper) {
        assertFalse(apiCallWrapper.isSuccessful());
        assertEquals(apiCallWrapper.getThrowable().getClass(), BookingApiException.class);
        assertFalse(StringUtils.isEmpty(apiCallWrapper.getMessage()));
    }

}