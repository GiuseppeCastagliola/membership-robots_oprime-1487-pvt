package com.odigeo.membership.robots.manager;

import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCall.Endpoint;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.ExpirationConfiguration;
import com.odigeo.membership.robots.criteria.CollectionAttemptPredicateProvider;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTOBuilder;
import com.odigeo.membership.robots.dto.booking.CollectionAttemptDTO;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManager;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManager;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManagerTracking;
import com.odigeo.membership.robots.manager.membership.search.MembershipSearchModuleManager;
import com.odigeo.membership.robots.manager.offer.MembershipOfferModuleManager;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;

public class ExternalModuleManagerBeanTest {
    private static final String WEBSITE = "ES";
    private static final Integer TWELVE = 12;
    private static final String EUR = "EUR";
    private static final BigDecimal TEST_SUBSCRIPTION_PRICE = BigDecimal.TEN;
    private static final long MEMBERSHIP_ID = 123456L;
    private static final long MEMBERSHIP_ID_2 = 456789L;
    private static final BigDecimal BALANCE = BigDecimal.TEN;
    private static final LocalDate TODAY = LocalDate.now();
    private static final MembershipDTO MEMBERSHIP_DTO = MembershipDTO.builder().id(MEMBERSHIP_ID).balance(BALANCE).website(WEBSITE).build();
    private static final MembershipDTO MEMBERSHIP_DTO_2 = MembershipDTO.builder().id(MEMBERSHIP_ID_2).balance(BALANCE).website(WEBSITE).build();
    private static final MembershipOfferDTO.Builder COMMON_MEMBERSHIP_OFFER_BUILDER = MembershipOfferDTO.builder().website(WEBSITE).price(TEST_SUBSCRIPTION_PRICE).currencyCode(EUR).duration(TWELVE);
    private static final MembershipOfferDTO MEMBERSHIP_OFFER_DTO = COMMON_MEMBERSHIP_OFFER_BUILDER.build();
    private static final SearchMembershipsDTO SEARCH_MEMBERSHIPS_DTO = SearchMembershipsDTO.builder().fromExpirationDate(TODAY).toExpirationDate(TODAY).status("ACTIVATED").build();

    private static final long BOOKING_ID = 999L;

    private static final BookingDTOBuilder bookingCommonBuilder = BookingDTO.builder()
            .id(BOOKING_ID)
            .membershipId(MEMBERSHIP_ID);
    private static final BookingDTO BOOKING_DTO = bookingCommonBuilder.build();
    private static final ApiCallWrapper<BookingDTO, MembershipDTO> GET_PRIME_SUBSCRIPTION_BOOKING_FOR_MEMBERSHIP_SUCCESS = new ApiCallWrapperBuilder<BookingDTO, MembershipDTO>(Endpoint.GET_BOOKING)
            .response(BOOKING_DTO).request(MEMBERSHIP_DTO).result(ApiCall.Result.SUCCESS).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> CREATE_PENDING_SUCCESS = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.CREATE_PENDING_TO_COLLECT)
            .result(ApiCall.Result.SUCCESS).build();
    private static final String RECURRING_ID = "123";

    @Mock
    private MembershipOfferModuleManager membershipOfferModuleManager;
    @Mock
    private MembershipModuleManager membershipModuleManager;
    @Mock
    private MembershipSearchModuleManager membershipSearchModuleManager;
    @Mock
    private BookingApiManager bookingApiManager;
    @Mock
    private ExpirationConfiguration expirationConfiguration;
    @Mock
    private CollectionAttemptPredicateProvider collectionAttemptPredicateProvider;
    @Mock
    private MembershipModuleManagerTracking membershipModuleManagerTracking;

    private ExternalModuleManagerBean externalModuleManagerBean;

    @BeforeMethod
    public void setup() {
        openMocks(this);
        when(expirationConfiguration.getDaysInThePast()).thenReturn(1L);
        externalModuleManagerBean = new ExternalModuleManagerBean(membershipOfferModuleManager, membershipModuleManager,
                membershipSearchModuleManager, bookingApiManager, membershipModuleManagerTracking, collectionAttemptPredicateProvider);
    }

    @Test
    public void testCreatePendingToCollectFromPreviousMembership() {
        given(membershipModuleManager.createPendingToCollect(any())).willReturn(CREATE_PENDING_SUCCESS);
        ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper = buildSubscriptionOfferWrapper(MEMBERSHIP_OFFER_DTO, MEMBERSHIP_DTO, ApiCall.Result.SUCCESS).build();

        final ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingToCollectWrapper = externalModuleManagerBean.createPendingToCollectFromPreviousMembership(subscriptionOfferWrapper);

        then(membershipModuleManager).should().createPendingToCollect(eq(subscriptionOfferWrapper));
        assertEquals(createPendingToCollectWrapper, CREATE_PENDING_SUCCESS);
    }

    @Test
    public void testGetPrimeSubscriptionBookingForMembership() {
        given(bookingApiManager.getPrimeSubscriptionBookingForMembership(any(MembershipDTO.class))).willReturn(GET_PRIME_SUBSCRIPTION_BOOKING_FOR_MEMBERSHIP_SUCCESS);

        final ApiCallWrapper<BookingDTO, MembershipDTO> primeSubscriptionBooking = externalModuleManagerBean.getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO);

        then(bookingApiManager).should().getPrimeSubscriptionBookingForMembership(eq(MEMBERSHIP_DTO));
        assertEquals(primeSubscriptionBooking.getResponse(), BOOKING_DTO);
    }

    @Test
    public void testGetMembershipSubscriptionOffer() {
        ApiCallWrapper<MembershipOfferDTO, MembershipDTO> subscriptionOfferWrapper = buildSubscriptionOfferWrapper(MEMBERSHIP_OFFER_DTO, MEMBERSHIP_DTO, ApiCall.Result.SUCCESS).build();
        given(membershipOfferModuleManager.getOffer(any(MembershipDTO.class))).willReturn(subscriptionOfferWrapper);

        final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> getMembershipSubscriptionOfferWrapper = externalModuleManagerBean.getMembershipSubscriptionOffer(MEMBERSHIP_DTO);

        then(membershipOfferModuleManager).should().getOffer(eq(MEMBERSHIP_DTO));
        assertEquals(getMembershipSubscriptionOfferWrapper, subscriptionOfferWrapper);
    }

    @Test
    public void testSearchMemberships() {
        ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> responseWrapper = new ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO>(Endpoint.SEARCH_MEMBERSHIPS).build();
        given(membershipSearchModuleManager.searchMemberships(any(SearchMembershipsDTO.class))).willReturn(responseWrapper);

        final ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> searchMembershipsWrapper = externalModuleManagerBean.searchMemberships(SEARCH_MEMBERSHIPS_DTO);

        then(membershipSearchModuleManager).should().searchMemberships(eq(SEARCH_MEMBERSHIPS_DTO));
        assertEquals(searchMembershipsWrapper, responseWrapper);
    }

    @Test
    public void testExpireMembership() {
        ApiCallWrapper<MembershipDTO, MembershipDTO> responseWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.EXPIRE_MEMBERSHIP)
                .result(ApiCall.Result.SUCCESS).build();
        given(membershipModuleManager.expireMembership(any(MembershipDTO.class))).willReturn(responseWrapper);

        final ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembershipWrapper = externalModuleManagerBean.expireMembership(MEMBERSHIP_DTO);

        then(membershipModuleManager).should().expireMembership(eq(MEMBERSHIP_DTO));
        assertEquals(expireMembershipWrapper, responseWrapper);
    }

    @Test
    public void testEnableAutoRenewal() {
        ApiCallWrapper<MembershipDTO, MembershipDTO> responseWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.ENABLE_AUTO_RENEWAL)
                .result(ApiCall.Result.SUCCESS).build();
        given(membershipModuleManager.enableAutoRenewal(eq(MEMBERSHIP_DTO))).willReturn(responseWrapper);
        externalModuleManagerBean.enableMembershipAutoRenewal(MEMBERSHIP_DTO);
        then(membershipModuleManager).should().enableAutoRenewal(eq(MEMBERSHIP_DTO));
    }

    @Test
    public void testDiscardMembership() {
        ApiCallWrapper<MembershipDTO, MembershipDTO> responseWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.DISCARD_MEMBERSHIP)
                .result(ApiCall.Result.SUCCESS).build();
        given(membershipModuleManager.discardMembership(any(MembershipDTO.class))).willReturn(responseWrapper);

        final ApiCallWrapper<MembershipDTO, MembershipDTO> discardMembershipWrapper = externalModuleManagerBean.discardMembership(MEMBERSHIP_DTO);

        then(membershipModuleManager).should().discardMembership(eq(MEMBERSHIP_DTO));
        assertEquals(discardMembershipWrapper, responseWrapper);
    }

    @Test
    public void testRetrieveMembershipsByIds() {
        List<Long> membershipIds = new ArrayList<>();
        membershipIds.add(MEMBERSHIP_ID);
        membershipIds.add(MEMBERSHIP_ID_2);

        given(membershipSearchModuleManager.getMembership(MEMBERSHIP_ID)).willReturn(new ApiCallWrapperBuilder<MembershipDTO, Long>(Endpoint.GET_MEMBERSHIP).result(ApiCall.Result.SUCCESS).request(MEMBERSHIP_ID).response(MEMBERSHIP_DTO).build());
        given(membershipSearchModuleManager.getMembership(MEMBERSHIP_ID_2)).willReturn(new ApiCallWrapperBuilder<MembershipDTO, Long>(Endpoint.GET_MEMBERSHIP).result(ApiCall.Result.SUCCESS).request(MEMBERSHIP_ID_2).response(MEMBERSHIP_DTO_2).build());

        ApiCallWrapper<List<MembershipDTO>, List<Long>> memberships = externalModuleManagerBean.retrieveMembershipsByIds(membershipIds);

        assertEquals(memberships, new ApiCallWrapperBuilder<List<MembershipDTO>, List<Long>>(Endpoint.SEARCH_MEMBERSHIPS).request(membershipIds).response(Arrays.asList(MEMBERSHIP_DTO, MEMBERSHIP_DTO_2)).build());
    }

    @Test
    public void testSearchBookings() {
        ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest> requestWrapperBuilder = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(Endpoint.SEARCH_BOOKINGS).result(ApiCall.Result.SUCCESS);
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> responseWrapper = requestWrapperBuilder.build();
        given(bookingApiManager.searchBookings(any())).willReturn(responseWrapper);

        final ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingsWrapper = externalModuleManagerBean.searchBookings(requestWrapperBuilder);

        then(bookingApiManager).should().searchBookings(eq(requestWrapperBuilder));
        assertEquals(searchBookingsWrapper, responseWrapper);
    }

    @Test
    public void testUpdateBooking() {
        ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest> requestWrapperBuilder = new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(Endpoint.UPDATE_BOOKING).result(ApiCall.Result.SUCCESS);
        ApiCallWrapper<BookingDTO, UpdateBookingRequest> responseWrapper = requestWrapperBuilder.build();
        given(bookingApiManager.updateBooking(any(Long.class), any())).willReturn(responseWrapper);

        final ApiCallWrapper<BookingDTO, UpdateBookingRequest> updateBookingsWrapper = externalModuleManagerBean.updateBooking(BOOKING_ID, requestWrapperBuilder);

        then(bookingApiManager).should().updateBooking(eq(BOOKING_ID), eq(requestWrapperBuilder));
        assertEquals(updateBookingsWrapper, responseWrapper);
    }

    @Test
    public void testConsumeRemnantMembershipBalance() {
        ApiCallWrapper<MembershipDTO, MembershipDTO> responseWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE)
                .result(ApiCall.Result.SUCCESS).build();
        given(membershipModuleManager.consumeRemnantMembershipBalance(any(MembershipDTO.class))).willReturn(responseWrapper);

        final ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantMembershipBalanceWrapper = externalModuleManagerBean.consumeRemnantMembershipBalance(MEMBERSHIP_DTO);

        then(membershipModuleManager).should().consumeRemnantMembershipBalance(eq(MEMBERSHIP_DTO));
        assertEquals(consumeRemnantMembershipBalanceWrapper, responseWrapper);
    }

    @Test
    public void testActivateMembership() {
        ApiCallWrapper<MembershipDTO, MembershipDTO> responseWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.ACTIVATE_MEMBERSHIP)
                .result(ApiCall.Result.SUCCESS).build();
        given(membershipModuleManager.activateMembership(eq(MEMBERSHIP_DTO), eq(BOOKING_DTO))).willReturn(responseWrapper);
        ApiCallWrapper<MembershipDTO, MembershipDTO> callWrapper = externalModuleManagerBean.activateMembership(MEMBERSHIP_DTO, BOOKING_DTO);
        then(membershipModuleManager).should().activateMembership(eq(MEMBERSHIP_DTO), eq(BOOKING_DTO));
        assertEquals(callWrapper, responseWrapper);
    }

    @Test
    public void testGetBookingTracking() {
        ApiCallWrapper<BookingTrackingDTO, BookingDTO> responseWrapper = new ApiCallWrapperBuilder<BookingTrackingDTO, BookingDTO>(Endpoint.GET_BOOKING_TRACKING)
                .result(ApiCall.Result.SUCCESS).build();
        given(membershipModuleManagerTracking.getBookingTracking(eq(BOOKING_DTO))).willReturn(responseWrapper);
        ApiCallWrapper<BookingTrackingDTO, BookingDTO> callWrapper = externalModuleManagerBean.getBookingTracking(BOOKING_DTO);
        then(membershipModuleManagerTracking).should().getBookingTracking(eq(BOOKING_DTO));
        assertEquals(callWrapper, responseWrapper);
    }

    @Test
    public void testTrackBookingAndUpdateBalance() {
        ApiCallWrapper<BookingTrackingDTO, BookingTrackingDTO> responseWrapper = new ApiCallWrapperBuilder<BookingTrackingDTO, BookingTrackingDTO>(Endpoint.TRACK_BOOKING)
                .result(ApiCall.Result.SUCCESS).build();
        BookingTrackingDTO bookingTrackingDTO = BookingTrackingDTO.builder().build();
        given(membershipModuleManagerTracking.trackBookingAndUpdateBalance(any(BookingTrackingDTO.class))).willReturn(responseWrapper);
        ApiCallWrapper<BookingTrackingDTO, BookingTrackingDTO> callWrapper = externalModuleManagerBean.trackBookingAndUpdateBalance(bookingTrackingDTO);
        then(membershipModuleManagerTracking).should().trackBookingAndUpdateBalance(eq(bookingTrackingDTO));
        assertEquals(callWrapper, responseWrapper);
    }

    @Test
    public void testDeleteBookingTrackingAndUpdateBalance() {
        ApiCallWrapper<Void, BookingDTO> responseWrapper = new ApiCallWrapperBuilder<Void, BookingDTO>(Endpoint.DELETE_BOOKING_TRACKING)
                .result(ApiCall.Result.SUCCESS).build();
        given(membershipModuleManagerTracking.deleteBookingTrackingAndUpdateBalance(BOOKING_DTO)).willReturn(responseWrapper);
        ApiCallWrapper<Void, BookingDTO> callWrapper = externalModuleManagerBean.deleteBookingTrackingAndUpdateBalance(BOOKING_DTO);
        then(membershipModuleManagerTracking).should().deleteBookingTrackingAndUpdateBalance(eq(BOOKING_DTO));
        assertEquals(callWrapper, responseWrapper);
    }

    @Test
    public void testGetMembership() {
        ApiCallWrapper<MembershipDTO, Long> responseWrapper = new ApiCallWrapperBuilder<MembershipDTO, Long>(Endpoint.GET_MEMBERSHIP)
                .result(ApiCall.Result.SUCCESS).build();
        given(membershipSearchModuleManager.getMembership(eq(MEMBERSHIP_ID))).willReturn(responseWrapper);
        ApiCallWrapper<MembershipDTO, Long> callWrapper = externalModuleManagerBean.getMembership(MEMBERSHIP_ID);
        then(membershipSearchModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        assertEquals(callWrapper, responseWrapper);
    }

    @Test
    public void testSaveRecurringId() {
        ApiCallWrapper<MembershipDTO, MembershipDTO> responseWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE)
                .result(ApiCall.Result.SUCCESS).build();
        given(membershipModuleManager.saveRecurringId(any(MembershipDTO.class))).willReturn(responseWrapper);
        given(collectionAttemptPredicateProvider.hasRecurringId()).willReturn(x -> true);
        given(collectionAttemptPredicateProvider.isLastMovementStatusAndActionValid()).willReturn(x -> true);
        BookingDTO bookingDTO = BookingDTO.builder()
                .collectionAttempts(Collections.singletonList(CollectionAttemptDTO.builder()
                        .recurringId(RECURRING_ID)
                        .build()))
                .build();
        ApiCallWrapper<MembershipDTO, MembershipDTO> wrapper = externalModuleManagerBean.saveRecurringId(MEMBERSHIP_DTO, bookingDTO);
        then(membershipModuleManager).should().saveRecurringId(any(MembershipDTO.class));
        assertEquals(wrapper, responseWrapper);
    }


    private ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO> buildSubscriptionOfferWrapper(MembershipOfferDTO response, MembershipDTO request, ApiCall.Result result) {
        return new ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO>(ApiCall.Endpoint.GET_OFFER)
                .result(result)
                .response(response)
                .request(request);
    }

}