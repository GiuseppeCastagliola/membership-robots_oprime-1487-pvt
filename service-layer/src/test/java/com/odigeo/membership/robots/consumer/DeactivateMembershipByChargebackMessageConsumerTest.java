package com.odigeo.membership.robots.consumer;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManager;
import com.odigeo.membership.robots.manager.bookingapi.product.ProductMembershipPrimeBookingItemType;
import com.odigeo.membership.robots.manager.membership.MembershipModuleManager;
import com.odigeo.membership.robots.manager.membership.search.MembershipSearchModuleManager;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.MockitoAnnotations.openMocks;

public class DeactivateMembershipByChargebackMessageConsumerTest {

    private static final String FLIGHT_PRODUCT = "FLIGHT";
    private static final Long MEMBERSHIP_ID = 12345l;
    private static final String MEMBERSHIP_STATUS = "ACTIVATED";
    private static final Long BOOKING_ID = 12345l;
    private static final String ACTION = "CHARGEBACK";
    private static final String STATUS = "CHARGEBACKED";
    private static final String SUCCESS_UPDATE_MEMBERSHIP = DeactivateMembershipByChargebackMessageConsumer.SUCCESS_UPDATE_MEMBERSHIP;
    private static final String FAIL_UPDATE_MEMBERSHIP = DeactivateMembershipByChargebackMessageConsumer.FAIL_UPDATE_MEMBERSHIP;
    private static final Long EXECUTION_TIME_MILLIS = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    private static final Long STORAGE_TIME_MILLIS = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    private static final MembershipDTO MEMBERSHIP_DTO = MembershipDTO.builder()
            .id(MEMBERSHIP_ID)
            .status(MEMBERSHIP_STATUS)
            .build();
    private static final BookingDTO BOOKING_DTO_NOT_PRODUCTS = BookingDTO.builder()
            .id(BOOKING_ID)
            .productTypes(Collections.EMPTY_LIST)
            .build();
    private static final BookingDTO BOOKING_DTO_NOT_PRIME = BookingDTO.builder()
            .id(BOOKING_ID)
            .productTypes(Collections.singletonList(FLIGHT_PRODUCT))
            .build();
    private static final BookingDTO BOOKING_DTO_NOT_STANDALONE = BookingDTO.builder()
            .id(BOOKING_ID)
            .productTypes(Arrays.asList(FLIGHT_PRODUCT, ProductMembershipPrimeBookingItemType.MEMBERSHIP_RENEWAL.name()))
            .build();
    private static final BookingDTO BOOKING_DTO_PRIME_STANDALONE = BookingDTO.builder()
            .id(BOOKING_ID)
            .productTypes(Collections.singletonList(ProductMembershipPrimeBookingItemType.MEMBERSHIP_RENEWAL.name()))
            .membershipId(MEMBERSHIP_ID)
            .build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_FAILED = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
            .result(ApiCall.Result.FAIL)
            .build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_FAILED_WITH_EXCEPTION = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
            .result(ApiCall.Result.FAIL)
            .throwable(new RuntimeException())
            .build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_WITHOUT_PRODUCT = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
            .result(ApiCall.Result.SUCCESS)
            .response(BOOKING_DTO_NOT_PRODUCTS)
            .build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_WITH_PRODUCT_NO_PRIME = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
            .result(ApiCall.Result.SUCCESS)
            .response(BOOKING_DTO_NOT_PRIME)
            .build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_WITH_PRODUCT_NO_STANDALONE = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
            .result(ApiCall.Result.SUCCESS)
            .response(BOOKING_DTO_NOT_STANDALONE)
            .build();
    private static final ApiCallWrapper<BookingDTO, BookingDTO> GET_BOOKING_SUCCESS_PRIME_STANDALONE = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING)
            .result(ApiCall.Result.SUCCESS)
            .response(BOOKING_DTO_PRIME_STANDALONE)
            .build();
    private static final ApiCallWrapper<MembershipDTO, Long> GET_MEMBERSHIP_FAILED = new ApiCallWrapperBuilder<MembershipDTO, Long>(ApiCall.Endpoint.GET_MEMBERSHIP)
            .result(ApiCall.Result.FAIL)
            .build();
    private static final ApiCallWrapper<MembershipDTO, Long> GET_MEMBERSHIP_FAILED_WITH_EXCEPTION = new ApiCallWrapperBuilder<MembershipDTO, Long>(ApiCall.Endpoint.GET_MEMBERSHIP)
            .result(ApiCall.Result.FAIL)
            .throwable(new RuntimeException())
            .build();
    private static final ApiCallWrapper<MembershipDTO, Long> GET_MEMBERSHIP_SUCCESS = new ApiCallWrapperBuilder<MembershipDTO, Long>(ApiCall.Endpoint.GET_MEMBERSHIP)
            .result(ApiCall.Result.SUCCESS)
            .response(MEMBERSHIP_DTO)
            .build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> DEACTIVATE_MEMBERSHIP_FAILED = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.DEACTIVATE_MEMBERSHIP)
            .result(ApiCall.Result.FAIL)
            .build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> DEACTIVATE_MEMBERSHIP_FAILED_WITH_EXCEPTION = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.DEACTIVATE_MEMBERSHIP)
            .result(ApiCall.Result.FAIL)
            .throwable(new RuntimeException())
            .build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> DEACTIVATE_MEMBERSHIP_SUCCESS = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.DEACTIVATE_MEMBERSHIP)
            .result(ApiCall.Result.SUCCESS)
            .build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> CONSUME_REMNANT_MEMBERSHIP_BALANCE_SUCCESS = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE)
            .result(ApiCall.Result.SUCCESS)
            .build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> CONSUME_REMNANT_MEMBERSHIP_BALANCE_FAILED = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE)
            .result(ApiCall.Result.FAIL)
            .build();

    @Mock
    MembershipSearchModuleManager membershipSearchModuleManager;
    @Mock
    BookingApiManager bookingApiManager;
    @Mock
    MembershipModuleManager membershipModuleManager;
    @Mock
    MembershipRobotsMetrics membershipRobotsMetrics;

    DeactivateMembershipByChargebackMessageConsumer deactivateMembershipByChargebackMessageConsumer;
    CollectionSummaryChargebackNotifications collectionSummaryChargebackNotifications;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        deactivateMembershipByChargebackMessageConsumer = new DeactivateMembershipByChargebackMessageConsumer(membershipSearchModuleManager, bookingApiManager, membershipModuleManager, membershipRobotsMetrics);
        collectionSummaryChargebackNotifications = CollectionSummaryChargebackNotifications.builder()
                .bookingId(BOOKING_ID)
                .action(ACTION)
                .status(STATUS)
                .executionTimeInMillis(EXECUTION_TIME_MILLIS)
                .storageTimeInMillis(STORAGE_TIME_MILLIS)
                .build();
    }

    @Test
    public void getBookingFailedWithoutException() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_FAILED);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should(never()).getMembership(anyLong());
        then(membershipModuleManager).should(never()).deactivateMembership(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any(), any(), any());
    }

    @Test(expectedExceptions = RetryableException.class)
    public void getBookingFailedWithException() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_FAILED_WITH_EXCEPTION);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should(never()).getMembership(anyLong());
        then(membershipModuleManager).should(never()).deactivateMembership(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any(), any(), any());
    }

    @Test
    public void getBookingWithoutProducts() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_WITHOUT_PRODUCT);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should(never()).getMembership(anyLong());
        then(membershipModuleManager).should(never()).deactivateMembership(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any(), any(), any());
    }

    @Test
    public void getBookingWithProductsNoPrime() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_WITH_PRODUCT_NO_PRIME);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should(never()).getMembership(anyLong());
        then(membershipModuleManager).should(never()).deactivateMembership(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any(), any(), any());
    }

    @Test
    public void getBookingWithProductsNoStandalone() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_WITH_PRODUCT_NO_STANDALONE);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should(never()).getMembership(anyLong());
        then(membershipModuleManager).should(never()).deactivateMembership(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any());
        then(membershipRobotsMetrics).should(never()).incrementCounter(any(), any(), any());
    }

    @Test
    public void getMembershipFailed() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_PRIME_STANDALONE);
        given(membershipSearchModuleManager.getMembership(eq(MEMBERSHIP_ID))).willReturn(GET_MEMBERSHIP_FAILED);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(membershipModuleManager).should(never()).deactivateMembership(any());
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_PROCESSED_PRIME_STANDALONE_CHARGEBACK));
        then(membershipRobotsMetrics).should(never()).incrementCounter(any(), any(), any());
    }

    @Test(expectedExceptions = RetryableException.class)
    public void getMembershipFailedWithException() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_PRIME_STANDALONE);
        given(membershipSearchModuleManager.getMembership(eq(MEMBERSHIP_ID))).willReturn(GET_MEMBERSHIP_FAILED_WITH_EXCEPTION);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(membershipModuleManager).should(never()).deactivateMembership(any());
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_PROCESSED_PRIME_STANDALONE_CHARGEBACK));
        then(membershipRobotsMetrics).should(never()).incrementCounter(any(), any(), any());
    }

    @Test
    public void deactivateMembershipFailed() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_PRIME_STANDALONE);
        given(membershipSearchModuleManager.getMembership(eq(MEMBERSHIP_ID))).willReturn(GET_MEMBERSHIP_SUCCESS);
        given(membershipModuleManager.deactivateMembership(eq(MEMBERSHIP_DTO))).willReturn(DEACTIVATE_MEMBERSHIP_FAILED);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(membershipModuleManager).should().deactivateMembership(eq(MEMBERSHIP_DTO));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_PROCESSED_PRIME_STANDALONE_CHARGEBACK));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK), eq(MetricsName.TagName.STATUS), eq(FAIL_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK), eq(MetricsName.TagName.STATUS), eq(SUCCESS_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE), eq(MetricsName.TagName.STATUS), eq(SUCCESS_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE), eq(MetricsName.TagName.STATUS), eq(FAIL_UPDATE_MEMBERSHIP));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void deactivateMembershipFailedWithException() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_PRIME_STANDALONE);
        given(membershipSearchModuleManager.getMembership(eq(MEMBERSHIP_ID))).willReturn(GET_MEMBERSHIP_SUCCESS);
        given(membershipModuleManager.deactivateMembership(eq(MEMBERSHIP_DTO))).willReturn(DEACTIVATE_MEMBERSHIP_FAILED_WITH_EXCEPTION);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(membershipModuleManager).should().deactivateMembership(eq(MEMBERSHIP_DTO));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_PROCESSED_PRIME_STANDALONE_CHARGEBACK));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK), eq(MetricsName.TagName.STATUS), eq(FAIL_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK), eq(MetricsName.TagName.STATUS), eq(SUCCESS_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE), eq(MetricsName.TagName.STATUS), eq(SUCCESS_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE), eq(MetricsName.TagName.STATUS), eq(FAIL_UPDATE_MEMBERSHIP));
    }

    @Test
    public void deactivateMembershipSuccessConsumeRemnantBalanceSuccess() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_PRIME_STANDALONE);
        given(membershipSearchModuleManager.getMembership(eq(MEMBERSHIP_ID))).willReturn(GET_MEMBERSHIP_SUCCESS);
        given(membershipModuleManager.deactivateMembership(eq(MEMBERSHIP_DTO))).willReturn(DEACTIVATE_MEMBERSHIP_SUCCESS);
        given(membershipModuleManager.consumeRemnantMembershipBalance(eq(DEACTIVATE_MEMBERSHIP_SUCCESS.getResponse()))).willReturn(CONSUME_REMNANT_MEMBERSHIP_BALANCE_SUCCESS);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(membershipModuleManager).should().deactivateMembership(eq(MEMBERSHIP_DTO));
        then(membershipModuleManager).should().consumeRemnantMembershipBalance(eq(DEACTIVATE_MEMBERSHIP_SUCCESS.getResponse()));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_PROCESSED_PRIME_STANDALONE_CHARGEBACK));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK), eq(MetricsName.TagName.STATUS), eq(SUCCESS_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK), eq(MetricsName.TagName.STATUS), eq(FAIL_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE), eq(MetricsName.TagName.STATUS), eq(SUCCESS_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE), eq(MetricsName.TagName.STATUS), eq(FAIL_UPDATE_MEMBERSHIP));
    }

    @Test
    public void deactivateMembershipSuccessConsumeRemnantBalanceFailed() {
        given(bookingApiManager.getBooking(any())).willReturn(GET_BOOKING_SUCCESS_PRIME_STANDALONE);
        given(membershipSearchModuleManager.getMembership(eq(MEMBERSHIP_ID))).willReturn(GET_MEMBERSHIP_SUCCESS);
        given(membershipModuleManager.deactivateMembership(eq(MEMBERSHIP_DTO))).willReturn(DEACTIVATE_MEMBERSHIP_SUCCESS);
        given(membershipModuleManager.consumeRemnantMembershipBalance(eq(DEACTIVATE_MEMBERSHIP_SUCCESS.getResponse()))).willReturn(CONSUME_REMNANT_MEMBERSHIP_BALANCE_FAILED);
        deactivateMembershipByChargebackMessageConsumer.deactivateMembershipByChargeback(collectionSummaryChargebackNotifications);
        then(bookingApiManager).should().getBooking(any());
        then(membershipSearchModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(membershipModuleManager).should().deactivateMembership(eq(MEMBERSHIP_DTO));
        then(membershipModuleManager).should().consumeRemnantMembershipBalance(eq(DEACTIVATE_MEMBERSHIP_SUCCESS.getResponse()));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_PROCESSED_PRIME_STANDALONE_CHARGEBACK));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK), eq(MetricsName.TagName.STATUS), eq(SUCCESS_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_MEMBERSHIP_DEACTIVATED_CHARGEBACK), eq(MetricsName.TagName.STATUS), eq(FAIL_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should(never()).incrementCounter(eq(MetricsName.TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE), eq(MetricsName.TagName.STATUS), eq(SUCCESS_UPDATE_MEMBERSHIP));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.TOTAL_CONSUME_REMNANT_MEMBERSHIP_BALANCE), eq(MetricsName.TagName.STATUS), eq(FAIL_UPDATE_MEMBERSHIP));
    }
}
