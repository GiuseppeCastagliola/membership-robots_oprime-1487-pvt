package com.odigeo.membership.robots.consumer;

import com.odigeo.commons.messaging.BasicMessage;
import com.odigeo.membership.robots.configuration.ConsumerRetriesConfig;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.then;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.testng.Assert.assertTrue;

public class BookingUpdatesMessageProcessorTest {
    private static final String MESSAGE_KEY = "1";
    @Mock
    Processor bookingProcessor;
    @Mock
    private MembershipRobotsMetrics membershipRobotsMetric;
    private ConsumerRetriesConfig retriesConfig;
    @Spy
    private BlockingQueue<Long> bookingIdUpdated = new LinkedBlockingQueue<>(10);


    private BookingIdQueueConsumer bookingIdQueueConsumer;
    private BookingUpdatesMessageProcessor bookingUpdatesMessageProcessor;
    private BasicMessage message;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        retriesConfig = new ConsumerRetriesConfig().defaultValues();
        bookingIdQueueConsumer = Mockito.spy(new BookingIdQueueConsumer(bookingIdUpdated, bookingProcessor, membershipRobotsMetric, retriesConfig));
        bookingUpdatesMessageProcessor = new BookingUpdatesMessageProcessor(bookingIdUpdated, bookingIdQueueConsumer);
        message = new BasicMessage();
        message.setKey(MESSAGE_KEY);
    }

    @Test
    public void testOnMessage() throws InterruptedException {
        bookingUpdatesMessageProcessor.onMessage(message);
        then(bookingIdUpdated).should().put(eq(Long.parseLong(MESSAGE_KEY)));
    }

    @Test
    public void testOnMessageTwice() throws InterruptedException {
        bookingUpdatesMessageProcessor.onMessage(message);
        bookingUpdatesMessageProcessor.onMessage(message);
        then(bookingIdUpdated).should(times(2)).put(eq(Long.parseLong(MESSAGE_KEY)));
    }

    @AfterMethod
    private void stopAndCheckQueueConsumerStop() throws InterruptedException {
        bookingUpdatesMessageProcessor.stop();
        then(bookingIdQueueConsumer).should().stop();
        assertTrue(bookingIdUpdated.isEmpty());
    }
}