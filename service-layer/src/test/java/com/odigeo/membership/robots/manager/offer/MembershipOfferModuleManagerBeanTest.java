package com.odigeo.membership.robots.manager.offer;

import com.odigeo.membership.offer.api.MembershipOfferService;
import com.odigeo.membership.offer.api.exception.InvalidParametersException;
import com.odigeo.membership.offer.api.exception.MembershipOfferServiceException;
import com.odigeo.membership.offer.api.request.MembershipRenewalOfferRequest;
import com.odigeo.membership.offer.api.response.MembershipSubscriptionOffer;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.mapper.request.MembershipOfferRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipOfferResponseMapper;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;
import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipOfferModuleManagerBeanTest {
    private static final String WEBSITE = "ES";
    private static final long TEST_ID = 1L;
    private static final MembershipSubscriptionOffer MEMBERSHIP_SUBSCRIPTION_OFFER = MembershipSubscriptionOffer.builder()
            .setPrice(BigDecimal.TEN).setOfferId(String.valueOf(TEST_ID)).setWebsite(WEBSITE).setCurrencyCode("EUR")
            .build();
    private static final String INVALID_MSG = "INVALID";
    private static final String NULL_VALUE_RETURNED_MSG = "Invalid MembershipSubscriptionOffer, null value returned.";
    private static final InvalidParametersException INVALID_PARAMETERS_EXCEPTION = new InvalidParametersException(INVALID_MSG);
    private static final MembershipOfferServiceException OFFER_SERVICE_EXCEPTION = new MembershipOfferServiceException(Response.Status.FORBIDDEN, INVALID_MSG, new Exception());
    private static final UndeclaredThrowableException UNDECLARED_THROWABLE_EXCEPTION = new UndeclaredThrowableException(new Exception());
    @Mock
    private MembershipOfferService membershipOfferService;

    private MembershipOfferModuleManagerBean membershipOfferModuleManagerBean;

    @DataProvider(name = "exception-provider")
    public Object[][] possibleExceptions() {
        return new Object[][]{{INVALID_PARAMETERS_EXCEPTION}, {OFFER_SERVICE_EXCEPTION}, {UNDECLARED_THROWABLE_EXCEPTION}};
    }

    @DataProvider(name = "error-responses")
    public Object[][] possibleErrorResponses() {
        return new Object[][]{{null},
                {MembershipSubscriptionOffer.builder()
                        .setPrice(null).setCurrencyCode(null).setOfferId(String.valueOf(TEST_ID)).setWebsite(WEBSITE).build()},
                {MembershipSubscriptionOffer.builder()
                        .setPrice(BigDecimal.TEN).setCurrencyCode(null).setOfferId(String.valueOf(TEST_ID)).setWebsite(WEBSITE).build()},
                {MembershipSubscriptionOffer.builder()
                        .setPrice(null).setCurrencyCode("EUR").setOfferId(String.valueOf(TEST_ID)).setWebsite(WEBSITE).build()}};
    }

    @BeforeMethod
    public void setup() {
        openMocks(this);
        membershipOfferModuleManagerBean = new MembershipOfferModuleManagerBean(membershipOfferService, Mappers.getMapper(MembershipOfferRequestMapper.class), Mappers.getMapper(MembershipOfferResponseMapper.class));
    }

    @Test
    public void getOfferTest() throws InvalidParametersException {
        MembershipDTO membershipDTO = MembershipDTO.builder().website(WEBSITE).id(TEST_ID).build();
        given(membershipOfferService.createMembershipRenewalOffer(any(MembershipRenewalOfferRequest.class))).willReturn(MEMBERSHIP_SUBSCRIPTION_OFFER);
        final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> offer = membershipOfferModuleManagerBean.getOffer(membershipDTO);
        assertEquals(offer.getResponse().getPrice(), MEMBERSHIP_SUBSCRIPTION_OFFER.getPrice());
        assertTrue(offer.isSuccessful());
        assertNull(offer.getThrowable());
    }

    @Test(dataProvider = "exception-provider")
    public void getOfferThrowsExceptionTest(Exception exception) throws InvalidParametersException {
        MembershipDTO membershipDTO = MembershipDTO.builder().website(WEBSITE).id(TEST_ID).build();
        given(membershipOfferService.createMembershipRenewalOffer(any(MembershipRenewalOfferRequest.class))).willThrow(exception);
        final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> offer = membershipOfferModuleManagerBean.getOffer(membershipDTO);
        assertNull(offer.getResponse());
        assertFalse(offer.isSuccessful());
        assertEquals(offer.getThrowable(), exception);
        assertEquals(offer.getMessage(), exception.getMessage());
    }

    @Test(dataProvider = "error-responses")
    public void getOfferReturnsNullValuesTest(MembershipSubscriptionOffer subscriptionOffer) throws InvalidParametersException {
        MembershipDTO membershipDTO = MembershipDTO.builder().website(WEBSITE).id(TEST_ID).build();
        given(membershipOfferService.createMembershipRenewalOffer(any(MembershipRenewalOfferRequest.class))).willReturn(subscriptionOffer);
        final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> offer = membershipOfferModuleManagerBean.getOffer(membershipDTO);
        assertFalse(offer.isSuccessful());
        assertNull(offer.getThrowable());
        assertTrue(offer.getMessage().startsWith(NULL_VALUE_RETURNED_MSG));
    }
}