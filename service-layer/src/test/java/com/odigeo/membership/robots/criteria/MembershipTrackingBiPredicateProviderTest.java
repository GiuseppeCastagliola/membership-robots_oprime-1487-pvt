package com.odigeo.membership.robots.criteria;

import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipTrackingBiPredicateProviderTest extends PredicateHelper<MembershipDTO> {
    private MembershipDTO membershipDTO;
    @Mock
    private BookingTrackingDTO bookingTrackingDTO;
    @Mock
    private BookingDTO bookingDTO;
    @Mock
    private MembershipPredicateProvider membershipPredicateProvider;
    @Mock
    private BookingTrackingPredicateProvider bookingTrackingPredicateProvider;
    @InjectMocks
    private MembershipTrackingBiPredicateProvider membershipTrackingBiPredicateProvider;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        membershipDTO = MembershipDTO.builder().id(1L).build();
    }

    @Test
    public void testIsActivatedAndBookingFeesAreUnbalancedAndNotTracked() {
        given(membershipPredicateProvider.isActivated()).willReturn(truePredicate());
        given(membershipPredicateProvider.areFeesUnbalanced(any())).willReturn(truePredicate());
        assertTrue(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(bookingDTO).test(membershipDTO, null));
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(bookingDTO).test(membershipDTO, null));
    }

    @Test
    public void testIsActivatedAndBookingFeesAreBalancedAndNotTracked() {
        given(membershipPredicateProvider.isActivated()).willReturn(truePredicate());
        given(membershipPredicateProvider.areFeesUnbalanced(any())).willReturn(falsePredicate());
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(bookingDTO).test(membershipDTO, null));
        assertTrue(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(bookingDTO).test(membershipDTO, null));
    }

    @Test
    public void testIsActivatedAndBookingFeesAreUnbalancedButTracked() {
        given(membershipPredicateProvider.isActivated()).willReturn(truePredicate());
        given(membershipPredicateProvider.areFeesUnbalanced(any())).willReturn(truePredicate());
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(bookingDTO).test(membershipDTO, bookingTrackingDTO));
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(bookingDTO).test(membershipDTO, bookingTrackingDTO));
    }

    @Test
    public void testIsNotActivatedAndBookingFeesAreUnbalancedAndNotTracked() {
        given(membershipPredicateProvider.isActivated()).willReturn(falsePredicate());
        given(membershipPredicateProvider.areFeesUnbalanced(any())).willReturn(truePredicate());
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreUnbalancedAndNotTracked(bookingDTO).test(membershipDTO, null));
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreBalancedAndNotTracked(bookingDTO).test(membershipDTO, null));
    }

    @Test
    public void testIsActivatedAndBookingIsTracked() {
        given(membershipPredicateProvider.isActivated()).willReturn(truePredicate());
        assertTrue(membershipTrackingBiPredicateProvider.isActivatedAndBookingIsTracked().test(membershipDTO, bookingTrackingDTO));
    }

    @Test
    public void testIsNotActivatedAndBookingIsTracked() {
        given(membershipPredicateProvider.isActivated()).willReturn(falsePredicate());
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingIsTracked().test(membershipDTO, bookingTrackingDTO));
    }

    @Test
    public void testIsActivatedAndBookingIsNotTracked() {
        given(membershipPredicateProvider.isActivated()).willReturn(truePredicate());
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingIsTracked().test(membershipDTO, null));
    }

    @Test
    public void testIsActivatedAndBookingFeesAreTrackedAndChanged() {
        given(membershipPredicateProvider.isActivated()).willReturn(falsePredicate());
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(bookingDTO).test(membershipDTO, bookingTrackingDTO));
        given(bookingTrackingPredicateProvider.isAlreadyTrackedAndFeesAreChanged(any(BookingDTO.class))).willReturn(bookDto -> false, bookDto -> true);
        given(membershipPredicateProvider.isActivated()).willReturn(truePredicate());
        assertFalse(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(bookingDTO).test(membershipDTO, bookingTrackingDTO));
        assertTrue(membershipTrackingBiPredicateProvider.isActivatedAndBookingFeesAreTrackedAndChanged(bookingDTO).test(membershipDTO, bookingTrackingDTO));
    }
}
