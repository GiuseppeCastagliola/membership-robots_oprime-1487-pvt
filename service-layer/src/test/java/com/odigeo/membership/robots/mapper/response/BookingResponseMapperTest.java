package com.odigeo.membership.robots.mapper.response;

import com.odigeo.bookingapi.mock.v12.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v12.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v12.response.BuilderException;
import com.odigeo.bookingapi.mock.v12.response.CollectionAttemptBuilder;
import com.odigeo.bookingapi.mock.v12.response.CollectionSummaryBuilder;
import com.odigeo.bookingapi.mock.v12.response.FeeBuilder;
import com.odigeo.bookingapi.mock.v12.response.FeeContainerBuilder;
import com.odigeo.bookingapi.mock.v12.response.MembershipSubscriptionBookingBuilder;
import com.odigeo.bookingapi.mock.v12.response.MovementBuilder;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingSummaryBuilder;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.manager.membership.MembershipFeeSubCodes;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class BookingResponseMapperTest {

    private static final BookingResponseMapper BOOKING_RESPONSE_MAPPER = Mappers.getMapper(BookingResponseMapper.class);
    private static final Long BOOKING_ID = 1L;
    private static final String RECURRING_ID = "REC";
    private static final String DIRECTY = "DIRECTY";
    private static final String PAID = "PAID";
    private static final Calendar CALENDAR = Calendar.getInstance();
    private static final ZoneId UTC = ZoneId.of("UTC");
    private static final String FEE_CONTAINER_TYPE_TEST = "TEST";
    private static final BigDecimal EXPECTED_TOTAL_AVOID_FEE = BigDecimal.valueOf(21);
    private static final BigDecimal EXPECTED_TOTAL_COST_FEE = BigDecimal.TEN;
    private static final BigDecimal EXPECTED_TOTAL_PERKS = BigDecimal.ONE;
    private Random random = new Random();
    private BookingSummary bookingSummary;
    private BookingDetail bookingDetail;

    @BeforeMethod
    public void setUp() throws BuilderException, com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        List<CollectionAttemptBuilder> collectionAttemptBuilders = Collections.singletonList(
                new CollectionAttemptBuilder().recurringId(RECURRING_ID).validMovements(
                        Collections.singletonList(new MovementBuilder().status(PAID).action(DIRECTY)))
        );
        List<FeeContainerBuilder> feeContainerBuilders = getFeeContainerBuilders();
        bookingDetail = new BookingDetailBuilder().bookingBasicInfoBuilder(new BookingBasicInfoBuilder().id(BOOKING_ID).creationDate(CALENDAR))
                .collectionSummary(new CollectionSummaryBuilder().attempts(collectionAttemptBuilders))
                .allFeeContainers(feeContainerBuilders)
                .build(random);
        bookingSummary = new BookingSummaryBuilder().membershipId(1L)
                .bookingBasicInfo(new com.odigeo.bookingsearchapi.mock.v1.response.BookingBasicInfoBuilder().id(BOOKING_ID))
                .build(random);
    }

    private List<FeeContainerBuilder> getFeeContainerBuilders() {
        List<FeeBuilder> feeBuildersAvoid = Arrays.asList(new FeeBuilder()
                        .feeLabel(MembershipFeeSubCodes.AVOID_FEES.name())
                        .amount(BigDecimal.TEN),
                new FeeBuilder()
                        .feeLabel(MembershipFeeSubCodes.AVOID_FEES.name())
                        .subCode(MembershipFeeSubCodes.AVOID_FEES.getFeeCode())
                        .amount(BigDecimal.TEN));
        List<FeeBuilder> feeBuildersAvoidAndCostAndPerks = Arrays.asList(new FeeBuilder()
                        .subCode(MembershipFeeSubCodes.COST_FEE.getFeeCode())
                        .amount(BigDecimal.TEN),
                new FeeBuilder()
                        .feeLabel(MembershipFeeSubCodes.PERKS.name())
                        .subCode(MembershipFeeSubCodes.PERKS.getFeeCode())
                        .amount(BigDecimal.ONE),
                new FeeBuilder()
                        .feeLabel(MembershipFeeSubCodes.AVOID_FEES.name())
                        .subCode(MembershipFeeSubCodes.AVOID_FEES.getFeeCode())
                        .amount(BigDecimal.ONE));
        return Arrays.asList(
                new FeeContainerBuilder().feeContainerType(FEE_CONTAINER_TYPE_TEST).fees(feeBuildersAvoid),
                new FeeContainerBuilder().feeContainerType(FEE_CONTAINER_TYPE_TEST).fees(feeBuildersAvoidAndCostAndPerks));
    }

    @Test
    public void testBookingDetailResponseToDto() {
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(bookingDetail);
        assertEquals(bookingDTO.getId(), bookingDetail.getBookingBasicInfo().getId());
        assertEquals(bookingDTO.getCurrencyCode(), bookingDetail.getCurrencyCode());
        assertNull(bookingDTO.getMembershipId());
        assertEquals(bookingDTO.getFeeContainers().size(), bookingDetail.getAllFeeContainers().size());
        assertEquals(Long.valueOf(bookingDTO.getUserId()), bookingDetail.getUserId());
        assertEquals(bookingDTO.getCollectionAttempts().get(0).getRecurringId(), RECURRING_ID);
        assertEquals(bookingDTO.getCollectionAttempts().get(0).getLastMovement().getStatus(), PAID);
        assertEquals(bookingDTO.getBookingDate(), LocalDateTime.ofInstant(CALENDAR.toInstant(), UTC));

    }

    @Test
    public void testBookingDetailResponseWithSubscriptionToDto() throws BuilderException {
        bookingDetail.setBookingProducts(Collections.singletonList(new MembershipSubscriptionBookingBuilder().totalPrice(BigDecimal.ONE).build(random)));
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(bookingDetail);
        assertEquals(bookingDTO.getId(), bookingDetail.getBookingBasicInfo().getId());
        assertEquals(bookingDTO.getCurrencyCode(), bookingDetail.getCurrencyCode());
        assertNull(bookingDTO.getMembershipId());
        assertEquals(bookingDTO.getFeeContainers().size(), bookingDetail.getAllFeeContainers().size());
        assertEquals(Long.valueOf(bookingDTO.getUserId()), bookingDetail.getUserId());
        assertEquals(bookingDTO.getCollectionAttempts().get(0).getRecurringId(), RECURRING_ID);
        assertEquals(bookingDTO.getCollectionAttempts().get(0).getLastMovement().getStatus(), PAID);
        assertEquals(bookingDTO.getBookingProducts().get(0).getPrice(), BigDecimal.ONE);
        assertEquals(bookingDTO.getBookingProducts().get(0).getProductType(), "MEMBERSHIP_SUBSCRIPTION");
    }

    @Test
    public void testBookingDetailResponseNullUserIdToDto() {
        bookingDetail.setUserId(null);
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(bookingDetail);
        assertEquals(bookingDTO.getId(), bookingDetail.getBookingBasicInfo().getId());
        assertEquals(bookingDTO.getCurrencyCode(), bookingDetail.getCurrencyCode());
        assertNull(bookingDTO.getMembershipId());
        assertEquals(bookingDTO.getUserId(), 0L);
        assertEquals(bookingDTO.getFeeContainers().size(), bookingDetail.getAllFeeContainers().size());
    }

    @Test
    public void testBookingDetailResponseNullToDto() {
        assertNull(BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(null));
    }

    @Test
    public void testBookingSummaryResponseToDto() {
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingSummaryResponseToDto(bookingSummary);
        assertEquals(bookingDTO.getMembershipId(), bookingSummary.getMembershipId());
        assertEquals(bookingDTO.getId(), bookingSummary.getBookingBasicInfo().getId());
    }

    @Test
    public void testBookingSummaryResponseNullToDto() {
        assertNull(BOOKING_RESPONSE_MAPPER.bookingSummaryResponseToDto(null));
    }

    @Test
    public void testBookingSummaryResponseNullMembershipIdToDto() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingSummaryResponseToDto(new BookingSummaryBuilder().membershipId(null).build(new Random()));
        assertNotNull(bookingDTO);
        assertNull(bookingDTO.getMembershipId());
    }

    @Test
    public void testBookingResponseToDTO() {
        bookingDetail.getBookingBasicInfo().setMembershipId(1L);
        BookingDTO bookingDetailDTO = BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(bookingDetail);
        BookingDTO bookingSummaryDTO = BOOKING_RESPONSE_MAPPER.bookingSummaryResponseToDto(bookingSummary);
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingResponsesToDto(bookingSummaryDTO, bookingDetailDTO);
        assertEquals(bookingDTO.getId(), bookingSummaryDTO.getId());
        assertEquals(bookingDTO.getCurrencyCode(), bookingDetailDTO.getCurrencyCode());
        assertEquals(bookingDTO.getMembershipId(), bookingSummaryDTO.getMembershipId());
        assertEquals(bookingDTO.getFeeContainers().size(), bookingDetailDTO.getFeeContainers().size());
        assertEquals(bookingDetailDTO.getTotalAvoidFeesAmount(), EXPECTED_TOTAL_AVOID_FEE);
        assertEquals(bookingDetailDTO.getTotalCostFeesAmount(), EXPECTED_TOTAL_COST_FEE);
        assertEquals(bookingDetailDTO.getTotalPerksAmount(), EXPECTED_TOTAL_PERKS);
    }

    @Test
    public void testBookingResponsesNullToDTO() {
        assertNull(BOOKING_RESPONSE_MAPPER.bookingResponsesToDto(null, null));
    }

    @Test
    public void testBookingResponsesNullFieldsToDTO() {
        BookingDTO bookingSummaryDTO = BookingDTO.builder().id(BOOKING_ID).build();
        BookingDTO bookingDTO = BookingDTO.builder().id(BOOKING_ID)
                .feeContainers(Collections.emptyList())
                .build();
        assertNotNull(BOOKING_RESPONSE_MAPPER.bookingResponsesToDto(bookingSummaryDTO, bookingDTO));
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void testBookingResponseToDTOException() {
        BookingDTO bookingDetailDTO = BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(bookingDetail);
        BookingDTO bookingSummaryDTO = BookingDTO.builder().build();
        BOOKING_RESPONSE_MAPPER.bookingResponsesToDto(bookingSummaryDTO, bookingDetailDTO);
    }

    @Test
    public void testBookingSummariesResponsesToDto() {
        List<BookingDTO> bookingDTOS = BOOKING_RESPONSE_MAPPER.bookingSummariesResponseToDto(Arrays.asList(bookingSummary, bookingSummary, bookingSummary));
        assertEquals(bookingDTOS.size(), 3);
    }

    @Test
    public void testBookingSummariesResponseNullToDto() {
        assertNull(BOOKING_RESPONSE_MAPPER.bookingSummariesResponseToDto(null));
    }
}
