package com.odigeo.membership.robots.exceptions.consumer;

import bean.test.BeanTest;

public class RetryableExceptionTest extends BeanTest<RetryableException> {

    private static final String MESSAGE = "message";

    @Override
    protected RetryableException getBean() {
        return new RetryableException(new RuntimeException(MESSAGE));
    }
}
