package com.odigeo.membership.robots.expiration;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.configuration.ExpirationConfiguration;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipDTOBuilder;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.dto.booking.BookingStatus;
import com.odigeo.membership.robots.dto.booking.FeeContainerDTO;
import com.odigeo.membership.robots.dto.booking.FeeDTO;
import com.odigeo.membership.robots.expiration.MembershipExpirationServiceBean.MembershipStatus;
import com.odigeo.membership.robots.fees.RemnantFeeService;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.manager.membership.MembershipFeeContainerTypes;
import com.odigeo.membership.robots.report.Report;
import com.odigeo.membership.robots.report.Reporter;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.odigeo.membership.robots.apicall.ApiCall.Endpoint.CREATE_PENDING_TO_COLLECT;
import static com.odigeo.membership.robots.apicall.ApiCall.Result.FAIL;
import static com.odigeo.membership.robots.apicall.ApiCall.Result.SUCCESS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipExpirationServiceBeanTest {
    @Mock
    private ExpirationConfiguration expirationConfiguration;
    @Mock
    private ExternalModuleManager externalModuleManager;
    @Mock
    private RemnantFeeService remnantFeeService;
    @Mock
    private Reporter reporter;

    private MembershipExpirationServiceBean membershipExpirationServiceBean;

    private static final LocalDateTime CUTOVER = LocalDate.of(2019, Month.OCTOBER, 1).atStartOfDay();
    private static final LocalDate NOW = LocalDate.now();
    private static final LocalDateTime EXPIRATION_DATE = LocalDateTime.now();
    private static final String WEBSITE = "ES";
    private static final String CURRENCY = "EUR";
    private static final String ENABLED = "ENABLED";
    private static final String DISABLED = "DISABLED";
    private static final String EXPIRED = "EXPIRED";
    private static final String ACTIVATED = "ACTIVATED";
    private static final String INIT = "INIT";
    private static final String CONTRACT = BookingStatus.CONTRACT.name();
    private static final BigDecimal ONE_CENT = new BigDecimal("0.01");
    private static final BigDecimal MEMBERSHIP_SUBSCRIPTION_AMOUNT = new BigDecimal("44.99");
    private static final MembershipDTOBuilder MEMBERSHIP_DTO_ID1_BUILDER = MembershipDTO.builder().id(1L).balance(BigDecimal.TEN).activationDate(CUTOVER); // activated on cutover day
    private static final MembershipDTOBuilder MEMBERSHIP_DTO_ID2_BUILDER = MembershipDTO.builder().id(2L).balance(BigDecimal.TEN).activationDate(CUTOVER.plusMonths(1L)); // activated 1 month after cutover
    private static final MembershipDTOBuilder MEMBERSHIP_DTO_ID3_BUILDER = MembershipDTO.builder().id(3L).balance(BigDecimal.TEN).activationDate(CUTOVER).productStatus(CONTRACT); // productStatus CONTRACT
    private static final MembershipDTOBuilder MEMBERSHIP_DTO_ID4_BUILDER = MembershipDTO.builder().id(4L).activationDate(CUTOVER); // null balance
    private static final MembershipDTOBuilder MEMBERSHIP_DTO_ID5_BUILDER = MembershipDTO.builder().id(5L).balance(BigDecimal.TEN); // null activation date
    private static final MembershipDTOBuilder MEMBERSHIP_DTO_ID6_BUILDER = MembershipDTO.builder().id(6L).balance(BigDecimal.ZERO).activationDate(CUTOVER); // Zero balance
    private static final MembershipDTOBuilder MEMBERSHIP_DTO_ID7_BUILDER = MembershipDTO.builder().id(7L).balance(BigDecimal.TEN).activationDate(CUTOVER.minusMonths(1L)); // Activated before cutover
    private static final MembershipDTOBuilder MEMBERSHIP_DTO_ID8_BUILDER = MembershipDTO.builder().id(8L).balance(BigDecimal.TEN).activationDate(CUTOVER).productStatus(INIT);  // productStatus = INIT
    private static MembershipDTO MEMBERSHIP_DTO_ID1;
    private static MembershipDTO MEMBERSHIP_DTO_ID2;
    private static MembershipDTO MEMBERSHIP_DTO_ID3;
    private static MembershipDTO MEMBERSHIP_DTO_ID4;
    private static MembershipDTO MEMBERSHIP_DTO_ID5;
    private static MembershipDTO MEMBERSHIP_DTO_ID6;
    private static MembershipDTO MEMBERSHIP_DTO_ID7;
    private static MembershipDTO MEMBERSHIP_DTO_ID8;
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID1;
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID2;
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID3;
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID4;
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID5;
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID6;
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID7;
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_ID8;

    private static final SearchMembershipsDTO.SearchMembershipsDTOBuilder COMMON_SEARCH_MEMBERSHIP_DTO_BUILDER = SearchMembershipsDTO.builder()
            .withMemberAccount(false).toExpirationDate(NOW).fromExpirationDate(NOW).minBalance(null).fromActivationDate(null).autoRenewal(null);
    private static final SearchMembershipsDTO SEARCH_MEMBERSHIP_DTO_RENEWABLE_MEMBERSHIPS = COMMON_SEARCH_MEMBERSHIP_DTO_BUILDER.status(MembershipStatus.ACTIVATED.name()).autoRenewal(ENABLED).minBalance(null).fromActivationDate(null).build();
    private static final SearchMembershipsDTO SEARCH_MEMBERSHIP_DTO_PENDING_TO_COLLECT = COMMON_SEARCH_MEMBERSHIP_DTO_BUILDER.status(MembershipStatus.PENDING_TO_COLLECT.name()).minBalance(null).fromActivationDate(null).autoRenewal(null).build();
    private static final SearchMembershipsDTO SEARCH_MEMBERSHIP_DTO_PENDING_TO_ACTIVATE = COMMON_SEARCH_MEMBERSHIP_DTO_BUILDER.status(MembershipStatus.PENDING_TO_ACTIVATE.name()).minBalance(null).fromActivationDate(null).autoRenewal(null).build();
    private static final SearchMembershipsDTO SEARCH_MEMBERSHIP_DTO_DEACTIVATED = COMMON_SEARCH_MEMBERSHIP_DTO_BUILDER.status(MembershipStatus.DEACTIVATED.name()).minBalance(null).fromActivationDate(null).autoRenewal(null).build();
    private static final SearchMembershipsDTO SEARCH_MEMBERSHIP_DTO_DISABLED_RENEWABLE = COMMON_SEARCH_MEMBERSHIP_DTO_BUILDER.status(MembershipStatus.ACTIVATED.name()).autoRenewal(DISABLED).minBalance(null).fromActivationDate(null).build();
    private static final SearchMembershipsDTO SEARCH_MEMBERSHIP_DTO_EXPIRED_POSITIVE_BALANCE = COMMON_SEARCH_MEMBERSHIP_DTO_BUILDER.status(MembershipStatus.EXPIRED.name()).minBalance(ONE_CENT).fromActivationDate(CUTOVER.toLocalDate()).autoRenewal(null).build();

    private static final Map<SearchMembershipsDTO, ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO>> SEARCH_MEMBERSHIPS_ANSWERS = new HashMap<>();
    private static final Map<MembershipDTO, ApiCallWrapper<MembershipOfferDTO, MembershipDTO>> GET_SUBSCRIPTION_OFFER_ANSWERS = new HashMap<>();
    private static final Map<MembershipDTO, ApiCallWrapper<MembershipDTO, MembershipDTO>> CREATE_PENDING_TO_COLLECT_ANSWERS = new HashMap<>();
    private static final Map<MembershipDTO, ApiCallWrapper<MembershipDTO, MembershipDTO>> EXPIRE_MEMBERSHIP_ANSWERS = new HashMap<>();
    private static final Map<Long, ApiCallWrapper<BookingDTO, MembershipDTO>> GET_SUBSCRIPTION_BOOKING_ANSWERS = new HashMap<>();

    @Captor
    private ArgumentCaptor<ApiCallWrapper<MembershipOfferDTO, MembershipDTO>> createPendingToCollectRequestArgumentCaptor;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        membershipExpirationServiceBean = new MembershipExpirationServiceBean(expirationConfiguration, externalModuleManager, remnantFeeService, reporter);
    }

    @AfterMethod
    public void clearCollections() {
        SEARCH_MEMBERSHIPS_ANSWERS.clear();
        GET_SUBSCRIPTION_OFFER_ANSWERS.clear();
        CREATE_PENDING_TO_COLLECT_ANSWERS.clear();
        EXPIRE_MEMBERSHIP_ANSWERS.clear();
        GET_SUBSCRIPTION_BOOKING_ANSWERS.clear();
    }

    @Test
    public void testExpireOnlyRenewableMemberships() {
        //GIVEN
        MEMBERSHIP_DTO_ID1 = MEMBERSHIP_DTO_ID1_BUILDER.status(ACTIVATED).build();
        MEMBERSHIP_DTO_ID2 = MEMBERSHIP_DTO_ID2_BUILDER.status(ACTIVATED).build();
        MEMBERSHIP_DTO_ID3 = MEMBERSHIP_DTO_ID3_BUILDER.status(ACTIVATED).build();
        MEMBERSHIP_DTO_ID4 = MEMBERSHIP_DTO_ID4_BUILDER.status(ACTIVATED).build();
        MEMBERSHIP_DTO_ID5 = MEMBERSHIP_DTO_ID5_BUILDER.status(ACTIVATED).build();
        MEMBERSHIP_DTO_ID6 = MEMBERSHIP_DTO_ID6_BUILDER.status(ACTIVATED).build();
        MEMBERSHIP_DTO_ID7 = MEMBERSHIP_DTO_ID7_BUILDER.status(ACTIVATED).build();
        MEMBERSHIP_DTO_ID8 = MEMBERSHIP_DTO_ID8_BUILDER.status(ACTIVATED).build();

        initExpiredMembershipDtoMocks();

        List<MembershipDTO> renewableMemberships = Arrays.asList(MEMBERSHIP_DTO_ID1, MEMBERSHIP_DTO_ID2, MEMBERSHIP_DTO_ID3, MEMBERSHIP_DTO_ID4,
                MEMBERSHIP_DTO_ID5, MEMBERSHIP_DTO_ID6, MEMBERSHIP_DTO_ID7, MEMBERSHIP_DTO_ID8);

        given(externalModuleManager.searchMemberships(any())).will(answerSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_RENEWABLE_MEMBERSHIPS, renewableMemberships, true));
        givenSearchNonRenewableMembershipsWillReturnEmptyList();
        given(externalModuleManager.getMembershipSubscriptionOffer(any())).will(answerMembershipSubscriptionOfferResponses(renewableMemberships, true));
        given(externalModuleManager.createPendingToCollectFromPreviousMembership(any())).will(answerPendingToCollectResponses(renewableMemberships, true));
        mockExpireMembershipResponse(renewableMemberships);

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        //THEN
        validateOfferWasRetrievedForRenewableMemberships(renewableMemberships);
        validateAllMembershipsWereExpired(renewableMemberships);
        validatePrimeBookingNeverRetrievedForTheseMemberships(Arrays.asList(MEMBERSHIP_DTO_EXPIRED_ID4, MEMBERSHIP_DTO_EXPIRED_ID5,
                MEMBERSHIP_DTO_EXPIRED_ID6, MEMBERSHIP_DTO_EXPIRED_ID7, MEMBERSHIP_DTO_EXPIRED_ID8));

        then(externalModuleManager).should(times(renewableMemberships.size())).createPendingToCollectFromPreviousMembership(createPendingToCollectRequestArgumentCaptor.capture());
        assertTrue(createPendingToCollectRequestArgumentCaptor.getAllValues().stream()
                .map(ApiCallWrapper::getRequest)
                .allMatch(renewableMemberships::contains)
        );

        validatePrimeBookingRetrievedForTheseMemberships(Arrays.asList(MEMBERSHIP_DTO_EXPIRED_ID1, MEMBERSHIP_DTO_EXPIRED_ID2, MEMBERSHIP_DTO_EXPIRED_ID3));

        then(remnantFeeService).should(times(3)).processPrimeSubscriptionBookingFees(any());
        then(remnantFeeService).should(times(3)).consumeMembershipRemnantBalance(any());
        then(reporter).should().generateReportLog(any(Report.class));
    }

    @Test
    public void testExpireOnlyNotRenewableMemberships() {
        //GIVEN
        MEMBERSHIP_DTO_ID1 = MEMBERSHIP_DTO_ID1_BUILDER.status(MembershipStatus.PENDING_TO_ACTIVATE.name()).build();
        MEMBERSHIP_DTO_ID2 = MEMBERSHIP_DTO_ID2_BUILDER.status(MembershipStatus.EXPIRED.name()).build();
        MEMBERSHIP_DTO_ID3 = MEMBERSHIP_DTO_ID3_BUILDER.status(MembershipStatus.PENDING_TO_COLLECT.name()).build();
        MEMBERSHIP_DTO_ID4 = MEMBERSHIP_DTO_ID4_BUILDER.status(MembershipStatus.PENDING_TO_COLLECT.name()).build();
        MEMBERSHIP_DTO_ID5 = MEMBERSHIP_DTO_ID5_BUILDER.status(MembershipStatus.DEACTIVATED.name()).build();
        MEMBERSHIP_DTO_ID6 = MEMBERSHIP_DTO_ID6_BUILDER.status(MembershipStatus.DEACTIVATED.name()).build();
        MEMBERSHIP_DTO_ID7 = MEMBERSHIP_DTO_ID7_BUILDER.status(MembershipStatus.ACTIVATED.name()).autoRenewal(DISABLED).build();
        MEMBERSHIP_DTO_ID8 = MEMBERSHIP_DTO_ID8_BUILDER.status(MembershipStatus.ACTIVATED.name()).autoRenewal(DISABLED).build();

        initExpiredMembershipDtoMocks();

        List<MembershipDTO> pendingToActivateMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID1);
        List<MembershipDTO> expiredPositiveBalanceMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID2);
        List<MembershipDTO> pendingToCollectMemberships = Arrays.asList(MEMBERSHIP_DTO_ID3, MEMBERSHIP_DTO_ID4);
        List<MembershipDTO> deactivatedMemberships = Arrays.asList(MEMBERSHIP_DTO_ID5, MEMBERSHIP_DTO_ID6);
        List<MembershipDTO> renewalDisabledMemberships = Arrays.asList(MEMBERSHIP_DTO_ID7, MEMBERSHIP_DTO_ID8);

        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_RENEWABLE_MEMBERSHIPS, Collections.emptyList());
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_RENEWABLE_MEMBERSHIPS, Collections.emptyList());
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_COLLECT, pendingToCollectMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_ACTIVATE, pendingToActivateMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DEACTIVATED, deactivatedMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DISABLED_RENEWABLE, renewalDisabledMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_EXPIRED_POSITIVE_BALANCE, expiredPositiveBalanceMemberships);

        mockExpireMembershipResponse(pendingToActivateMemberships);
        mockExpireMembershipResponse(expiredPositiveBalanceMemberships);
        mockExpireMembershipResponse(pendingToCollectMemberships);
        mockExpireMembershipResponse(deactivatedMemberships);
        mockExpireMembershipResponse(renewalDisabledMemberships);

        List<MembershipDTO> membershipsToProcess = Arrays.asList(MEMBERSHIP_DTO_EXPIRED_ID1, MEMBERSHIP_DTO_EXPIRED_ID2, MEMBERSHIP_DTO_EXPIRED_ID3);
        given(externalModuleManager.getPrimeSubscriptionBookingForMembership(any())).will(answerGetPrimeSubscriptionBookingResponses(membershipsToProcess, true));

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        //THEN
        then(externalModuleManager).should(never()).getMembershipSubscriptionOffer(any(MembershipDTO.class));
        then(externalModuleManager).should(never()).createPendingToCollectFromPreviousMembership(any());

        validateAllMembershipsWereExpired(Arrays.asList(MEMBERSHIP_DTO_ID1, MEMBERSHIP_DTO_ID2, MEMBERSHIP_DTO_ID3, MEMBERSHIP_DTO_ID4,
                MEMBERSHIP_DTO_ID5, MEMBERSHIP_DTO_ID6, MEMBERSHIP_DTO_ID7, MEMBERSHIP_DTO_ID8));

        then(remnantFeeService).should(times(3)).processPrimeSubscriptionBookingFees(any());
        then(remnantFeeService).should(times(3)).consumeMembershipRemnantBalance(any());
        then(reporter).should().generateReportLog(any(Report.class));
    }

    private void mockExpireMembershipResponse(List<MembershipDTO> pendingToActivateMemberships) {
        given(externalModuleManager.expireMembership(any())).will(answerExpireMembershipResponses(pendingToActivateMemberships, true));
    }

    private void mockSearchMembershipsResponse(SearchMembershipsDTO searchMembershipDto, List<MembershipDTO> resultList) {
        given(externalModuleManager.searchMemberships(any())).will(answerSearchMembershipsResponse(searchMembershipDto, resultList, true));
    }

    @Test
    public void testDoNotProcessRemnantFeeMemberships() {
        //GIVEN
        MEMBERSHIP_DTO_ID4 = MEMBERSHIP_DTO_ID4_BUILDER.status(MembershipStatus.PENDING_TO_COLLECT.name()).build();
        MEMBERSHIP_DTO_ID5 = MEMBERSHIP_DTO_ID5_BUILDER.status(MembershipStatus.PENDING_TO_ACTIVATE.name()).build();
        MEMBERSHIP_DTO_ID6 = MEMBERSHIP_DTO_ID6_BUILDER.status(MembershipStatus.DEACTIVATED.name()).build();
        MEMBERSHIP_DTO_ID7 = MEMBERSHIP_DTO_ID7_BUILDER.status(MembershipStatus.ACTIVATED.name()).autoRenewal(DISABLED).build();
        MEMBERSHIP_DTO_ID8 = MEMBERSHIP_DTO_ID8_BUILDER.status(MembershipStatus.ACTIVATED.name()).build();

        MEMBERSHIP_DTO_EXPIRED_ID4 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID4);
        MEMBERSHIP_DTO_EXPIRED_ID5 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID5);
        MEMBERSHIP_DTO_EXPIRED_ID6 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID6);
        MEMBERSHIP_DTO_EXPIRED_ID7 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID7);
        MEMBERSHIP_DTO_EXPIRED_ID8 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID8);

        List<MembershipDTO> pendingToActivateMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID5);
        List<MembershipDTO> pendingToCollectMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID4);
        List<MembershipDTO> deactivatedMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID6);
        List<MembershipDTO> renewalDisabledMemberships = Arrays.asList(MEMBERSHIP_DTO_ID7, MEMBERSHIP_DTO_ID8);

        // define searchMembership responses for each membership
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_RENEWABLE_MEMBERSHIPS, Collections.emptyList());
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_COLLECT, pendingToCollectMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_ACTIVATE, pendingToActivateMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DEACTIVATED, deactivatedMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DISABLED_RENEWABLE, renewalDisabledMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_EXPIRED_POSITIVE_BALANCE, Collections.emptyList());

        // define expireMembership responses for each membership
        mockExpireMembershipResponse(pendingToActivateMemberships);
        mockExpireMembershipResponse(pendingToCollectMemberships);
        mockExpireMembershipResponse(deactivatedMemberships);
        mockExpireMembershipResponse(renewalDisabledMemberships);

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        //THEN
        then(externalModuleManager).should(never()).getPrimeSubscriptionBookingForMembership(any());
        then(remnantFeeService).should(never()).processPrimeSubscriptionBookingFees(any());
        then(remnantFeeService).should(never()).consumeMembershipRemnantBalance(any());
        then(reporter).should().generateReportLog(any(Report.class));
    }

    @Test
    public void testExpireBothRenewableOrNotMemberships() {
        //GIVEN
        initRenewableAndNotRenewableMembershipDtoMocks();
        initExpiredMembershipDtoMocks();
        mockResponsesFromExternalModuleManager();

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        //THEN
        validateAllMembershipsWereExpired(Arrays.asList(MEMBERSHIP_DTO_ID1, MEMBERSHIP_DTO_ID2, MEMBERSHIP_DTO_ID3, MEMBERSHIP_DTO_ID4,
                MEMBERSHIP_DTO_ID5, MEMBERSHIP_DTO_ID6, MEMBERSHIP_DTO_ID7, MEMBERSHIP_DTO_ID8));

        then(remnantFeeService).should(times(3)).processPrimeSubscriptionBookingFees(any());
        then(remnantFeeService).should(times(3)).consumeMembershipRemnantBalance(any());
        then(reporter).should().generateReportLog(any(Report.class));
    }

    @Test
    public void testExpireBothRenewableOrNotMembershipsAllSuccess() {
        //GIVEN
        initRenewableAndNotRenewableMembershipDtoMocks();
        initExpiredMembershipDtoMocks();
        mockResponsesFromExternalModuleManager();
        List<MembershipDTO> membershipsToProcess = Arrays.asList(MEMBERSHIP_DTO_EXPIRED_ID1, MEMBERSHIP_DTO_EXPIRED_ID2, MEMBERSHIP_DTO_EXPIRED_ID3);
        given(externalModuleManager.getPrimeSubscriptionBookingForMembership(any())).will(answerGetPrimeSubscriptionBookingResponses(membershipsToProcess, true));

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        //THEN
        validatePrimeBookingRetrievedForTheseMemberships(Arrays.asList(MEMBERSHIP_DTO_EXPIRED_ID1, MEMBERSHIP_DTO_EXPIRED_ID2, MEMBERSHIP_DTO_EXPIRED_ID3));
        validatePrimeBookingNeverRetrievedForTheseMemberships(Arrays.asList(MEMBERSHIP_DTO_EXPIRED_ID4, MEMBERSHIP_DTO_EXPIRED_ID5,
                MEMBERSHIP_DTO_EXPIRED_ID6, MEMBERSHIP_DTO_EXPIRED_ID7, MEMBERSHIP_DTO_EXPIRED_ID8));

        then(remnantFeeService).should(times(3)).processPrimeSubscriptionBookingFees(any());
        then(remnantFeeService).should(times(3)).consumeMembershipRemnantBalance(any());
        then(reporter).should().generateReportLog(any(Report.class));
    }

    @Test
    public void testExpireOnlyRenewableGetOfferFails() {
        //GIVEN
        MEMBERSHIP_DTO_ID1 = MEMBERSHIP_DTO_ID1_BUILDER.status(MembershipStatus.ACTIVATED.name()).build();
        MEMBERSHIP_DTO_ID2 = MEMBERSHIP_DTO_ID2_BUILDER.status(MembershipStatus.EXPIRED.name()).build();
        MEMBERSHIP_DTO_ID3 = MEMBERSHIP_DTO_ID3_BUILDER.status(MembershipStatus.PENDING_TO_ACTIVATE.name()).build();

        MEMBERSHIP_DTO_EXPIRED_ID1 = MembershipDTOBuilder.builderFromDto(MEMBERSHIP_DTO_ID1).status(EXPIRED).expirationDate(EXPIRATION_DATE).build();
        MEMBERSHIP_DTO_EXPIRED_ID2 = MembershipDTOBuilder.builderFromDto(MEMBERSHIP_DTO_ID2).status(EXPIRED).expirationDate(EXPIRATION_DATE).build();
        MEMBERSHIP_DTO_EXPIRED_ID3 = MembershipDTOBuilder.builderFromDto(MEMBERSHIP_DTO_ID3).status(EXPIRED).expirationDate(EXPIRATION_DATE).build();

        List<MembershipDTO> renewableMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID1);
        List<MembershipDTO> expiredPositiveBalanceMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID2);
        List<MembershipDTO> pendingToActivateMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID3);
        List<MembershipDTO> membershipsToProcess = Arrays.asList(MEMBERSHIP_DTO_EXPIRED_ID1, MEMBERSHIP_DTO_EXPIRED_ID2, MEMBERSHIP_DTO_EXPIRED_ID3);

        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_RENEWABLE_MEMBERSHIPS, renewableMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_ACTIVATE, pendingToActivateMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_EXPIRED_POSITIVE_BALANCE, expiredPositiveBalanceMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_COLLECT, Collections.emptyList());
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DEACTIVATED, Collections.emptyList());
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DISABLED_RENEWABLE, Collections.emptyList());

        // getMembershipSubscriptionOffer and createPendingToCollect calls return failed responses
        given(externalModuleManager.getMembershipSubscriptionOffer(any())).will(answerMembershipSubscriptionOfferResponses(renewableMemberships, false));
        given(externalModuleManager.createPendingToCollectFromPreviousMembership(any())).will(answerPendingToCollectResponses(renewableMemberships, false));

        mockExpireMembershipResponse(pendingToActivateMemberships);
        mockExpireMembershipResponse(expiredPositiveBalanceMemberships);
        given(externalModuleManager.getPrimeSubscriptionBookingForMembership(any())).will(answerGetPrimeSubscriptionBookingResponses(membershipsToProcess, true));

        //WHEN
        membershipExpirationServiceBean.expireMemberships();

        //THEN
        then(externalModuleManager).should().getMembershipSubscriptionOffer(MEMBERSHIP_DTO_ID1); // the only renewable
        then(externalModuleManager).should(never()).getMembershipSubscriptionOffer(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should(never()).getMembershipSubscriptionOffer(MEMBERSHIP_DTO_ID3);

        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(createPendingToCollectRequestArgumentCaptor.capture());
        ApiCallWrapper<MembershipOfferDTO, MembershipDTO> pendingToCollectRequest = createPendingToCollectRequestArgumentCaptor.getValue();
        assertFalse(pendingToCollectRequest.isSuccessful());
        assertEquals(pendingToCollectRequest.getRequest(), MEMBERSHIP_DTO_ID1);

        then(externalModuleManager).should(never()).expireMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should().expireMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should().expireMembership(MEMBERSHIP_DTO_ID3);

        then(externalModuleManager).should(never()).getPrimeSubscriptionBookingForMembership(MEMBERSHIP_DTO_EXPIRED_ID1);
        validatePrimeBookingRetrievedForTheseMemberships(Arrays.asList(MEMBERSHIP_DTO_EXPIRED_ID2, MEMBERSHIP_DTO_EXPIRED_ID3));

        then(remnantFeeService).should(times(2)).processPrimeSubscriptionBookingFees(any());
        then(remnantFeeService).should(times(2)).consumeMembershipRemnantBalance(any());
        then(reporter).should().generateReportLog(any(Report.class));
    }

    private void initRenewableAndNotRenewableMembershipDtoMocks() {
        MEMBERSHIP_DTO_ID1 = MEMBERSHIP_DTO_ID1_BUILDER.status(MembershipStatus.ACTIVATED.name()).build();
        MEMBERSHIP_DTO_ID2 = MEMBERSHIP_DTO_ID2_BUILDER.status(MembershipStatus.EXPIRED.name()).build();
        MEMBERSHIP_DTO_ID3 = MEMBERSHIP_DTO_ID3_BUILDER.status(MembershipStatus.PENDING_TO_ACTIVATE.name()).build();
        MEMBERSHIP_DTO_ID4 = MEMBERSHIP_DTO_ID4_BUILDER.status(MembershipStatus.ACTIVATED.name()).autoRenewal(DISABLED).build();
        MEMBERSHIP_DTO_ID5 = MEMBERSHIP_DTO_ID5_BUILDER.status(MembershipStatus.DEACTIVATED.name()).build();
        MEMBERSHIP_DTO_ID6 = MEMBERSHIP_DTO_ID6_BUILDER.status(MembershipStatus.DEACTIVATED.name()).build();
        MEMBERSHIP_DTO_ID7 = MEMBERSHIP_DTO_ID7_BUILDER.status(MembershipStatus.PENDING_TO_COLLECT.name()).build();
        MEMBERSHIP_DTO_ID8 = MEMBERSHIP_DTO_ID8_BUILDER.status(MembershipStatus.PENDING_TO_COLLECT.name()).build();
    }

    private void initExpiredMembershipDtoMocks() {
        MEMBERSHIP_DTO_EXPIRED_ID1 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID1);
        MEMBERSHIP_DTO_EXPIRED_ID2 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID2);
        MEMBERSHIP_DTO_EXPIRED_ID3 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID3);
        MEMBERSHIP_DTO_EXPIRED_ID4 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID4);
        MEMBERSHIP_DTO_EXPIRED_ID5 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID5);
        MEMBERSHIP_DTO_EXPIRED_ID6 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID6);
        MEMBERSHIP_DTO_EXPIRED_ID7 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID7);
        MEMBERSHIP_DTO_EXPIRED_ID8 = getMembershipDtoFromMembershipDtoId(MEMBERSHIP_DTO_ID8);
    }

    private MembershipDTO getMembershipDtoFromMembershipDtoId(MembershipDTO membershipDtoId) {
        return MembershipDTOBuilder.builderFromDto(membershipDtoId).status(EXPIRED).expirationDate(EXPIRATION_DATE).build();
    }

    private void validateOfferWasRetrievedForRenewableMemberships(List<MembershipDTO> renewableMemberships) {
        renewableMemberships.forEach(membershipDTO -> then(externalModuleManager).should().getMembershipSubscriptionOffer(membershipDTO));
    }

    private void validateAllMembershipsWereExpired(List<MembershipDTO> renewableMemberships) {
        renewableMemberships.forEach(membershipDTO -> then(externalModuleManager).should().expireMembership(membershipDTO));
    }

    private void validatePrimeBookingRetrievedForTheseMemberships(List<MembershipDTO> membershipDTOS) {
        membershipDTOS.forEach(membershipDTO -> then(externalModuleManager).should().getPrimeSubscriptionBookingForMembership(membershipDTO));
    }

    private void validatePrimeBookingNeverRetrievedForTheseMemberships(List<MembershipDTO> membershipDTOS) {
        membershipDTOS.forEach(membershipDTO -> then(externalModuleManager).should(never()).getPrimeSubscriptionBookingForMembership(membershipDTO));
    }

    private void mockResponsesFromExternalModuleManager() {
        List<MembershipDTO> renewableMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID1);
        List<MembershipDTO> expiredPositiveBalanceMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID2);
        List<MembershipDTO> pendingToActivateMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID3);
        List<MembershipDTO> renewalDisabledMemberships = Collections.singletonList(MEMBERSHIP_DTO_ID4);
        List<MembershipDTO> deactivatedMemberships = Arrays.asList(MEMBERSHIP_DTO_ID5, MEMBERSHIP_DTO_ID6);
        List<MembershipDTO> pendingToCollectMemberships = Arrays.asList(MEMBERSHIP_DTO_ID7, MEMBERSHIP_DTO_ID8);

        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_RENEWABLE_MEMBERSHIPS, renewableMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_COLLECT, pendingToCollectMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_ACTIVATE, pendingToActivateMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DEACTIVATED, deactivatedMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DISABLED_RENEWABLE, renewalDisabledMemberships);
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_EXPIRED_POSITIVE_BALANCE, expiredPositiveBalanceMemberships);

        given(externalModuleManager.getMembershipSubscriptionOffer(any())).will(answerMembershipSubscriptionOfferResponses(renewableMemberships, true));
        given(externalModuleManager.createPendingToCollectFromPreviousMembership(any())).will(answerPendingToCollectResponses(renewableMemberships, true));

        mockExpireMembershipResponse(renewableMemberships);
        mockExpireMembershipResponse(pendingToActivateMemberships);
        mockExpireMembershipResponse(expiredPositiveBalanceMemberships);
        mockExpireMembershipResponse(pendingToCollectMemberships);
        mockExpireMembershipResponse(deactivatedMemberships);
        mockExpireMembershipResponse(renewalDisabledMemberships);
    }

    private void givenSearchNonRenewableMembershipsWillReturnEmptyList() {
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_COLLECT, Collections.emptyList());
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_PENDING_TO_ACTIVATE, Collections.emptyList());
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DEACTIVATED, Collections.emptyList());
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_DISABLED_RENEWABLE, Collections.emptyList());
        mockSearchMembershipsResponse(SEARCH_MEMBERSHIP_DTO_EXPIRED_POSITIVE_BALANCE, Collections.emptyList());
    }

    private Answer<ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO>> answerSearchMembershipsResponse(SearchMembershipsDTO searchRequest, List<MembershipDTO> searchResultList, boolean successfulAnswers) {
        final ApiCallWrapper<List<MembershipDTO>, SearchMembershipsDTO> responseWrapper = new ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS, SUCCESS)
                .result(successfulAnswers ? SUCCESS : FAIL)
                .request(searchRequest)
                .response(searchResultList)
                .build();

        final ApiCallWrapperBuilder<List<MembershipDTO>, SearchMembershipsDTO> defaultFailResponse = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.SEARCH_MEMBERSHIPS, FAIL);

        SEARCH_MEMBERSHIPS_ANSWERS.put(searchRequest, responseWrapper);

        return (InvocationOnMock invocation) -> {
            final SearchMembershipsDTO requestDto = (SearchMembershipsDTO) invocation.getArguments()[0];

            return SEARCH_MEMBERSHIPS_ANSWERS.getOrDefault(requestDto, defaultFailResponse.request(requestDto).build());
        };
    }

    private Answer<ApiCallWrapper<MembershipOfferDTO, MembershipDTO>> answerMembershipSubscriptionOfferResponses(List<MembershipDTO> renewableMemberships, boolean successfulAnswers) {
        final ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO> defaultFailResponse = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.GET_OFFER, FAIL);

        renewableMemberships.forEach(membershipToRenew -> GET_SUBSCRIPTION_OFFER_ANSWERS.put(membershipToRenew, buildSubscriptionOfferResponse(membershipToRenew, successfulAnswers)));

        return (InvocationOnMock invocation) -> {
            final MembershipDTO requestDto = (MembershipDTO) invocation.getArguments()[0];
            return GET_SUBSCRIPTION_OFFER_ANSWERS.getOrDefault(requestDto, defaultFailResponse.request(requestDto).build());
        };
    }

    private ApiCallWrapper<MembershipOfferDTO, MembershipDTO> buildSubscriptionOfferResponse(MembershipDTO membership, boolean successfulAnswers) {
        return new ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO>(ApiCall.Endpoint.GET_OFFER)
                .result(successfulAnswers ? SUCCESS : FAIL)
                .request(membership)
                .response(MembershipOfferDTO.builder().price(BigDecimal.TEN).website(WEBSITE).currencyCode(CURRENCY).duration(12).build())
                .build();
    }

    private Answer<ApiCallWrapper<MembershipDTO, MembershipDTO>> answerPendingToCollectResponses(List<MembershipDTO> renewableMemberships, boolean successfulAnswers) {
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> defaultFailedResponse = new ApiCallWrapperBuilder<>(CREATE_PENDING_TO_COLLECT, FAIL);

        GET_SUBSCRIPTION_OFFER_ANSWERS.values().stream()
                .filter(wrapper -> renewableMemberships.contains(wrapper.getRequest()))
                .forEach(subscriptionOfferResponse ->
                        CREATE_PENDING_TO_COLLECT_ANSWERS.put(subscriptionOfferResponse.getRequest(), buildPendingToCollectResponse(subscriptionOfferResponse, successfulAnswers)));

        return (InvocationOnMock invocation) -> {
            final MembershipDTO previousMembership = ((ApiCallWrapper<MembershipOfferDTO, MembershipDTO>) invocation.getArguments()[0]).getRequest();
            return CREATE_PENDING_TO_COLLECT_ANSWERS.getOrDefault(previousMembership, defaultFailedResponse.request(previousMembership).build());
        };
    }

    private ApiCallWrapper<MembershipDTO, MembershipDTO> buildPendingToCollectResponse(ApiCallWrapper<MembershipOfferDTO, MembershipDTO> offerResponse, boolean successfulAnswers) {
        MembershipDTO previousMembership = offerResponse.getRequest();
        MembershipOfferDTO subscriptionOffer = offerResponse.getResponse();
        MembershipDTO pendingToCollectMembership = MembershipDTOBuilder.builderFromDto(previousMembership)
                .expirationDate(NOW.plusMonths(12).atStartOfDay())
                .totalPrice(subscriptionOffer.getPrice())
                .currencyCode(subscriptionOffer.getCurrencyCode())
                .monthsDuration(subscriptionOffer.getDuration())
                .id(1000 + previousMembership.getId()).build();

        return new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(CREATE_PENDING_TO_COLLECT)
                .result(successfulAnswers ? SUCCESS : FAIL)
                .request(previousMembership)
                .response(pendingToCollectMembership)
                .build();
    }

    private Answer<ApiCallWrapper<MembershipDTO, MembershipDTO>> answerExpireMembershipResponses(List<MembershipDTO> membershipsToExpire, boolean successfulAnswers) {
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> defaultFailResponse = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP, FAIL);

        membershipsToExpire.forEach(
                membershipToExpire -> EXPIRE_MEMBERSHIP_ANSWERS.put(membershipToExpire, buildExpireMembershipResponse(membershipToExpire, successfulAnswers)));

        return (InvocationOnMock invocation) -> {
            final MembershipDTO requestDto = (MembershipDTO) invocation.getArguments()[0];
            return EXPIRE_MEMBERSHIP_ANSWERS.getOrDefault(requestDto, defaultFailResponse.request(requestDto).build());
        };
    }

    private ApiCallWrapper<MembershipDTO, MembershipDTO> buildExpireMembershipResponse(MembershipDTO membershipToExpire, boolean successfulAnswers) {
        MembershipDTO expiredMembership = getMembershipDtoFromMembershipDtoId(membershipToExpire);

        return new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
                .result(successfulAnswers ? SUCCESS : FAIL)
                .request(membershipToExpire)
                .response(expiredMembership)
                .build();
    }

    private Answer<ApiCallWrapper<BookingDTO, MembershipDTO>> answerGetPrimeSubscriptionBookingResponses(List<MembershipDTO> expiredMemberships, boolean successfulAnswers) {
        final ApiCallWrapperBuilder<BookingDTO, MembershipDTO> defaultFailResponse = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.GET_BOOKING, FAIL);

        expiredMemberships.forEach(
                membershipToExpire -> GET_SUBSCRIPTION_BOOKING_ANSWERS.put(membershipToExpire.getId(), buildGetPrimeSubscriptionBookingResponse(membershipToExpire, successfulAnswers)));

        return (InvocationOnMock invocation) -> {
            final MembershipDTO requestDto = (MembershipDTO) invocation.getArguments()[0];
            return GET_SUBSCRIPTION_BOOKING_ANSWERS.getOrDefault(requestDto.getId(), defaultFailResponse.request(requestDto).build());
        };
    }

    private ApiCallWrapper<BookingDTO, MembershipDTO> buildGetPrimeSubscriptionBookingResponse(MembershipDTO requestMembership, boolean successfulAnswers) {
        return new ApiCallWrapperBuilder<BookingDTO, MembershipDTO>(ApiCall.Endpoint.GET_BOOKING)
                .result(successfulAnswers ? SUCCESS : FAIL)
                .request(requestMembership)
                .response(buildBookingForMembership(requestMembership))
                .build();
    }

    private BookingDTO buildBookingForMembership(MembershipDTO membership) {
        MembershipFeeContainerTypes membershipFeeType = MembershipFeeContainerTypes.MEMBERSHIP_SUBSCRIPTION;
        List<FeeDTO> fees = Collections.singletonList(FeeDTO.builder().subCode(membershipFeeType.getSubCode().name()).amount(MEMBERSHIP_SUBSCRIPTION_AMOUNT).build());
        return BookingDTO.builder()
                .id(membership.getId() * 10)
                .currencyCode(CURRENCY)
                .membershipId(membership.getId())
                .feeContainers(Collections.singletonList(buildFeeContainer(membershipFeeType.name(), fees)))
                .build();
    }

    private FeeContainerDTO buildFeeContainer(String feeContainerType, List<FeeDTO> fees) {
        return FeeContainerDTO.builder().feeContainerType(feeContainerType)
                .fees(fees)
                .build();
    }
}
