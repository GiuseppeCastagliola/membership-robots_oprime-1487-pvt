package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.manager.bookingapi.SearchBookingsRequestBuilder;
import org.apache.commons.lang3.BooleanUtils;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class BookingRequestMapperTest {
    private static final BookingRequestMapper BOOKING_REQUEST_MAPPER = Mappers.getMapper(BookingRequestMapper.class);
    private static final long ID = 1L;

    @Test
    public void testNullMapper() {
        assertNull(BOOKING_REQUEST_MAPPER.dtoToPrimeSubscriptionBookingSummaryRequest(null));
        assertNull(BOOKING_REQUEST_MAPPER.trackingDtoFromBookingDTO(null));
    }

    @Test
    public void testPrimeSubscriptionBookingSummaryMapper() {
        SearchBookingsRequestBuilder searchBookingsRequestBuilder = BOOKING_REQUEST_MAPPER.dtoToPrimeSubscriptionBookingSummaryRequest(MembershipDTO.builder().id(ID).build());
        assertEquals(searchBookingsRequestBuilder.build().getMembershipId(), Long.valueOf(ID));
        assertTrue(BooleanUtils.toBoolean(searchBookingsRequestBuilder.build().getIsBookingSubscriptionPrime()));
    }

    @Test
    public void testTrackingDTOFromBookingDTO() {
        BookingDTO bookingDto = BookingDTO.builder().membershipId(ID).id(ID).bookingDate(LocalDateTime.MIN).totalAvoidFeesAmount(BigDecimal.ONE).totalCostFeesAmount(BigDecimal.ONE).totalPerksAmount(BigDecimal.ONE).build();
        BookingTrackingDTO bookingTrackingDTO = BOOKING_REQUEST_MAPPER.trackingDtoFromBookingDTO(bookingDto);
        assertEquals(bookingTrackingDTO.getBookingId(), bookingDto.getId());
        assertEquals(bookingTrackingDTO.getAvoidFeeAmount(), bookingDto.getTotalAvoidFeesAmount());
    }
}