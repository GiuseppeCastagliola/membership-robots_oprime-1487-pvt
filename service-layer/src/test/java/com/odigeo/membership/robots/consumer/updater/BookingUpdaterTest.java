package com.odigeo.membership.robots.consumer.updater;

import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.bookingapi.BookingApiException;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import org.apache.commons.lang3.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.MockitoAnnotations.openMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingUpdaterTest {
    private static final long BOOKING_ID = 1L;
    private static final long MEMBERSHIP_ID = 1L;
    @Mock
    private ExternalModuleManager externalModuleManager;
    @Mock
    private MembershipRobotsMetrics membershipRobotsMetrics;
    @InjectMocks
    private BookingUpdater bookingUpdater;
    private MembershipDTO membershipDTO;
    private BookingDTO bookingDTO;


    @BeforeMethod
    public void setUp() {
        openMocks(this);
        membershipDTO = MembershipDTO.builder()
                .id(MEMBERSHIP_ID)
                .balance(BigDecimal.ONE)
                .build();
        bookingDTO = BookingDTO.builder()
                .id(BOOKING_ID)
                .totalPerksAmount(BigDecimal.ONE)
                .totalCostFeesAmount(BigDecimal.ONE)
                .totalAvoidFeesAmount(BigDecimal.ONE)
                .build();
    }

    @AfterMethod
    public void cleanUp() {
        bookingUpdater = null;
    }

    @Test
    public void testBalanceBooking() {
        given(externalModuleManager.updateBooking(eq(bookingDTO.getId()), any()))
                .willReturn(new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(ApiCall.Endpoint.UPDATE_BOOKING, ApiCall.Result.SUCCESS)
                        .response(bookingDTO).build());
        bookingUpdater.updateBookingWithUnbalancedFees(bookingDTO, membershipDTO);
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.UPDATE_BOOKING_SUCCESS));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.UPDATE_BOOKING_ATTEMPTS));
        then(externalModuleManager).should().updateBooking(eq(bookingDTO.getId()), any());
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testBalanceBookingFailUnknown() {
        given(externalModuleManager.updateBooking(eq(bookingDTO.getId()), any()))
                .willReturn(new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(ApiCall.Endpoint.UPDATE_BOOKING, ApiCall.Result.FAIL)
                        .build());
        bookingUpdater.updateBookingWithUnbalancedFees(bookingDTO, membershipDTO);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testBalanceBookingFail() {
        given(externalModuleManager.updateBooking(eq(bookingDTO.getId()), any()))
                .willReturn(new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(ApiCall.Endpoint.UPDATE_BOOKING, ApiCall.Result.FAIL)
                        .throwable(new BookingApiException(StringUtils.EMPTY))
                        .build());
        bookingUpdater.updateBookingWithUnbalancedFees(bookingDTO, membershipDTO);
    }

    @Test
    public void testUpdateBookingBasicFreeFirstBooking() {
        given(externalModuleManager.updateBooking(eq(bookingDTO.getId()), any()))
                .willReturn(new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(ApiCall.Endpoint.UPDATE_BOOKING, ApiCall.Result.SUCCESS)
                        .response(bookingDTO).build());
        given(externalModuleManager.searchBookings(any()))
                .willReturn(new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS, ApiCall.Result.SUCCESS)
                        .build());
        assertTrue(bookingUpdater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO));
        then(externalModuleManager).should().updateBooking(eq(bookingDTO.getId()), any());
        then(externalModuleManager).should().searchBookings(any());
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.BASIC_FREE_UPDATES));
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.BASIC_FREE_UPDATES_SUCCESS));
    }

    @Test
    public void testUpdateBookingBasicFreeFirstBookingAlreadyUpdated() {
        given(externalModuleManager.updateBooking(eq(bookingDTO.getId()), any()))
                .willReturn(new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(ApiCall.Endpoint.UPDATE_BOOKING, ApiCall.Result.SUCCESS)
                        .response(bookingDTO).build());
        given(externalModuleManager.searchBookings(any()))
                .willReturn(new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS, ApiCall.Result.SUCCESS)
                        .response(Collections.singletonList(bookingDTO))
                        .build());
        assertFalse(bookingUpdater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO));
        then(externalModuleManager).should(never()).updateBooking(eq(bookingDTO.getId()), any());
        then(externalModuleManager).should().searchBookings(any());
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.BASIC_FREE_UPDATES_ALREADY_WITH_SUBSCRIPTION));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testUpdateBookingBasicFreeSearchBookingFails() {
        given(externalModuleManager.searchBookings(any()))
                .willReturn(new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS, ApiCall.Result.FAIL)
                        .throwable(new Exception())
                        .build());
        bookingUpdater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testUpdateBookingBasicFreeUpdateBookingFails() {
        given(externalModuleManager.updateBooking(eq(bookingDTO.getId()), any()))
                .willReturn(new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(ApiCall.Endpoint.UPDATE_BOOKING, ApiCall.Result.FAIL)
                        .throwable(new Exception())
                        .build());
        given(externalModuleManager.searchBookings(any()))
                .willReturn(new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS, ApiCall.Result.SUCCESS)
                        .build());
        bookingUpdater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testUpdateBookingBasicFreeSearchBookingFailsUnknown() {
        given(externalModuleManager.searchBookings(any()))
                .willReturn(new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS, ApiCall.Result.FAIL)
                        .build());
        bookingUpdater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO);
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testUpdateBookingBasicFreeUpdateBookingFailsUnknown() {
        given(externalModuleManager.updateBooking(eq(bookingDTO.getId()), any()))
                .willReturn(new ApiCallWrapperBuilder<BookingDTO, UpdateBookingRequest>(ApiCall.Endpoint.UPDATE_BOOKING, ApiCall.Result.FAIL)
                        .build());
        given(externalModuleManager.searchBookings(any()))
                .willReturn(new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS, ApiCall.Result.SUCCESS)
                        .build());
        bookingUpdater.updateBookingBasicFreeFirstBooking(bookingDTO, membershipDTO);
    }
}