package com.odigeo.membership.robots.manager.bookingapi;

import bean.test.BeanTest;
import com.odigeo.bookingapi.v12.requests.CreateBookingProductCategoryRequest;
import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.PaymentType;
import com.odigeo.bookingapi.v12.requests.ProductCategoryBookingItemType;
import com.odigeo.bookingapi.v12.requests.Status;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class UpdateBookingRequestBuilderTest extends BeanTest<UpdateBookingRequestBuilder> {

    private static final FeeRequest FEE_REQUEST = new FeeRequestBuilder().build();
    private static final String CURRENCY = "EUR";
    private static final Long MEMBERSHIP_ID = 1L;
    private CreateBookingProductCategoryRequest createProduct;

    @Override
    protected UpdateBookingRequestBuilder getBean() {
        UpdateBookingRequestBuilder.CreateOtherProductProviderBookingBuilder createOtherProductProviderBookingBuilder = new UpdateBookingRequestBuilder.CreateOtherProductProviderBookingBuilder(Status.CONTRACT, BigDecimal.ZERO, "EUR", MEMBERSHIP_ID)
                .createProviderPaymentDetailRequestBuilder(new UpdateBookingRequestBuilder.CreateProviderPaymentDetailRequestBuilder(PaymentType.CASH, BigDecimal.ZERO, CURRENCY));
        createProduct = new UpdateBookingRequestBuilder.CreateBookingProductCategoryRequestBuilder(ProductCategoryBookingItemType.MEMBERSHIP_SUBSCRIPTION)
                .createOtherProductProviderBookingBuilder(createOtherProductProviderBookingBuilder)
                .feeRequests(FEE_REQUEST)
                .build();
        return new UpdateBookingRequestBuilder().withFeeRequests(Collections.singletonList(FEE_REQUEST)).createBookingProductCategoryRequests(createProduct);
    }

    @Test
    public void testBuild() {
        UpdateBookingRequest updateBookingRequest = getBean().build();
        assertEquals(updateBookingRequest.getBookingFees().get(0), FEE_REQUEST);
        UpdateBookingRequest updateBookingRequestAddProduct = getBean().buildAddProduct();
        assertTrue(updateBookingRequestAddProduct.getCreateBookingProductCategoryList().contains(createProduct));
    }

}
