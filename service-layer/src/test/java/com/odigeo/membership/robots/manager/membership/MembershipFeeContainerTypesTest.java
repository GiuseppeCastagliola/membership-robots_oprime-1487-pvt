package com.odigeo.membership.robots.manager.membership;

import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.testng.Assert.assertEquals;

public class MembershipFeeContainerTypesTest {

    @Test
    public void testGetAllSubCodes() {
        List<String> containerTypeSubCodes = MembershipFeeContainerTypes.getAllSubCodes();
        Set<String> subCodesSet = new HashSet<>(containerTypeSubCodes);

        assertEquals(subCodesSet.size(), containerTypeSubCodes.size(), "subCode has been duplicated. 1:1 relationship is assumed for processRemnantFee");
        assertEquals(MembershipFeeContainerTypes.values().length, containerTypeSubCodes.size(), "Unexpected number of subCodes");
    }
}
