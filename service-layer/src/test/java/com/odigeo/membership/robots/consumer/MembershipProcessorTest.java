package com.odigeo.membership.robots.consumer;

import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.consumer.updater.Updater;
import com.odigeo.membership.robots.criteria.MembershipPredicateProvider;
import com.odigeo.membership.robots.dto.BookingTrackingDTO;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class MembershipProcessorTest {
    private static final Long MEMBERSHIP_ID = 1L;
    private static final Long BOOKING_ID = 1L;
    @Mock
    private BookingDTO bookingDTO;
    @Mock
    private ExternalModuleManager externalModuleManager;
    @Mock
    private MembershipPredicateProvider membershipPredicateProvider;
    @Mock
    private Updater updater;
    @Mock
    private MembershipBookingTrackingProcessorFactory membershipBookingTrackingProcessorFactory;
    @Mock
    private MembershipBookingTrackingProcessor membershipBookingTrackingProcessor;
    @InjectMocks
    private MembershipProcessor membershipProcessor;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        given(bookingDTO.getMembershipId()).willReturn(MEMBERSHIP_ID);
        given(bookingDTO.getId()).willReturn(BOOKING_ID);
        givenSuccessfulGetMembership();
        givenVoidSuccessfulSearchSubscriptionBooking();
        when(membershipBookingTrackingProcessorFactory.create(any(), any())).thenReturn(membershipBookingTrackingProcessor);
    }

    @AfterMethod
    public void cleanUp() {
        membershipProcessor = null;
    }

    @Test
    public void testProcessEmptyFilteredExecutions() {
        membershipProcessor.process(MEMBERSHIP_ID);
        then(externalModuleManager).shouldHaveNoInteractions();
        then(membershipPredicateProvider).shouldHaveNoInteractions();
    }

    @Test
    public void testProcessActivationValid() {
        setUpConsumerAndCondition(MembershipProcessor::addActivationCandidate, membershipPredicateProvider.isPendingToActivate(), true);
        membershipProcessor.process(MEMBERSHIP_ID);
        then(externalModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(membershipPredicateProvider).should().isPendingToActivate();
        then(updater).should().activateMembership(any(), any());
        then(externalModuleManager).shouldHaveNoMoreInteractions();
    }

    @Test
    public void testProcessBasicFreeUpdateCandidate() {
        setUpConsumerAndCondition(MembershipProcessor::addBasicFreeUpdateCandidate, membershipPredicateProvider.isBasicFreeToProcess(), true);
        membershipProcessor.process(MEMBERSHIP_ID);
        then(externalModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(membershipPredicateProvider).should().isBasicFreeToProcess();
        then(updater).should().updateBookingBasicFreeFirstBooking(eq(bookingDTO), any(MembershipDTO.class));
    }

    @Test
    public void testProcessRecurringCandidate() {
        setUpConsumerAndCondition(MembershipProcessor::addRecurringSavingCandidate, membershipPredicateProvider.hasRecurringId(), false);
        membershipProcessor.process(MEMBERSHIP_ID);
        then(externalModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(membershipPredicateProvider).should().hasRecurringId();
        then(updater).should().saveRecurringId(any(), any());
        then(externalModuleManager).shouldHaveNoMoreInteractions();
    }

    @Test
    public void testMembershipNotCompliantToAnyProcessing() {
        setUpConsumerAndCondition(MembershipProcessor::addActivationCandidate, membershipPredicateProvider.isPendingToActivate(), false);
        setUpConsumerAndCondition(MembershipProcessor::addRecurringSavingCandidate, membershipPredicateProvider.hasRecurringId(), true);
        setUpConsumerAndCondition(MembershipProcessor::addBasicFreeUpdateCandidate, membershipPredicateProvider.isBasicFreeToProcess(), false);
        setUpConsumerAndCondition(MembershipProcessor::addTrackingCandidate, membershipPredicateProvider.isActivated(), false);
        setUpConsumerAndCondition(MembershipProcessor::addRebalanceNotContractBookingTrackingCandidate, membershipPredicateProvider.isActivated(), false);
        membershipProcessor.process(MEMBERSHIP_ID);
        then(externalModuleManager).should().getMembership(eq(MEMBERSHIP_ID));
        then(externalModuleManager).shouldHaveNoMoreInteractions();
        then(updater).shouldHaveNoInteractions();
    }

    @Test
    public void testTrackingCandidate() {
        setUpConsumerAndCondition(MembershipProcessor::addTrackingCandidate, membershipPredicateProvider.isActivated(), true);
        membershipProcessor.process(MEMBERSHIP_ID);
        then(membershipBookingTrackingProcessor).should().addTrackingCandidate();
        then(membershipBookingTrackingProcessor).should().process(eq(bookingDTO.getId()));
    }

    @Test
    public void testRebalanceTrackingCandidate() {
        setUpConsumerAndCondition(MembershipProcessor::addRebalanceNotContractBookingTrackingCandidate, membershipPredicateProvider.isActivated(), true);
        membershipProcessor.process(MEMBERSHIP_ID);
        then(membershipBookingTrackingProcessor).should().addRebalanceNotContractBookingTrackingCandidate();
        then(membershipBookingTrackingProcessor).should().process(eq(bookingDTO.getId()));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testFailedGetMembership() {
        givenSuccessfulGetBookingTracking();
        setUpConsumerAndCondition(MembershipProcessor::addActivationCandidate, membershipPredicateProvider.isActivated(), true);
        given(externalModuleManager.getMembership(eq(MEMBERSHIP_ID)))
                .willReturn(new ApiCallWrapperBuilder<MembershipDTO, Long>(ApiCall.Endpoint.GET_MEMBERSHIP, ApiCall.Result.FAIL).throwable(new Exception()).build());
        membershipProcessor.process(MEMBERSHIP_ID);
    }

    private void givenVoidSuccessfulSearchSubscriptionBooking() {
        ApiCallWrapper<List<BookingDTO>, SearchBookingsRequest> searchBookingApiCall = new ApiCallWrapperBuilder<List<BookingDTO>, SearchBookingsRequest>(ApiCall.Endpoint.SEARCH_BOOKINGS, ApiCall.Result.SUCCESS)
                .build();
        given(externalModuleManager.searchBookings(any())).willReturn(searchBookingApiCall);
    }

    private void givenSuccessfulGetMembership() {
        MembershipDTO membershipDTO = MembershipDTO.builder().id(MEMBERSHIP_ID).build();
        ApiCallWrapper<MembershipDTO, Long> membershipApiCallWrapper = new ApiCallWrapperBuilder<MembershipDTO, Long>(ApiCall.Endpoint.GET_MEMBERSHIP, ApiCall.Result.SUCCESS).response(membershipDTO).build();
        given(externalModuleManager.getMembership(MEMBERSHIP_ID)).willReturn(membershipApiCallWrapper);
    }

    private void givenSuccessfulGetBookingTracking() {
        ApiCallWrapper<BookingTrackingDTO, BookingDTO> getTrackingApiCall = new ApiCallWrapperBuilder<BookingTrackingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING_TRACKING, ApiCall.Result.SUCCESS)
                .response(BookingTrackingDTO.builder().build())
                .build();
        given(externalModuleManager.getBookingTracking(eq(bookingDTO))).willReturn(getTrackingApiCall);
    }

    private void setUpConsumerAndCondition(Consumer<MembershipProcessor> membershipProcessorMethod, Predicate<MembershipDTO> condition, boolean conditionResult) {
        given(condition).willReturn(membership -> conditionResult);
        membershipProcessorMethod.accept(membershipProcessor);
    }
}