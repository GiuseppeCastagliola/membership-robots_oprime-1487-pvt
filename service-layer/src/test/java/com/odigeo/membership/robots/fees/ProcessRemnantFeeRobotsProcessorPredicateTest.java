package com.odigeo.membership.robots.fees;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.odigeo.membership.robots.apicall.ApiCall.Result.FAIL;
import static com.odigeo.membership.robots.apicall.ApiCall.Result.SUCCESS;
import static com.odigeo.membership.robots.fees.ProcessRemnantFeeFilterPredicate.PROCESS_REMNANT_FEE_CHECK;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ProcessRemnantFeeRobotsProcessorPredicateTest {

    private static final long MEMBERSHIP_ID = 354L;
    private static final BigDecimal POSITIVE_BALANCE = BigDecimal.TEN;
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_OK = buildMembershipDTO(POSITIVE_BALANCE, ProcessRemnantFeeFilterPredicate.CUTOVER_DATE.plusMonths(1));
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_ZERO_BALANCE = buildMembershipDTO(BigDecimal.ZERO, ProcessRemnantFeeFilterPredicate.CUTOVER_DATE.plusMonths(1));
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_BEFORE_CUTOVER = buildMembershipDTO(POSITIVE_BALANCE, ProcessRemnantFeeFilterPredicate.CUTOVER_DATE.minusMonths(1));
    private static MembershipDTO MEMBERSHIP_DTO_EXPIRED_INIT = buildMembershipDTO(POSITIVE_BALANCE, ProcessRemnantFeeFilterPredicate.CUTOVER_DATE.plusMonths(1), ProcessRemnantFeeFilterPredicate.INIT);

    @Test
    public void testProcessRemnantFeeCheckPredicate_AllConditionsApply() {
        assertTrue(PROCESS_REMNANT_FEE_CHECK.test(buildApiCallWrapper(SUCCESS, MEMBERSHIP_DTO_EXPIRED_OK)));
    }

    @Test(dataProvider = "negativeApiCallWrapperExamples")
    public void testProcessRemnantFeeCheckPredicate_CallFailed(ApiCall.Result result, MembershipDTO membershipDTO) {
        assertFalse(PROCESS_REMNANT_FEE_CHECK.test(buildApiCallWrapper(result, membershipDTO)));
    }

    @DataProvider(name = "negativeApiCallWrapperExamples")
    public Object[][] negativeApiCallWrapperExamples() {
        return new Object[][] {
                {FAIL, null},
                {SUCCESS, MEMBERSHIP_DTO_EXPIRED_INIT},
                {SUCCESS, MEMBERSHIP_DTO_EXPIRED_ZERO_BALANCE},
                {SUCCESS, MEMBERSHIP_DTO_EXPIRED_BEFORE_CUTOVER}
        };
    }

    private static MembershipDTO buildMembershipDTO(BigDecimal balance, LocalDateTime activationDate) {
        return buildMembershipDTO(balance, activationDate, null);
    }

    private static MembershipDTO buildMembershipDTO(BigDecimal balance, LocalDateTime activationDate, String productStatus) {
        return MembershipDTO.builder()
                .id(MEMBERSHIP_ID)
                .status(ProcessRemnantFeeFilterPredicate.EXPIRED)
                .balance(balance)
                .activationDate(activationDate)
                .productStatus(productStatus)
                .build();
    }

    private ApiCallWrapper<MembershipDTO, MembershipDTO> buildApiCallWrapper(ApiCall.Result result, MembershipDTO membershipDTO) {
        return new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.EXPIRE_MEMBERSHIP)
                .result(result)
                .response(membershipDTO)
                .build();
    }
}