package com.odigeo.membership.robots.criteria;

import java.util.Arrays;
import java.util.function.Predicate;

import static org.testng.Assert.assertTrue;

public class PredicateHelper<T> {

    public Predicate<T> truePredicate() {
        return t -> true;
    }

    public Predicate<T> falsePredicate() {
        return t -> false;
    }

    protected void predicateMultipleTest(Predicate<T> predicate, T argument, Boolean... expected) {
        Arrays.stream(expected).forEach(expectedAssert -> assertTrue(expectedAssert ? predicate.test(argument) : predicate.negate().test(argument)));
    }
}
