package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.request.search.MembershipSearchRequest;
import com.odigeo.membership.robots.dto.SearchMemberAccountsDTO;
import com.odigeo.membership.robots.dto.SearchMembershipsDTO;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MembershipSearchRequestMapperTest {
    private static final MembershipSearchRequestMapper MEMBERSHIP_SEARCH_REQUEST_MAPPER = Mappers.getMapper(MembershipSearchRequestMapper.class);
    private static final LocalDate NOW = LocalDate.now();
    private static final String WEBSITE_ES = "ES";
    private static final Long USER_ID = 1L;

    @Test
    public void testNullMapper() {
        assertNull(MEMBERSHIP_SEARCH_REQUEST_MAPPER.searchDtoToSearchRequestBuilder(null));
    }

    @Test
    public void testSearchDtoToSearchRequestBuilder() {
        SearchMembershipsDTO searchMembershipsDTO = SearchMembershipsDTO.builder()
                .status(StringUtils.SPACE)
                .autoRenewal(StringUtils.SPACE)
                .fromExpirationDate(NOW)
                .toExpirationDate(NOW)
                .fromActivationDate(NOW)
                .minBalance(BigDecimal.TEN)
                .withMemberAccount(true)
                .website(WEBSITE_ES)
                .searchMemberAccountsDTO(SearchMemberAccountsDTO.builder()
                        .userId(USER_ID)
                        .firstName(StringUtils.SPACE)
                        .lastName(StringUtils.SPACE)
                        .build())
                .build();
        MembershipSearchRequest membershipSearchRequest = MEMBERSHIP_SEARCH_REQUEST_MAPPER.searchDtoToSearchRequestBuilder(searchMembershipsDTO).build();
        assertEquals(membershipSearchRequest.getStatus(), StringUtils.SPACE);
        assertEquals(membershipSearchRequest.getAutoRenewal(), StringUtils.SPACE);
        assertEquals(membershipSearchRequest.getFromActivationDate(), NOW.format(DateTimeFormatter.ISO_DATE));
        assertEquals(membershipSearchRequest.getFromExpirationDate(), NOW.format(DateTimeFormatter.ISO_DATE));
        assertEquals(membershipSearchRequest.getToExpirationDate(), NOW.format(DateTimeFormatter.ISO_DATE));
        assertEquals(membershipSearchRequest.getMinBalance(), BigDecimal.TEN);
        assertEquals(membershipSearchRequest.getWebsite(), WEBSITE_ES);
        assertEquals(membershipSearchRequest.getMemberAccountSearchRequest().getFirstName(), StringUtils.SPACE);
        assertEquals(membershipSearchRequest.getMemberAccountSearchRequest().getLastName(), StringUtils.SPACE);
        assertEquals(membershipSearchRequest.getMemberAccountSearchRequest().getUserId(), USER_ID);
    }
}