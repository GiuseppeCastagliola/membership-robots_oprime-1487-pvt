package com.odigeo.membership.robots.consumer;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.criteria.BookingPredicateProvider;
import com.odigeo.membership.robots.criteria.PredicateHelper;
import com.odigeo.membership.robots.dto.booking.BookingDTO;
import com.odigeo.membership.robots.exceptions.consumer.BookingNotFoundException;
import com.odigeo.membership.robots.exceptions.consumer.RetryableException;
import com.odigeo.membership.robots.manager.bookingapi.BookingApiManager;
import com.odigeo.membership.robots.metrics.MembershipRobotsMetrics;
import com.odigeo.membership.robots.metrics.MetricsName;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.MockitoAnnotations.openMocks;

public class BookingProcessorTest {

    private static final long BOOKING_ID = 1L;
    private static final Long MEMBERSHIP_ID = 111L;
    private static final String BOOKING_NOT_FOUND_MSG = "GET_BOOKING : 1 FAIL com.edreams.base.MissingElementException: Don't find booking: 1 - 1";

    @Mock
    private BookingApiManager bookingApiManager;
    @Mock
    private BookingPredicateProvider bookingPredicateProvider;
    @Mock
    private MembershipProcessorFactory membershipProcessorFactory;
    @Mock
    private MembershipRobotsMetrics membershipRobotsMetrics;
    @Mock
    private MembershipProcessor membershipProcessor;

    private PredicateHelper<BookingDTO> predicateHelper;

    private BookingProcessor bookingProcessor;
    private ApiCallWrapper<BookingDTO, BookingDTO> successfulGetBookingCall;
    private ApiCallWrapper<BookingDTO, BookingDTO> failedGetBookingCall;
    private ApiCallWrapper<BookingDTO, BookingDTO> failedGetBookingNotFoundCall;


    @BeforeMethod
    public void setUp() {
        openMocks(this);
        BookingDTO bookingDTO = BookingDTO.builder().id(BOOKING_ID).membershipId(MEMBERSHIP_ID).build();
        successfulGetBookingCall = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING, ApiCall.Result.SUCCESS)
                .response(bookingDTO)
                .build();
        failedGetBookingCall = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING, ApiCall.Result.FAIL)
                .response(null)
                .throwable(new Exception())
                .build();
        failedGetBookingNotFoundCall = new ApiCallWrapperBuilder<BookingDTO, BookingDTO>(ApiCall.Endpoint.GET_BOOKING, ApiCall.Result.FAIL)
                .response(null)
                .message(BOOKING_NOT_FOUND_MSG)
                .build();
        predicateHelper = new PredicateHelper<>();
        given(membershipProcessorFactory.create(eq(bookingDTO))).willReturn(membershipProcessor);
        given(bookingPredicateProvider.isNotTestAndIsPrime()).willReturn(predicateHelper.truePredicate());
    }

    @Test
    public void testProcessBooking() {
        configureBookingProcessor(true, true, true, true, true);
        given(bookingApiManager.getBooking(any(ApiCallWrapperBuilder.class))).willReturn(successfulGetBookingCall);
        bookingProcessor.process(BOOKING_ID);
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.PRIME_BOOKING));
        then(bookingApiManager).should().getBooking(any(ApiCallWrapperBuilder.class));
        then(membershipProcessor).should().process(eq(MEMBERSHIP_ID));
        then(membershipProcessor).should().addActivationCandidate();
        then(membershipProcessor).should().addTrackingCandidate();
        then(membershipProcessor).should().addRebalanceNotContractBookingTrackingCandidate();
        then(membershipProcessor).should().addRecurringSavingCandidate();
        then(membershipProcessor).should().addBasicFreeUpdateCandidate();
    }

    @Test
    public void testProcessBookingEligibleForActivationOnly() {
        configureBookingProcessor(true, false, false, false, false);
        given(bookingApiManager.getBooking(any(ApiCallWrapperBuilder.class))).willReturn(successfulGetBookingCall);
        bookingProcessor.process(BOOKING_ID);
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.PRIME_BOOKING));
        then(bookingApiManager).should().getBooking(any(ApiCallWrapperBuilder.class));
        then(membershipProcessor).should().addActivationCandidate();
        then(membershipProcessor).should().process(eq(MEMBERSHIP_ID));
        then(membershipProcessor).shouldHaveNoMoreInteractions();
    }

    @Test
    public void testProcessBookingNoEligibility() {
        configureBookingProcessor(false, false, false, false, false);
        given(bookingApiManager.getBooking(any(ApiCallWrapperBuilder.class))).willReturn(successfulGetBookingCall);
        bookingProcessor.process(BOOKING_ID);
        then(membershipRobotsMetrics).should().incrementCounter(eq(MetricsName.PRIME_BOOKING));
        then(bookingApiManager).should().getBooking(any(ApiCallWrapperBuilder.class));
        then(membershipProcessor).should().process(anyLong());
        then(membershipProcessor).shouldHaveNoMoreInteractions();
    }

    @Test
    public void testNotPrimeBooking() {
        given(bookingPredicateProvider.isNotTestAndIsPrime()).willReturn(predicateHelper.falsePredicate());
        given(bookingApiManager.getBooking(any(ApiCallWrapperBuilder.class))).willReturn(successfulGetBookingCall);
        configureBookingProcessor(false, false, false, false, false);
        bookingProcessor.process(BOOKING_ID);
        then(membershipProcessor).shouldHaveNoInteractions();
        then(membershipRobotsMetrics).shouldHaveNoInteractions();
    }

    @Test(expectedExceptions = RetryableException.class)
    public void testProcessBookingRetryable() {
        configureBookingProcessor(false, false, false, false, false);
        given(bookingApiManager.getBooking(any(ApiCallWrapperBuilder.class))).willReturn(failedGetBookingCall);
        bookingProcessor.process(1);
    }

    @Test(expectedExceptions = BookingNotFoundException.class)
    public void testProcessBookingNotFound() {
        configureBookingProcessor(false, false, false, false, false);
        given(bookingApiManager.getBooking(any(ApiCallWrapperBuilder.class))).willReturn(failedGetBookingNotFoundCall);
        bookingProcessor.process(1);
    }

    private void configureBookingProcessor(boolean activation, boolean tracking, boolean trackingUpdate, boolean recurring, boolean basicFree) {
        given(bookingPredicateProvider.eligibleForActivation()).willReturn(bookingDTO -> activation);
        given(bookingPredicateProvider.eligibleForTracking()).willReturn(bookingDTO -> tracking);
        given(bookingPredicateProvider.eligibleForTrackingUpdate()).willReturn(bookingDTO -> trackingUpdate);
        given(bookingPredicateProvider.eligibleForRecurring()).willReturn(bookingDTO -> recurring);
        given(bookingPredicateProvider.eligibleForBasicFreeUpdate()).willReturn(bookingDTO -> basicFree);
        bookingProcessor = new BookingProcessor(bookingApiManager, bookingPredicateProvider, membershipProcessorFactory, membershipRobotsMetrics);

    }
}