package com.odigeo.membership.robots.discard;

import com.odigeo.membership.robots.apicall.ApiCall;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipDTOBuilder;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.manager.ExternalModuleManager;
import com.odigeo.membership.robots.report.Reporter;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.odigeo.membership.robots.apicall.ApiCall.Result.SUCCESS;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.MockitoAnnotations.openMocks;

public class MembershipDiscardServiceBeanTest {

    private static final MembershipDTOBuilder MEMBERSHIP_DTO_BUILDER = MembershipDTO.builder();
    private static final MembershipOfferDTO.Builder MEMBERSHIP_OFFER_DTO_BUILDER = MembershipOfferDTO.builder();
    private static final MembershipDTO MEMBERSHIP_DTO_ID1 = MEMBERSHIP_DTO_BUILDER.id(1L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_ID2 = MEMBERSHIP_DTO_BUILDER.id(2L).build();
    private static final MembershipDTO MEMBERSHIP_DTO_DISCARDED_ID1 = MEMBERSHIP_DTO_BUILDER.id(1L).balance(BigDecimal.TEN).status("DISCARDED").build();
    private static final MembershipDTO MEMBERSHIP_DTO_DISCARDED_ID2 = MEMBERSHIP_DTO_BUILDER.id(2L).balance(BigDecimal.ZERO).status("DISCARDED").build();
    private static final MembershipOfferDTO MEMBERSHIP_OFFER_DTO_ID1 = MEMBERSHIP_OFFER_DTO_BUILDER.price(BigDecimal.TEN).duration(12).currencyCode("EUR").build();
    private static final MembershipOfferDTO MEMBERSHIP_OFFER_DTO_ID2 = MEMBERSHIP_OFFER_DTO_BUILDER.price(BigDecimal.TEN).duration(12).currencyCode("EUR").build();
    private static final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> GET_OFFER_SUCCESS_1 = new ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO>(ApiCall.Endpoint.GET_OFFER)
            .result(SUCCESS).request(MEMBERSHIP_DTO_DISCARDED_ID1).response(MEMBERSHIP_OFFER_DTO_ID1).build();
    private static final ApiCallWrapper<MembershipOfferDTO, MembershipDTO> GET_OFFER_SUCCESS_2 = new ApiCallWrapperBuilder<MembershipOfferDTO, MembershipDTO>(ApiCall.Endpoint.GET_OFFER)
            .result(SUCCESS).request(MEMBERSHIP_DTO_DISCARDED_ID2).response(MEMBERSHIP_OFFER_DTO_ID2).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> DISCARD_SUCCESS_1 = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.DISCARD_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_DISCARDED_ID1).build();
    private static final ApiCallWrapper<MembershipDTO, MembershipDTO> DISCARD_SUCCESS_2 = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(ApiCall.Endpoint.DISCARD_MEMBERSHIP)
            .result(SUCCESS).response(MEMBERSHIP_DTO_DISCARDED_ID2).build();

    @Mock
    private ExternalModuleManager externalModuleManager;
    @Mock
    private Reporter reporter;

    private MembershipDiscardServiceBean membershipDiscardServiceBean;

    @BeforeMethod
    public void setUp() {
        openMocks(this);
        membershipDiscardServiceBean = new MembershipDiscardServiceBean(externalModuleManager, reporter);
    }

    @Test
    public void testDiscardMembershipsById() {
        List<Long> listOfMembershipIds = new ArrayList<>();
        listOfMembershipIds.add(MEMBERSHIP_DTO_ID1.getId());
        listOfMembershipIds.add(MEMBERSHIP_DTO_ID2.getId());

        given(externalModuleManager.retrieveMembershipsByIds(listOfMembershipIds)).will(answerSuccess(listOfMembershipIds, MEMBERSHIP_DTO_ID1, MEMBERSHIP_DTO_ID2));
        given(externalModuleManager.discardMembership(MEMBERSHIP_DTO_ID1)).willReturn(DISCARD_SUCCESS_1);
        given(externalModuleManager.discardMembership(MEMBERSHIP_DTO_ID2)).willReturn(DISCARD_SUCCESS_2);
        given(externalModuleManager.getMembershipSubscriptionOffer(MEMBERSHIP_DTO_DISCARDED_ID1)).willReturn(GET_OFFER_SUCCESS_1);
        given(externalModuleManager.getMembershipSubscriptionOffer(MEMBERSHIP_DTO_DISCARDED_ID2)).willReturn(GET_OFFER_SUCCESS_2);

        membershipDiscardServiceBean.discardMemberships(listOfMembershipIds);

        then(externalModuleManager).should().retrieveMembershipsByIds(listOfMembershipIds);
        then(externalModuleManager).should().discardMembership(MEMBERSHIP_DTO_ID1);
        then(externalModuleManager).should().discardMembership(MEMBERSHIP_DTO_ID2);
        then(externalModuleManager).should().getMembershipSubscriptionOffer(DISCARD_SUCCESS_1.getResponse());
        then(externalModuleManager).should().getMembershipSubscriptionOffer(DISCARD_SUCCESS_2.getResponse());
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(GET_OFFER_SUCCESS_1);
        then(externalModuleManager).should().createPendingToCollectFromPreviousMembership(GET_OFFER_SUCCESS_2);

    }

    private Answer<ApiCallWrapper<List<MembershipDTO>, List<Long>>> answerSuccess(List<Long> request, MembershipDTO... responses) {
        final ApiCallWrapperBuilder<List<MembershipDTO>, List<Long>> dtoApiCallWrapperBuilder = new ApiCallWrapperBuilder<>(ApiCall.Endpoint.GET_MEMBERSHIP);
        return (InvocationOnMock invocation) -> dtoApiCallWrapperBuilder.result(SUCCESS).response(Arrays.asList(responses)).request(request).build();
    }

}
