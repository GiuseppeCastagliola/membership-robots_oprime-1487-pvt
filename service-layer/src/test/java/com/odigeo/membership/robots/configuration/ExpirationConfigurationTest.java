package com.odigeo.membership.robots.configuration;

import bean.test.BeanTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ExpirationConfigurationTest extends BeanTest<ExpirationConfiguration> {

    private static final String[] WEBSITE_LIST = {"CH"};
    private static final String WEBSITE_CH = "CH";
    private static final String WEBSITE_AU = "AU";
    private static final String WEBSITE_IT = "IT";

    private ExpirationConfiguration expirationConfiguration;

    @BeforeMethod
    public void setUp() {
        expirationConfiguration = new ExpirationConfiguration();
        expirationConfiguration.setDaysInThePast(0L);
        expirationConfiguration.setMonthlyWebsites(WEBSITE_LIST);
    }

    @Override
    protected ExpirationConfiguration getBean() {
        return expirationConfiguration;
    }

    @Test
    public void testIsMonthlyWebsite() {
        assertTrue(expirationConfiguration.isMonthlyWebsite(WEBSITE_CH));
        assertFalse(expirationConfiguration.isMonthlyWebsite(WEBSITE_AU));
        assertFalse(expirationConfiguration.isMonthlyWebsite(WEBSITE_IT));
    }
}
